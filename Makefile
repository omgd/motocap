# SPDX-License-Identifier: AGPL-3.0-or-later

OLDPAGES ?= 0

TABLES := materials.tsv gears.tsv
tables: ${TABLES}				# Process local data

.PHONY: cleanall index index-products update

update:
	${MAKE} OLDPAGES=4 index
	${MAKE} index-products
	${MAKE} pdf
	${MAKE} tables


# ------------ Deps & utils
DRY := $(findstring n,$(MAKEFLAGS))
CURL = $(if ${DRY},@echo "(DRY-RUN)" )curl -LfZ --create-dirs --user-agent 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3'
PDF2TXT := pdftotext -layout
PYTHON := python3
JQ := jq

.SECONDARY:
.SUFFIXES:
MAKEFLAGS += --no-builtin-rules


# ------------ Mirror / update from web
WEB := motocap.com.au

index ${WEB}/products:
	${CURL} "https://${WEB}/products/{pants,jackets,gloves}?&sort_by=published_time&sort_order=DESC&page=[0-${OLDPAGES}]" -o "${WEB}/products/$$(date -I)-#1-#2"
	+touch ${WEB}/products

urls-product.mk: ${WEB}/products
	printf '%s\\\n' "URLS_PRODUCT := " >$@
	grep -ohE 'href="/product/[a-zA-Z0-9%-_]*"' $</* | sed 's/href="\([^"]*\)"/${WEB}\1 \\/g' |sort |uniq  >>$@
	printf '\n' >>$@
include urls-product.mk

index-products: ${URLS_PRODUCT}

${WEB}/%:
	${CURL} "https://$@" -o "$@"

urls-pdf.mk: ${WEB}/product urls-product.mk
	printf '%s\\\n' "URLS_PDF := " >$@
	grep -ohE 'https?://${WEB}/[a-zA-Z0-9%-/_]*\.pdf' $</* | sed -E 's/^https?:\/\/([^>]*)/\1 \\/g' |sort |uniq >>$@
	printf '\n' >>$@
include urls-pdf.mk

pdf: private DIRS = ${WEB}/sites/default/files/20*
pdf: urls-pdf.mk ${URLS_PDF}  # Make symlinks to all downloaded pdf reports (to last one for homonyms)
	mkdir -p $@
	for f in ${DIRS}/*.pdf; do ln -srf "$$f" $@/$$(basename "$$f" |sed 's/%20/__/g' |sed 's/%/_/g'); done

.INTERMEDIATE: ${URLS_PRODUCT} ${URLS_PDF}


# ------------ Local processing
REPORTS := $(sort $(subst %,_,$(subst %20,__,$(notdir ${URLS_PDF:.pdf=}))))
# REPORTS := $(patsubst pdf/%.pdf,%,$(wildcard pdf/*.pdf))
reports:
	mkdir -p $@

.SECONDEXPANSION:

reports-txt: ${REPORTS:%=reports/%.txt}
${REPORTS:%=reports/%.txt}: reports/%.txt: pdf/%.pdf $$P | reports
	${PDF2TXT} "$<" "$@"
	$(if $P,patch $@ $P)
%.txt: private P = $(wildcard patches/$(subst /,--,$@).patch)

reports-json: ${REPORTS:%=reports/%.json}
${REPORTS:%=reports/%.json}: reports/%.json: reports/%.txt
	${PYTHON} parse.py $< >$@

${TABLES}: %: filters.jq ${REPORTS:%=reports/%.json}
	cat reports/*.json | ${JQ} -rs 'include "./filters"; $(subst .,_,$@)' >$@

cleanall:
	rm -rf urls-*.mk pdf reports ${TABLES}

clean-pdf:
	rm -rf ${WEB}/sites/default/files

# ------------ Git/Make
# Make relies on mtimes, which git does not store...
MTIMES_DUMP = { for f in `git ls | grep -v "^.mtimes\$$"` ; do stat -c "@%Y %n" "$$f" ; done } >.mtimes
MTIMES_RESTORE = { while read t n; do touch -d "$$t" "$$n" ; done } <.mtimes

.PHONY: mtimes-dump mtimes-restore git-commit-data

mtimes-dump:					# Dump modification times of all files
	${MTIMES_DUMP}
mtimes-restore:					# Restore modification times
	${MTIMES_RESTORE}

git-commit-data:
	git add ${TABLES} urls-*.mk reports/
	git ci -m "add reports `date +%Y-%m-%d`"
.git/hooks/pre-commit: .git/hooks
	printf '#!/bin/sh\n%s\ngit add .mtimes\n' '${MTIMES_DUMP}' >$@ && chmod +x $@
.git/hooks/post-checkout: .git/hooks
	printf '#!/bin/sh\n%s' '${MTIMES_RESTORE}' >$@ && chmod +x $@
.git/hooks: ; mkdir -p $@
