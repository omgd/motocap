                                                           This MotoCAP safety rating applies to:
                                                           Brand                    DriRider
                                                           Model                    Vivid 2 Ladies
                                                           Type                     Jacket - Textile
                                                           Date purchased           23 February 2021
                                                           Sizes tested             14 and 20
                                                           Test garment gender      Female
                                                           Style                    All Purpose
                                                           RRP                      $319.95


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating          ★★             30.9
                                                           Abrasion                            1/10          0.99
                                                           Burst                              10/10         1776
                                                           Impact                              4/10          27.4
                                                           MotoCAP Breathability Rating        ⯨            0.126
                                                           Moisture Vapour Resistance           -           155.0
                                                           Thermal Resistance                   -           0.325
                                                           Water resistance                   1/10           56.8
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are zipped vents in the chest, arms and back to allow controlled airflow
movement through the garment. The breathability rating is based on tests of the garment's materials when
all vents are closed. The breathability of this product may be better when the vents can be opened.
Breathability was measured without the removable thermal liner installed.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                            DriRider Vivid 2 Ladies
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                       Abrasion Resistance Performance
                                                                       Abrasion rating      1/10
                                                                       Abrasion score       0.99




Determining Criteria    Area            Good        Acceptable       Marginal      Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6       1.3 - 2.9     < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5       0.8 - 1.7     < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5       0.4 - 0.9     < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.86   0.76     1.12     1.02     1.00     1.17                 0.99   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.86     0.76     1.12     1.02     1.00     1.17                  0.99   M

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.86     0.76     1.12     1.02     1.00     1.17                  0.99   M


Details of materials used in jacket
Material A       Woven fabric shell, water resistant layer and mesh inner liner




                                           DriRider Vivid 2 Ladies
Page 2 of 5                                    Textile Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                            Burst Strength Performance
                                                                            Burst rating    10/10
                                                                            Burst score      1776




Determining Criteria        Unit         Good        Acceptable       Marginal      Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799        < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4        Sample 5     Sample 6      Average
Zones 1 & 2 1947           1949           1550          1947            1957         1951          1883       G
Zones 3 & 4 1209           1626           1445          1433            889          1465          1344       G




                                            DriRider Vivid 2 Ladies
Page 3 of 5                                     Textile Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximium
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    4/10
                                                                               Impact score     27.4




Determining Criteria         Unit            Good       Acceptable       Marginal        Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            22.9       A                                    24.2         A
Maximum force (kN)                            27.0       M                                    27.5         M
Coverage of Zone 1 area                       80%                                             90%
Coverage of Zone after displacement           90%                                             90%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           20.7             20.5           27.0              24.4              23.6          27.5
Impact Protector 2           21.2             23.0           23.7              21.5              22.8          27.4
Impact Protector 3           21.6             24.6           23.4              21.9              22.7          26.1



                                               DriRider Vivid 2 Ladies
Page 4 of 5                                        Textile Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                     With water-resistant liner
Breathability rating       ⯨                                 Breathability rating       N/A
Breathability score      0.126                               Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)              1                2       Average
Without removable liners                               139.8          170.3          155.0
With water-resistant liner                              N/A            N/A            N/A
                              2
Thermal Resistance - Rct (K.m /W)                        1                2       Average
Without removable liners                               0.328          0.323          0.325
With water-resistant liner                              N/A            N/A            N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment            Water absorbed by underwear
                Volume (ml)    Percentage (%)        Volume (ml)    Percentage (%)
Jacket 1            320             22%                  192             71%
Jacket 2            235             16%                  122             43%
Average             277             19%                  157             57%

Location of wetting
There was major wetting to the cotton underwear present at the neck and chest for both jackets tested.




    Assessment Details.
    Brand                           DriRider
    Model                           Vivid 2 Ladies
    Type                            Jacket - Textile
    Date purchased                  23 February 2021
    Tested by                       AMCAF, Deakin University
    Garment test reference          J20T15
    Rating first published          June 2021
    Rating updated                  17 June 2021


                                                DriRider Vivid 2 Ladies
Page 5 of 5                                         Textile Jacket                            motocap.com.au
