                                                            This MotoCAP safety rating applies to:
                                                           Brand:                       Dririder
                                                           Model:                       Sprint 2
                                                           Type:                        Glove - Leather
                                                           Date purchased:              19 June 2024
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       Streetwear
                                                           RRP:                         $109.95
                                                            Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating           ★★           2.4
                                                           Abrasion                            5/10        2.72
                                                           Seam strength                       1/10         2.5
                                                           Impact                              5/10         9.8
                                                           Water resistance                    N/A         N/A
This glove is fitted with impact protectors for the knuckles and palm areas. Mesh fabric on the back of the
fingers and back of the wrist provides continuous airflow within the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          ✓
                                                                             Palm              ✓




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                               Dririder Sprint 2
Page 1 of 5                                     Leather Glove                                      motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating      5/10
                                                                      Abrasion score       2.72




Determining Criteria    Area            Good         Acceptable     Marginal      Poor
High abrasion risk      Zone 1 & 2      > 4.0         2.7 - 4.0     1.2 - 2.6     < 1.2
Medium abrasion risk    Zone 3           2.5          1.8 - 2.5     0.8 - 1.7     < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        45%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material C        55%              2.38   1.08     1.05     1.83     1.62     1.77                  1.62    M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        40%              4.75   5.85     4.17     5.77     5.15     5.91                  5.27    G
Material C        60%              2.38   1.08     1.05     1.83     1.62     1.77                  1.62    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        30%              4.75   5.85     4.17     5.77     5.15     5.91                  5.27    G
Material C        70%              2.38   1.08     1.05     1.83     1.62     1.77                  1.62    M
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell with mesh inner liner
Material B       Suede leather patch over leather shell
Material C       Leather shell




                                                Dririder Sprint 2
Page 2 of 5                                      Leather Glove                               motocap.com.au
Seam Tensile Strength
The tensile strength of the glove's seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                      Seam Strength Performance
                                                                                      Seam strength rating  1/10
                                                                                      Seam strength score    2.5




Determining Criteria        Unit             Good         Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11              9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200           100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2                3                  4                 5              Average
Zones 1 & 2             11.44        12.77            12.20              12.30             16.08          12.96     G
Zone 3                  6.83         10.85            9.18               9.90              12.49          9.85      A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2                3                  4                 5              Average
Wrist restraint         47.1         41.1             45.0               37.9              41.7           42.6      P




                                                     Dririder Sprint 2
Page 3 of 5                                           Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                     Impact Protection Performance
                                                                                     Impact rating    5/10
                                                                                     Impact score      9.8




Determining Criteria                      Unit             Good         Acceptable          Marginal         Poor
Impact force                              (kN)              <2             2 - 4.9           5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                        Palm
Average force (kN)                             1.1          G                 4.5       A
Maximum force (kN)                             1.5          G                 5.2       M
Coverage of zone 1 area                       100%                           50%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                                Palm
Strike number                   1                2                3                   1                2
Impact Protector 1              0.8              1.1              1.0                 4.3              3.8
Impact Protector 2              0.8              0.8              1.5                 5.2              4.5
Impact Protector 3              1.0              1.3              1.4                 4.7              4.2




                                                       Dririder Sprint 2
Page 4 of 5                                             Leather Glove                                        motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Dririder
   Model                          Sprint 2
   Type                           Glove - Leather
   Date purchased                 19 June 2024
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G24L39
   Rating first published         September 2024
   Rating updated                 5 September 2024


                                              Dririder Sprint 2
Page 5 of 5                                    Leather Glove                                motocap.com.au
