                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Finn Moto
                                                           Model                    882 Protective
                                                           Type                     Pants - Denim
                                                           Date purchased           10 December 2024
                                                           Sizes tested             36 and 38
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $218.00


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating        ★★★★           62.0
                                                           Abrasion                            9/10        6.47
                                                           Burst                              10/10       1397
                                                           Impact                              7/10        52.4
                                                           MotoCAP Breathability Rating      ★★★          0.426
                                                           Moisture Vapour Resistance          -           33.6
                                                           Thermal Resistance                  -          0.238
                                                           Water resistance                   N/A          N/A
This garment is fitted with impact protectors for the knees and hips. There are no vents to allow airflow
movement through the garment. There is the potential for burns from heat transferred through the fly
button of the pants during a slide.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Knee                        
                                                                      Hip                         




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact

                                          Finn Moto 882 Protective
Page 1 of 5                                     Denim Pants                                     motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      9/10
                                                                     Abrasion score       6.47




Determining Criteria    Area            Good        Acceptable     Marginal          Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             6.47   4.29     6.31     6.11     7.14     8.48                 6.47   G

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           6.47     4.29     6.31     6.11     7.14     8.48                  6.47   G

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           6.47     4.29     6.31     6.11     7.14     8.48                  6.47   G


Details of materials used in pant
Material A       Denim fabric shell, para-aramid fabric layer and mesh inner liner




                                          Finn Moto 882 Protective
Page 2 of 5                                     Denim Pants                                  motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1397




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1111           1665           1133          1593          1404          1707          1436        G
Zones 3 & 4 1317           1177           1289          1267          1281          1133          1244        G




                                          Finn Moto 882 Protective
Page 3 of 5                                     Denim Pants                                    motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     52.4




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                            13.9       G                                        15.6      A
Maximum force (kN)                            16.3       A                                        21.3      A
Coverage of Zone 1 area                       95%                                                130%
Coverage of Zone after displacement           80%                                                100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre            Mid            Edge              Centre            Mid            Edge
Impact Protector 1           12.5             14.0           16.3               13.6             17.2           21.3
Impact Protector 2           12.5             13.6           14.0               14.7             13.2           13.8
Impact Protector 3           13.2             14.0           15.0               15.6             16.1           14.8




                                              Finn Moto 882 Protective
Page 4 of 5                                         Denim Pants                                          motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating   ★★★                                 Breathability rating       N/A
Breathability score      0.426                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)          1               2          Average
Without removable liners                            34.4            32.7         33.6
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1               2         Average
Without removable liners                            0.243           0.233        0.238
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
These pants have not been advertised as water-resistant so have not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Finn Moto
   Model                            882 Protective
   Type                             Pants - Denim
   Date purchased                   10 December 2024                                        l
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           P25D09
   Rating first published           February 2025
   Rating updated                   10 February 2025


                                           Finn Moto 882 Protective
Page 5 of 5                                      Denim Pants                                    motocap.com.au
