                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Speed and Strength
                                                           Model                    Straight Savage 2.0
                                                           Type                     Jacket - Leather
                                                           Date purchased           10 February 2022
                                                           Sizes tested             XL and 2 XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $459.95


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating          ★★           32.7
                                                           Abrasion                            3/10        2.67
                                                           Burst                              10/10       1036
                                                           Impact                              4/10        29.9
                                                           MotoCAP Breathability Rating      ★★★          0.519
                                                           Moisture Vapour Resistance          -           30.3
                                                           Thermal Resistance                  -          0.262
                                                           Water resistance                   N/A          N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are zipped vents in the sides and arms to allow controlled airflow
movement through the garment. The breathability rating is based on tests of the garment's materials when
all vents are closed. The breathability of this product may be better when the vents can be opened.
Breathability was measured without the removable thermal liner installed.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                   
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Elbow                       
                                                                      Shoulder                    
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                   Speed and Strength Straight Savage 2.0
Page 1 of 5                                   Leather Jacket                                    motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      3/10
                                                                     Abrasion score       2.67




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6          3.0 - 5.6   1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5          1.8 - 2.5   0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5           1.0 - 1.5   0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             5.08   3.11     3.82     3.96     3.17     5.14                 4.05   A

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.61     0.50     0.39     0.38     0.57     0.43                  0.48   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       10%            5.08     3.11     3.82     3.96     3.17     5.14                  4.05   G
Material B       90%            0.61     0.50     0.39     0.38     0.57     0.43                  0.48   M
Details of materials used in jacket
Material A       Leather shell with mesh inner liner
Material B       Fabric shell with mesh inner liner




                                  Speed and Strength Straight Savage 2.0
Page 2 of 5                                  Leather Jacket                                  motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1036




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1467           825            1230          943           1239          1229          1156        G
Zones 3 & 4 366            356            314           712           775           823           558         M




                                   Speed and Strength Straight Savage 2.0
Page 3 of 5                                   Leather Jacket                                   motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    4/10
                                                                               Impact score     29.9




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            21.5       A                                    21.0         A
Maximum force (kN)                            24.3       A                                    24.7         A
Coverage of Zone 1 area                       80%                                             95%
Coverage of Zone after displacement           60%                                             90%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           18.4             21.4           23.0              20.2              22.5          20.8
Impact Protector 2           20.4             19.7           22.6              19.2              21.0          24.7
Impact Protector 3           21.5             22.4           24.3              19.5              19.2          22.0



                                      Speed and Strength Straight Savage 2.0
Page 4 of 5                                      Leather Jacket                                         motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating   ★★★                                 Breathability rating       N/A
Breathability score      0.519                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            30.1            30.5          30.3
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.261           0.263        0.262
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Speed and Strength
    Model                           Straight Savage 2.0
    Type                            Jacket - Leather
    Date purchased                  10 February 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J20L35
    Rating first published          May 2022
    Rating updated                  13 May 2022


                                    Speed and Strength Straight Savage 2.0
Page 5 of 5                                    Leather Jacket                               motocap.com.au
