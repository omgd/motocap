                                                           This MotoCAP safety rating applies to:
                                                           Brand                    BMW
                                                           Model                    Airflow
                                                           Type                     Pants - Textile
                                                           Date purchased           1 March 2022
                                                           Sizes tested             54
                                                           Test garment gender      Male
                                                           Style                    Tourer
                                                           RRP                      $580.00


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating          ★★           36.6
                                                           Abrasion                            1/10        0.51
                                                           Burst                              10/10       1440
                                                           Impact                              9/10        65.5
                                                           MotoCAP Breathability Rating       ★★          0.395
                                                           Moisture Vapour Resistance          -           34.3
                                                           Thermal Resistance                  -          0.226
                                                           Water resistance                   N/A          N/A
This garment is fitted with impact protectors for the knees and hips. There are mesh vents in the front of
upper legs to allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Knee                        
                                                                      Hip                         




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                BMW Airflow
Page 1 of 5                                     Textile Pants                                   motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.51




Determining Criteria    Area             Good        Acceptable    Marginal       Poor
High abrasion risk      Zones 1 & 2      > 5.6        3.0 - 5.6    1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3           > 2.5        1.8 - 2.5    0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4           >1.5         1.0 - 1.5    0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.37   0.45     0.51     0.48     0.46     0.52                 0.46   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       90%            0.37     0.45     0.51     0.48     0.46     0.52                  0.46     P
Material B       10%            1.79     1.56     1.36     1.37                                    1.52     M
Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       90%            0.37     0.45     0.51     0.48     0.46     0.52                  0.46     M
Material B       10%            1.79     1.56     1.36     1.37                                    1.52     G
Details of materials used in jacket
Material A       Fabric shell with fabric inner liner
Material B       Mesh fabric shell with fabric inner liner




                                                 BMW Airflow
Page 2 of 5                                      Textile Pants                               motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1440




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1362           1057           1553          2013          1156          1262          1401        G
Zones 3 & 4 738            1977           1530          1506          1764          2075          1598        G




                                                 BMW Airflow
Page 3 of 5                                      Textile Pants                                 motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    9/10
                                                                               Impact score     65.5




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15          15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                             10.4       G                                       17.9      A
Maximum force (kN)                             15.5       A                                       22.9      A
Coverage of Zone 1 area                       150%                                               130%
Coverage of Zone after displacement            90%                                               100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre             Mid            Edge             Centre            Mid            Edge
Impact Protector 1            9.0              9.9            13.1              14.5             18.0           21.0
Impact Protector 2            8.4              9.4            11.3              15.2             16.3           21.1
Impact Protector 3            8.4              8.7            15.5              14.9             17.2           22.9



                                                      BMW Airflow
Page 4 of 5                                           Textile Pants                                      motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★★                                Breathability rating       N/A
Breathability score      0.395                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            34.2            34.4          34.3
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.226           0.225        0.226
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           BMW
    Model                           Airflow
    Type                            Pants - Textile
    Date purchased                  1 March 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          P20T23
    Rating first published          July 2022
    Rating updated                  25 July 2022


                                                 BMW Airflow
Page 5 of 5                                      Textile Pants                              motocap.com.au
