                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Merlin
                                                           Model:                       Icon
                                                           Type:                        Glove - Leather/Textile
                                                           Date purchased:              23 March 2021
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $79.95
                                                           Test Results Summary:
                                                                                               Rating      Score
                                                           MotoCAP Protection Rating          ★★★           3.1
                                                           Abrasion                            8/10        4.35
                                                           Seam strength                       6/10         9.8
                                                           Impact                              2/10         5.6
                                                           Water resistance                    N/A          N/A
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm              




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                                 Merlin Icon
Page 1 of 5                                 Leather/Textile Glove                                  motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      8/10
                                                                     Abrasion score       4.35




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        50%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        50%              6.95   5.47     7.48     5.32     4.59     6.40                  6.04    G
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        60%              1.85   1.90     2.29     3.34     1.31     3.60                  2.38    M
Material D        40%              1.45   1.25     0.88     1.80     1.23     2.15                  1.46    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        30%              1.85   1.90     2.29     3.34     1.31     3.60                  2.38    A
Material D        70%              1.45   1.25     0.88     1.80     1.23     2.15                  1.46    M
Details of materials used in glove - derived from manufacturer provided information
Material A       Denim fabric shell over hard-shell armour
Material B       Leather patch over leather shell
Material C       Leather shell
Material D       Denim fabric shell




                                                Merlin Icon
Page 2 of 5                                Leather/Textile Glove                             motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  6/10
                                                                                     Seam strength score    9.8




Determining Criteria        Unit             Good        Acceptable         Marginal              Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             10.67        13.38           12.32              12.61             10.74          11.94     G
Zone 3                  5.58         11.32           4.01               12.24             9.47           8.52      M
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         194.1        128.5           225.4              137.6             242.3          185.6     A




                                                     Merlin Icon
Page 3 of 5                                     Leather/Textile Glove                                       motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    2/10
                                                                                   Impact score      5.6




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9            5-8             >8


* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             2.9        A                  5.8      M
Maximum force (kN)                             6.5        M                  7.0      M
Coverage of zone 1 area                       100%                          50%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                2              3                   1                2
Impact Protector 1              1.4              1.8            1.8                 7.0              4.9
Impact Protector 2              1.6              2.9            3.9                 5.7              5.7
Impact Protector 3              1.4              4.5            6.5                 7.0              4.8




                                                      Merlin Icon
Page 4 of 5                                      Leather/Textile Glove                                     motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Merlin
   Model                          Icon
   Type                           Glove - Leather/Textile
   Date purchased                 23 March 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L25
   Rating first published         August 2021
   Rating updated                 18 August 2021



                                                Merlin Icon
Page 5 of 5                                Leather/Textile Glove                            motocap.com.au
