                                                            This MotoCAP safety rating applies to:
                                                            Brand                    Ixon
                                                            Model                    Exhaust Ladies
                                                            Type                     Jacket - Textile
                                                            Date purchased           23 February 2021
                                                            Sizes tested             XL and 2XL
                                                            Test garment gender      Female
                                                            Style                    All Purpose
                                                            RRP                      $359.95


                                                            Test Results Summary                Rating        Score
                                                            MotoCAP Protection Rating           ★★             31.3
                                                            Abrasion                             1/10          0.58
                                                            Burst                               10/10         1319
                                                            Impact                               7/10          50.8
                                                            MotoCAP Breathability Rating         ⯨            0.105
                                                            Moisture Vapour Resistance            -           186.0
                                                            Thermal Resistance                    -           0.326
                                                            Water resistance                    6/10            8.5
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are no vents to allow airflow movement through the garment. This
garment has a removable 2 in 1 thermal/water-resistant liner. The breathability rating above was achieved
with the 2 in 1 thermal/water-resistant liner removed. When tested with the 2 in 1 thermal/water-resistant
liner installed, the breathability rating reduced but remained in the half star range.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                        Removable liners
                                                                        Thermal liner                    
                                                                        Water resistant liner            

                                                                        Removable impact protection
                                                                                      Pockets        Armour
                                                                        Elbow                         
                                                                        Shoulder                      
                                                                        Back            




          Zone 1                      Zone 2                         Zone 3                       Zone 4

   High risk of abrasion       High risk of abrasion      Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               Ixon Exhaust Ladies
Page 1 of 5                                       Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.58




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.76   0.56     0.41     0.59     0.00     0.00                 0.58   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.76     0.56     0.41     0.59     0.67     0.51                  0.58   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.76     0.56     0.41     0.59     0.67     0.51                  0.58   M


Details of materials used in jacket
Material A       Woven fabric shell with mesh inner liner




                                             Ixon Exhaust Ladies
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1319




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1687           1117           1625          1507          890           1648          1412        G
Zones 3 & 4 1038           835            1024          938           983           865.1         947         A




                                             Ixon Exhaust Ladies
Page 3 of 5                                     Textile Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximium
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     50.8




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            10.9       G                                    10.9         G
Maximum force (kN)                            14.7       G                                    14.7         G
Coverage of Zone 1 area                       85%                                             95%
Coverage of Zone after displacement           90%                                             90%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           10.5             10.2           14.7              10.5              10.2          14.7
Impact Protector 2            9.8              9.7           10.4               9.8               9.7          10.4
Impact Protector 3            9.5             10.7           12.8               9.5              10.7          12.8



                                                Ixon Exhaust Ladies
Page 4 of 5                                        Textile Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ⯨                               Breathability rating        ⯨
Breathability score      0.105                             Breathability score       0.102

Moisture Vapour Resistance - Ret (kPa.m2/W)            1              2         Average
Without removable liners                             208.0          164.0          186.0
With water-resistant liner                           294.3          296.5          295.4
                              2
Thermal Resistance - Rct (K.m /W)                      1              2         Average
Without removable liners                             0.327          0.325          0.326
With water-resistant liner                           0.507          0.497          0.502

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment          Water absorbed by underwear
                Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Jacket 1            506             30%                 26              9%
Jacket 2            446             26%                 22              8%
Average             476             28%                 24              9%

Location of wetting
There was minor wetting to the cotton underwear present at the neck for both jackets tested.




    Assessment Details.
    Brand                           Ixon
    Model                           Exhaust Ladies
    Type                            Jacket - Textile
    Date purchased                  23 February 2021
    Tested by                       AMCAF, Deakin University
    Garment test reference          J20T13
    Rating first published          April 2021
    Rating updated                  29 April 2021


                                                Ixon Exhaust Ladies
Page 5 of 5                                        Textile Jacket                            motocap.com.au
