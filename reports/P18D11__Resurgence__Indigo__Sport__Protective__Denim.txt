                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Resurgence
                                                           Model:                  Indigo sport
                                                           Type:                   Pants - Denim
                                                           Date purchased:         29 November 2018
                                                           Sizes tested:           36
                                                           Gender:                 M
                                                           Style:                  All Purpose
                                                           Test code:              P18D11
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating                  36.1
                                                           Abrasion                         3/10        2.21
                                                           Burst                           10/10       1017
                                                           Impact                           7/10        49.5
                                                           MotoCAP Comfort Rating                    0.355
                                                           Moisture Vapour Resistance                   28.4
                                                           Thermal Resistance                          0.168
                                                           Water resistance                 N/A         N/A
This garment is fitted with impact protectors for the knees and hips. There are no vents to allow airflow
movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact

                                           Resurgence Indigo Sport
Page 1 of 5                                   Protective Denim                                motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Denim shell, aramid fabric layer and mesh inner liner
Material B:      Denim shell




Zone             Coverage      Abrasion time for each test (seconds)                                    Average
                 (%)                1         2          3          4              5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       100%             2.82      2.75       2.47       1.84            2.45       2.43         2.46      M

Zone 3 area (Medium abrasion risk)
Material A       100%             2.82           2.75     2.47       1.84         2.45       2.43         2.46      A

Zone 4 area (Low abrasion risk)
Material A       20%                  2.82       2.75     2.47       1.84         2.45       2.43         2.46      G
Material B       80%                  0.54       0.35     0.45       0.42         0.33       0.32         0.40      M
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                             Good       Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:          > 5.6      3.0 - 5.6     1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:           > 2.5      1.8 - 2.5     0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:           >1.5       1.0 - 1.5     0.4 - 0.9          < 0.4

                                              Resurgence Indigo Sport
Page 2 of 5                                      Protective Denim                                    motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2               3                  4                 5              Average
Zones 1 & 2          956            1929            1929               1259              1734           1561      G
Zone EZ             765             800             753                548               607            695       M
Zones 3 & 4         691             696             553                341               579            572       M
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good           Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000         800 - 1000         500 - 799          < 500




                                             Resurgence Indigo Sport
Page 3 of 5                                     Protective Denim                                          motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The table below shows the test results for each strike on each impact protector in kilonewton (kN) and their
area of coverage as a proportion (%) of the Zone.
Impact protector type                        Knee                                                  Hip
Average force (kN)                           15.0        A                                         15.3      A
Maximum force (kN)                            17.5       A                                         16.9      A
Coverage of zone 1 area                      120%                                                  90%
Coverage of zone after displacement           70%                                                  90%
Individual test results
Impact force (kN)             Knee                                                  Hip
Strike location                A               B              C                      A              B             C
Impact Protector 1             15.2           14.3           15.7                   14.8           14.4          15.6
Impact Protector 2             14.6           14.3           14.5                   14.7           15.1          15.7
Impact Protector 3             14.3           17.5           14.5                   15.2           16.9          15.1
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good        Acceptable        Marginal         Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24         25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                              Resurgence Indigo Sport
Page 4 of 5                                      Protective Denim                                         motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                         1             2         Average
Moisture Vapour Resistance - Ret        27.6         29.2          28.4
              2
     (kPam /W)
                                         1             2         Average
Thermal Resistance - Rct               0.171         0.166         0.168
          2
      (Km /W)



Water spray and rain resistance
This garment has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                         Resurgence Indigo Sport
Page 5 of 5                                 Protective Denim                               motocap.com.au
