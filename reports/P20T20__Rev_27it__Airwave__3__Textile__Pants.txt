                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Rev'it
                                                           Model                    Airwave 3
                                                           Type                     Pants - Textile
                                                           Date purchased           10 February 2022
                                                           Sizes tested             XL and 2XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $299.95


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating           ★             18.5
                                                           Abrasion                           1/10           0.60
                                                           Burst                              7/10           728
                                                           Impact                             4/10           27.6
                                                           MotoCAP Breathability Rating       ★★            0.399
                                                           Moisture Vapour Resistance          -             30.9
                                                           Thermal Resistance                  -            0.205
                                                           Water resistance                   N/A            N/A
This garment is fitted with impact protectors for the knees and hips. Replacing the knee and hip armour with
higher performing impact protectors would improve the protection levels of this garment. There are no
vents to allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Knee                          
                                                                      Hip                           




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               Rev'it Airwave 3
Page 1 of 5                                     Textile Pants                                   motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.60




Determining Criteria    Area            Good         Acceptable    Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6         3.0 - 5.6    1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5         1.8 - 2.5    0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5          1.0 - 1.5    0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        90%              1.82   0.75     0.66     0.68     0.67     0.73                 0.88   P
Material B        10%              0.51   0.15     0.46     0.26     0.43     0.32                 0.36   P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             0.51   0.15     0.46     0.26     0.43     0.32                 0.36   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       15%            1.82     0.75     0.66     0.68     0.67     0.73                  0.88   M
Material B       85%            0.51     0.15     0.46     0.26     0.43     0.32                  0.36   P
Details of materials used in jacket
Material A       Woven fabric shell with mesh inner liner
Material B       Mesh fabric shell with mesh inner liner




                                                Rev'it Airwave 3
Page 2 of 5                                      Textile Pants                               motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     7/10
                                                                          Burst score       728




Determining Criteria        Unit         Good         Acceptable    Marginal       Poor
Burst strength             (kPa)        > 1000        800 - 1000    500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3       Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 539            653            426            1151          761          635           694         M
Zones 3 & 4 610            742            1071           1034          932          778           861         A




                                                 Rev'it Airwave 3
Page 3 of 5                                       Textile Pants                                motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    4/10
                                                                               Impact score     27.6




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                             23.7      A                                        28.2      M
Maximum force (kN)                             25.9      M                                        31.6      P
Coverage of Zone 1 area                       110%                                               120%
Coverage of Zone after displacement            50%                                               100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre            Mid            Edge              Centre            Mid            Edge
Impact Protector 1           22.0             24.0           24.0               28.1             24.0           29.9
Impact Protector 2           22.4             24.5           25.9               28.1             24.0           31.6
Impact Protector 3           22.5             22.8           25.2               30.0             28.8           29.5



                                                    Rev'it Airwave 3
Page 4 of 5                                          Textile Pants                                       motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★★                                Breathability rating       N/A
Breathability score      0.399                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            30.3            31.5          30.9
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1              2          Average
Without removable liners                            0.216           0.195        0.205
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Rev'it
    Model                           Airwave 3
    Type                            Pants - Textile
    Date purchased                  10 February 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          P20T20
    Rating first published          June 2022
    Rating updated                  23 June 2022


                                               Rev'it Airwave 3
Page 5 of 5                                     Textile Pants                               motocap.com.au
