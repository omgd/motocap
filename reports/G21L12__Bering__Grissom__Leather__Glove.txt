                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Bering
                                                           Model:                       Grissom
                                                           Type:                        Glove - Leather
                                                           Date purchased:              17 June 2022
                                                           Sizes tested:                XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $79.95
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating           ★★          2.3
                                                           Abrasion                            6/10       3.02
                                                           Seam strength                       1/10        3.0
                                                           Impact                              4/10        7.0
                                                           Water resistance                    N/A         N/A
These gloves are fitted with impact protection for the knuckles only. There is no impact protection for the
palms. There is no provision for ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                                Bering Grissom
Page 1 of 5                                      Leather Glove                                     motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      6/10
                                                                     Abrasion score       3.02




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8



Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        50%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        50%              2.72   3.02     2.59     3.80     3.05     3.08                  3.04    A
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        80%              0.61   0.77     0.98     0.74     0.88                           0.80    P
Material D        20%              0.35   0.32     0.09     0.46     0.52                           0.35    P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        10%              2.72   3.02     2.59     3.80     3.05     3.08                  3.04    G
Material D        90%              0.35   0.32     0.09     0.46     0.52                           0.35    P
Details of materials used in glove - derived from manufacturer provided information
Material A       Woven fabric shell over hard-shell armour
Material B       Suede patch, foam layer and suede shell
Material C       Suede shell with fabric inner liner
Material D       Fabric shell with fabric inner liner




                                                Bering Grissom
Page 2 of 5                                      Leather Glove                               motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  1/10
                                                                                     Seam strength score    3.0




Determining Criteria        Unit            Good         Acceptable            Marginal          Poor
Seam tensile strength       (N/mm)          > 11              9 - 11           6 - 8.9           <6
Glove restraint             (N)             > 200           100 - 200          50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5             Average
Zones 1 & 2             9.33         7.74            9.81               8.61              7.23          8.54      M
Zone 3                  9.02         4.17            0.62               6.10              6.48          5.28      P
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5             Average
Wrist restraint         66.1         91.1            88.5               97.5              69.8          82.6      M




                                                    Bering Grissom
Page 3 of 5                                          Leather Glove                                         motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                    Impact Protection Performance
                                                                                    Impact rating    4/10
                                                                                    Impact score      7.0




Determining Criteria                      Unit            Good         Acceptable       Marginal           Poor
Impact force                              (kN)             <2           2 - 4.9            5-8              >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             2.0         A                           P
Maximum force (kN)                             3.2         A                           P
Coverage of zone 1 area                       110%                          0%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                               Palm         No impact protector present
Strike number                   1                2               3                   1                 2
Impact Protector 1              3.2              2.2             1.6
Impact Protector 2              2.6              1.6             1.6
Impact Protector 3              2.0              1.6             2.0




                                                       Bering Grissom
Page 4 of 5                                             Leather Glove                                      motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Bering
   Model                          Grissom
   Type                           Glove - Leather
   Date purchased                 17 June 2022
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G21L12
   Rating first published         August 2022
   Rating updated                 22 August 2022


                                              Bering Grissom
Page 5 of 5                                    Leather Glove                                motocap.com.au
