                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Five
                                                           Model:                  Arizona
                                                           Type:                   Glove - Leather
                                                           Date purchased:         9 March 2020
                                                           Sizes tested:           XL and 2XL
                                                           Gender:                 M
                                                           Style:                  All Purpose
                                                           Test code:              G19L43
                                                           Test Results Summary:
                                                                                          Rating      Result
                                                           MotoCAP Protection Rating                  3.4
                                                           Abrasion                       10/10        6.13
                                                           Seam strength                   1/10         4.7
                                                           Impact                          1/10         1.7
                                                           Water resistance                N/A         N/A
This glove is fitted with impact protection for the knuckles and palm. There is no impact protection for the
wrist area. Perforated leather is present in the fingers, palm and back of the hand to provide continuous
airflow movement within the glove.



Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                     Zone 4

    High risk of impact        High risk of abrasion     Medium risk of abrasion       Low risk of abrasion




                                                 Five Arizona
Page 1 of 5                                     Leather Glove                                motocap.com.au
Abrasion Resistance
The glove was tested for abrasion resistance in accordance with MotoCAP test protocols. The table below
shows the test results for time to abrade to material failure for each sample by Zone, type and area
coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Quilted leather patch over leather shell
Material B:      Perforated leather patch over leather shell
Material C:      Perforated leather shell
Material D:      Quilted leather shell with fabric inner liner
Material E:      Perforated leather shell with fabric inner liner

Zone             Coverage        Abrasion time for each test (s)                                       Average
                 (%)                 1          2          3         4            5             6         (s)
Zone 2 area (High abrasion risk)
Material A       100%              7.23       7.29       8.67       6.64         5.11       6.90         6.97    G

Zone 3 area (Medium abrasion risk)
Material B       50%              7.18         7.43       5.68      4.80         8.00       7.00         6.68    G
Material C       50%              2.12         2.91       3.86      3.69         1.46       3.83         2.98    A
Zone 4 area (Low abrasion risk)
Material D       50%              6.46         7.77       5.35      7.93         6.22                    6.75    G
Material E       50%              3.74         3.21       3.24      1.48         3.46       3.52         3.11    G
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each Zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                           Good        Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk          Zone 2:        > 4.0        2.7 - 4.0    1.2 - 2.6          < 1.2
Medium abrasion risk        Zone 3:        > 3.5        2.5 - 3.5    1.0 - 2.4          < 1.0
Low abrasion risk           Zone 4:        >2.5         1.8 - 2.5    0.8 - 1.7          < 0.8

                                                     Five Arizona
Page 2 of 5                                         Leather Glove                                   motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The table below shows the seam
tensile strength in newtons per millimeter (N/mm) for each seam tested by Zone and the average result for
each Zone.
Seam tensile strength (N/mm)
Area                  1                 2               3                  4                5               Average
Zones 2 & 3           8.75              12.76           9.04               10.65            11.58           10.55     A
Zone 4                16.66             14.24           11.38              16.01            10.51           13.76     A
The table below shows the force required to remove the restrained glove in newtons (N) for each of the five
gloves tested and the average result.
Glove restraint (N)
Glove                 1                 2               3                  4                5               Average
Wrist restraint       181.7             177.7           115.7              156.8            161.6           158.7     P
The diagram below illustrates the tensile strength and wrist restraint results in terms of the likely
performance of the glove in a crash and is a pictorial representation of the data from the tables above.




                                                Good        Acceptable         Marginal             Poor
Determining Criteria
Seam tensile strength         (N/mm)            > 15            10 - 15         6.5 - 9.9           < 6.5
Glove restraint                   (N)           > 400          300 - 400       200 - 299            <200




                                                         Five Arizona
Page 3 of 5                                             Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
table below shows the test results for each strike on each impact protector in kilonewtons (kN) and their
area of coverage in percentage (%) within the Zone.
Impact protector type                       Knuckles                        Palm                        Wrist
Average force             (kN)                10.0        P                  9.9       P                        P
Maximum force             (kN)                 10.0       P                 10.0       P                        P
Coverage of zone 1 area                        90%                          80%                          0%
Impact forces are capped at a maximum of 10.0kN.
Individual test results
Impact force (kN)            Knuckles                                              Palm
Strike number                   1               2               3                   1               2
Impact Protector 1               10.0          10.0           10.0                 10.0            10.0
Impact Protector 2               10.0          10.0           10.0                  9.5            10.0
Impact Protector 3               10.0          10.0           10.0                 10.0            10.0
Impact force (kN)                Wrist     No impact protector present
Strike number                     1             2
Impact Protector 1
Impact Protector 2
Impact Protector 3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good         Acceptable         Marginal       Poor*
Determining Criteria
Knuckle and wrist Impact force (kN)          <2            2 - 4.9           5-8           >8
Palm impact force              (kN)          <4            4 - 5.9           6-8           >8
* Poor may also indicate that no impact protector is present in the glove
Areas shaded black are not considered in the impact protection ratings.

                                                       Five Arizona
Page 4 of 5                                           Leather Glove                                       motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                                Five Arizona
Page 5 of 5                                    Leather Glove                                motocap.com.au
