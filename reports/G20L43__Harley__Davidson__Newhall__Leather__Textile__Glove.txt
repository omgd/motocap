                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Harley Davidson
                                                           Model:                       Newhall
                                                           Type:                        Glove - Leather/Textile
                                                           Date purchased:              19 October 2021
                                                           Sizes tested:                XL and 3XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $104.35
                                                           Test Results Summary:
                                                                                               Rating      Score
                                                           MotoCAP Protection Rating            ⯨           0.8
                                                           Abrasion                            2/10        1.15
                                                           Seam strength                       2/10         5.6
                                                           Impact                              1/10         0.0
                                                           Water resistance                    N/A          N/A
These gloves are not fitted with impact protection. Perforated leather in the palm and fingers, together with
the mesh fabric in the top of the hand and thumb provide continuous airflow within the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles
                                                                             Palm




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                          Harley Davidson Newhall
Page 1 of 5                                Leather/Textile Glove                                  motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      2/10
                                                                     Abrasion score       1.15




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        30%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00 G
Material B        70%              0.70   0.49     0.13     0.48     0.36     0.53                  0.45  P
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        100%             3.34   1.72     1.99     2.05     2.25     0.83                  2.03  M

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material D       60%            1.95     1.38     1.38     1.17     1.50     2.13                  1.58   M
Material E       40%            0.89     0.33     0.42     0.75     0.45     0.84                  0.61   P
Details of materials used in glove - derived from manufacturer provided information
Material A       Leather shell with gel protector
Material B       Thick stretch fabric shell
Material C       Leather shell
Material D       Perforated leather shell
Material E       Fabric shell, foam layer and fabric inner liner




                                          Harley Davidson Newhall
Page 2 of 5                                Leather/Textile Glove                             motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  2/10
                                                                                     Seam strength score    5.6




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             6.97         3.41            9.46               6.84              8.37           7.01      M
Zone 3                  8.65         5.97            8.23               7.30              8.38           7.70      M
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         172.0        123.8           160.2              166.0             152.9          155.0     A




                                              Harley Davidson Newhall
Page 3 of 5                                    Leather/Textile Glove                                        motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    1/10
                                                                                   Impact score      0.0




Determining Criteria                      Unit           Good         Acceptable       Marginal           Poor
Impact force                              (kN)            <2            2 - 4.9           5-8              >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                                         P                          P
Maximum force (kN)                                         P                          P
Coverage of zone 1 area                           0%                        0%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles     No impact protector present              Palm         No impact protector present
Strike number                   1               2               3                   1                 2
Impact Protector 1
Impact Protector 2
Impact Protector 3




                                                 Harley Davidson Newhall
Page 4 of 5                                       Leather/Textile Glove                                   motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Harley Davidson
   Model                          Newhall
   Type                           Glove - Leather/Textile
   Date purchased                 19 October 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L43
   Rating first published         December 2021
   Rating updated                 10 December 2021



                                         Harley Davidson Newhall
Page 5 of 5                               Leather/Textile Glove                             motocap.com.au
