                                                          This MotoCAP safety rating applies to:
                                                          Brand:                   Rhino Leather
                                                          Model:                   Healy
                                                          Type:                    Glove - Leather
                                                          Date purchased:          5 May 2023
                                                          Sizes tested:            L, 2XL and 3XL
                                                          Test glove gender:       Male
                                                          Style:                   All Purpose
                                                          RRP:                     $99.99
                                                          Test Results Summary:
                                                                                         Rating      Score
                                                          MotoCAP Protection Rating       ★★           4.1
                                                          Abrasion                        10/10       7.33
                                                          Seam strength                    7/10       10.0
                                                          Impact                           1/10        0.0
                                                          Water resistance                10/10        0.0
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                        Impact protection
                                                                        Knuckles          ✓
                                                                        Palm              ✓




              Zone 1                    Zone 2                      Zone 3

       High risk of impact       High risk of abrasion     Medium risk of abrasion
      High risk of abrasion


                                             Rhino Leather Healy
Page 1 of 5                                     Leather Glove                                  motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                       Abrasion Resistance Performance
                                                                       Abrasion rating    10/10
                                                                       Abrasion score      7.33




Determining Criteria   Area            Good         Acceptable       Marginal      Poor
High abrasion risk     Zone 1 & 2      > 4.0         2.7 - 4.0   1.2 - 2.6        < 1.2
Medium abrasion risk   Zone 3           2.5          1.8 - 2.5   0.8 - 1.7        < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material A       70%             10.00  10.00    10.00    10.00    10.00    10.00                10.00     G
Material B       30%              8.76  10.88     9.72     9.02     5.90     9.99                 9.04     G
Zone 2           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material B       35%              8.76  10.88     9.72     9.02     5.90     9.99                 9.04     G
Material C       65%              4.54   3.95     9.10     1.69     1.96     2.65                 3.98     A
Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material B       15%              8.76  10.88     9.72     9.02     5.90     9.99                 9.04     G
Material C       85%              4.54   3.95     9.10     1.69     1.96     2.65                 3.98     G
Details of materials used in glove - derived from manufacturer provided information
Material A      Hard-shell armour over foam layer, water-resistant layer and mesh inner liner
Material B      Leather patch over leather shell, water-resistant layer and mesh inner liner
Material C      Leather shell, water-resistant layer and mesh inner liner




                                               Rhino Leather Healy
Page 2 of 5                                       Leather Glove                               motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates
the tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash
and is a pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating 7/10
                                                                                     Seam strength score  10.0




Determining Criteria        Unit             Good          Acceptable          Marginal           Poor
Seam tensile strength       (N/mm)           > 11            9 - 11            6 - 8.9            <6
Glove restraint             (N)              > 200         100 - 200           50 - 99            <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2                 3                4                 5              Average
Zones 1 & 2             9.51         10.30             12.25            8.74              13.52          10.86       A
Zone 3                  4.84         5.37              13.17            8.71              11.48          8.72        M
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2                 3                4                 5              Average
Wrist restraint         308.1        242.4             185.3            245.0             200.5          236.3       G




                                                     Rhino Leather Healy
Page 3 of 5                                             Leather Glove                                            motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered in the impact protection
ratings.




                                                                                 Impact Protection Performance
                                                                                 Impact rating   1/10
                                                                                 Impact score     0.0




Determining Criteria                     Unit            Good       Acceptable       Marginal         Poor
Impact force                             (kN)             <2          2 - 4.9           5-8           >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces
are capped at a maximum of 10.0kN.
Impact protector type                       Knuckles                     Palm
Average force (kN)                            10.0         P             10.0       P
Maximum force (kN)                            10.0         P             10.0       P
Coverage of zone 1 area                      100%                        60%

Individual test results: - The table below shows the test results for each strike on each impact protector in
kilonewtons (kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type       Knuckles                                             Palm
Strike number                  1                 2              3                 1               2
Impact Protector 1             10.0             10.0                             10.0
Impact Protector 2             10.0             10.0                             10.0
Impact Protector 3             10.0             10.0                             10.0




                                                     Rhino Leather Healy
Page 4 of 5                                             Leather Glove                                    motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the
wetting proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove           Water absorbed by cotton glove
                 Volume (ml) Percentage (%)        Volume (ml) Percentage (%)
Pair 1               0.00            0.00              0.00            0.00
Pair 2               0.19            0.07              0.19            0.86
Average              0.09            0.04              0.09            0.43
Location of wetting:
There was no visible wetting to the cotton under-glove in all four of the gloves tested.




   Assessment Details.
   Brand                          Rhino Leather
   Model                          Healy
   Type                           Glove - Leather
   Date purchased                 5 May 2023
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G23L18
   Rating first published         June 2023
   Rating updated                 19 June 2023

                                             Rhino Leather Healy
Page 5 of 5                                     Leather Glove                                motocap.com.au
