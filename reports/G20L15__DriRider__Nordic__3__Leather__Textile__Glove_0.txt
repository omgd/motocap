                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       DriRider
                                                           Model:                       Nordic 3
                                                           Type:                        Glove - Leather/Textile
                                                           Date purchased:              8 February 2021
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       Tourer
                                                           RRP:                         $69.95
                                                           Test Results Summary:
                                                                                               Rating      Score
                                                           MotoCAP Protection Rating           ★★           2.8
                                                           Abrasion                            10/10       5.15
                                                           Seam strength                        1/10        4.9
                                                           Impact                               1/10        0.0
                                                           Water resistance                     1/10       33.8
These gloves are fitted with impact protection for the knuckles only. There is no impact protection for the
palms. There is no provision for ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                              DriRider Nordic 3
Page 1 of 5                                 Leather/Textile Glove                                  motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating     10/10
                                                                      Abrasion score       5.15




Determining Criteria    Area             Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2       > 4.0       2.7 - 4.0      1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3            2.5        1.8 - 2.5      0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A        45%             10.00  10.00    10.00    10.00    10.00    10.00                  10.00     G
Material B        55%             10.00  10.00    10.00    10.00    10.00    10.00                  10.00     G
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material C        65%              4.19   3.25     4.25     4.40     3.91     4.33                   4.06     G
Material D        35%              0.70   0.60     0.89     0.78     0.87     0.64                   0.75     P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material C        35%              4.19   3.25     4.25     4.40     3.91     4.33                   4.06     G
Material D        65%              0.70   0.60     0.89     0.78     0.87     0.64                   0.75     P
Details of materials used in glove - derived from manufacturer provided information
Material A       Woven fabric patch over foam layer, water-resistant layer and fabric inner liner
Material B       Leather patch over leather shell, water-resistant layer, foam layer and fabric inner liner
Material C       Leather shell, water-resistant layer, foam layer and fabric inner liner
Material D       Woven fabric shell, water-resistant layer, foam layer and fabric inner liner




                                              DriRider Nordic 3
Page 2 of 5                                 Leather/Textile Glove                             motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  1/10
                                                                                     Seam strength score    4.9




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11           6 - 8.9            <6
Glove restraint             (N)              > 200          100 - 200          50 - 99            <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             16.00        16.00           6.82               9.01              8.22           11.21     G
Zone 3                  16.00        16.00           11.47              9.92              13.98          13.48     G
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         104.1        69.3            57.3               69.2              103.2          80.6      M




                                                  DriRider Nordic 3
Page 3 of 5                                     Leather/Textile Glove                                       motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximium
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    1/10
                                                                                   Impact score      0.0




Determining Criteria                      Unit           Good         Acceptable       Marginal           Poor
Knuckle Impact force                      (kN)            <2            2 - 4.9           5-8              >8
Palm impact force                         (kN)            <4            4 - 5.9           6-8              >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             8.6         P                          P
Maximum force (kN)                             9.5         P                          P
Coverage of zone 1 area                       100%                          0%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm         No impact protector present
Strike number                   1                2              3                   1                 2
Impact Protector 1              9.5              7.9            8.4
Impact Protector 2              9.2              8.3            8.7
Impact Protector 3              8.6              8.7            7.9




                                                   DriRider Nordic 3
Page 4 of 5                                      Leather/Textile Glove                                    motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove            Water absorbed by cotton glove
                 Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Pair 1               255.1           126%               64.5            304%
Pair 2               249.9           123%               70.8            329%
Average              252.5           124%               67.7            317%
Location of wetting:
Visible wetting to the cotton under-glove was present over the entire hand in all four of the gloves tested.




   Assessment Details.
   Brand                          DriRider
   Model                          Nordic 3
   Type                           Glove - Leather/Textile
   Date purchased                 8 February 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L15
   Rating first published         September 2021
   Rating updated                 14 September 2021



                                              DriRider Nordic 3
Page 5 of 5                                 Leather/Textile Glove                            motocap.com.au
