                                                            This MotoCAP safety rating applies to:
                                                            Brand:                   Dainese
                                                            Model:                   Blackjack
                                                            Type:                    Glove - Leather
                                                            Date purchased:          29 October 2018
                                                            Sizes tested:            XL
                                                            Gender:                  M&F
                                                            Style:                   All Purpose
                                                            Test code:               G18L16
                                                            Test Results Summary:
                                                                                            Rating      Result
                                                            MotoCAP Protection Rating                    1.7
                                                            Abrasion                         5/10        2.87
                                                            Seam strength                    1/10         5.0
                                                            Impact                           1/10         0.8
                                                            Water resistance                 N/A         N/A
This glove is fitted with impact protectors for the knuckles, there is no provision for impact protection for
the palms and wrist. There is perforated leather on the tops of the fingers to allow airflow cooling in hot
weather.



Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                       Zone 3                      Zone 4

    High risk of impact        High risk of abrasion     Medium risk of abrasion        Low risk of abrasion




                                               Dianese Blackjack
Page 1 of 5                                     Leather Gloves                                 motocap.com.au
Abrasion Resistance
The glove was tested for abrasion resistance in accordance with MotoCAP test protocols. The table below
shows the test results for time to abrade to material failure for each sample by Zone, type and area
coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Leather patch, leather shell and fabric inner liner
Material B:      Leather shell and fabric liner liner




Zone             Coverage        Abrasion time for each test (s)                                          Average
                 (%)                 1          2          3            4            5             6         (s)
Zone 2 area (High abrasion risk)
Material A       20%               4.01       3.46       6.47          5.62         2.91       3.72         4.36    G
Material B       80%               2.97       2.32       3.11          1.68         3.84       3.14         2.84    A
Zone 3 area (Medium abrasion risk)
Material A       40%               4.01       3.46       6.47          5.62         2.91       3.72         4.36    G
Material B       60%               2.97       2.32       3.11          1.68         3.84       3.14         2.84    A
Zone 4 area (Low abrasion risk)
Material A       50%               4.01       3.46       6.47          5.62         2.91       3.72         4.36    G
Material B       50%               2.97       2.32       3.11          1.68         3.84       3.14         2.84    G
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each Zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                           Good         Acceptable      Marginal           Poor
Determining Criteria
High abrasion risk          Zone 2:        > 4.0         2.7 - 4.0      1.2 - 2.6          < 1.2
Medium abrasion risk        Zone 3:         3.5          2.5 - 3.5      1.0 - 2.4          < 1.0
Low abrasion risk           Zone 4:        >2.5          1.8 - 2.5      0.8 - 1.7          < 0.8

                                                   Dianese Blackjack
Page 2 of 5                                         Leather Gloves                                     motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The table below shows the seam
tensile strength in newtons per millimeter (N/mm) for each seam tested by Zone and the average result for
each Zone.
Seam tensile strength (N/mm)
Area                  1                 2                 3                  4                   5               Average
Zones 2 & 3           13.54             9.86              12.05              8.09                9.58            10.62     A
Zone 4                14.71             13.26             6.34               4.25                12.76           10.26     A
The table below shows the force required to remove the restrained glove in newtons (N) for each of the five
gloves tested and the average result.
Glove restraint (N)
Glove                 1                 2                 3                  4                   5               Average
Wrist restraint       129.9             92.0              80.4               332.6               310.6           189.1     P
The diagram below illustrates the tensile strength and wrist restraint results in terms of the likely
performance of the glove in a crash and is a pictorial representation of the data from the tables above.




                                                Good          Acceptable            Marginal             Poor
Determining Criteria
Seam tensile strength         (N/mm)            > 15              10 - 15            6.5 - 9.9           < 6.5
Glove restraint                   (N)           > 400            300 - 400          200 - 299            <200




                                                        Dianese Blackjack
Page 3 of 5                                              Leather Gloves                                             motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
table below shows the test results for each strike on each impact protector in kilonewtons (kN) and their
area of coverage in percentage (%) within the Zone.
Impact protector type                       Knuckles                        Palm                   Wrist
Average force             (kN)                9.8         P                            P                      P
Maximum force             (kN)                 10.0       P                            P                      P
Coverage of zone 1 area                        80%                          0%                      0%
Impact forces are capped at a maximum of 10.0kN.
Individual test results
Impact force (kN)            Knuckles                                              Palm    No impact protector present
Strike location                 A               B              C                    A            B
Impact Protector 1                9.7          9.1            10.0
Impact Protector 2
Impact Protector 3
Impact force (kN)                Wrist    No impact protector present
Strike location                   A             B
Impact Protector 1
Impact Protector 2
Impact Protector 3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good         Acceptable         Marginal       Poor*
Determining Criteria
Knuckle and wrist Impact force (kN)          <2            2 - 4.9           5-8           >8
Palm impact force              (kN)          <4            4 - 5.9           6-8           >8
* Poor may also indicate that no impact protector is present in the glove
Areas shaded black are not considered in the impact protection ratings.

                                                    Dianese Blackjack
Page 4 of 5                                          Leather Gloves                                  motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                             Dianese Blackjack
Page 5 of 5                                   Leather Gloves                                motocap.com.au
