                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Klim
                                                           Model:                       Badlands Aero Pro Short
                                                           Type:                        Glove - Leather
                                                           Date purchased:              16 May 2022
                                                           Sizes tested:                L, XL and 3XL
                                                           Test glove gender:           Male
                                                           Style:                       Tourer
                                                           RRP:                         $255.00
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating           ★★          2.7
                                                           Abrasion                            5/10       2.85
                                                           Seam strength                       5/10        8.5
                                                           Impact                              5/10        9.3
                                                           Water resistance                    N/A         N/A
This glove is fitted with impact protectors for the knuckles and palm areas. Perforated leather on the wrist
and fingers provides continuous airflow within the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles         
                                                                             Palm             




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                        Klim Badlands Aero Pro Short
Page 1 of 5                                    Leather Glove                                      motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      5/10
                                                                     Abrasion score       2.85




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8



Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        70%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00 G
Material B        30%              1.63   1.94     1.17     1.82     1.35     0.98                  1.48  M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             1.63   1.94     1.17     1.82     1.35     0.98                  1.48  M

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material C       20%            4.84     3.53     5.41     4.29     5.35     4.36                  4.63   G
Material B       80%            1.63     1.94     1.17     1.82     1.35     0.98                  1.48   M
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell
Material B       Perforated leather shell
Material C       Leather shell, foam layer and fabric inner liner




                                       Klim Badlands Aero Pro Short
Page 2 of 5                                   Leather Glove                                  motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  5/10
                                                                                     Seam strength score    8.5




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             8.69         12.83           12.05              15.44             17.76          13.35     G
Zone 3                  7.84         15.51           14.10              6.88              8.12           10.49     A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         74.4         148.0           96.5               187.8             185.8          138.5     A




                                             Klim Badlands Aero Pro Short
Page 3 of 5                                         Leather Glove                                           motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    5/10
                                                                                   Impact score      9.3




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9            5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             1.8        G                  7.1      M
Maximum force (kN)                             2.6        A                  9.1      P
Coverage of zone 1 area                       130%                          50%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                2              3                   1                2
Impact Protector 1              1.5              1.5            1.5                 7.1              6.2
Impact Protector 2              2.3              2.6            1.7                 6.1              9.1
Impact Protector 3              1.5              1.5            2.2                 5.7              8.2




                                            Klim Badlands Aero Pro Short
Page 4 of 5                                        Leather Glove                                           motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Klim
   Model                          Badlands Aero Pro Short
   Type                           Glove - Leather
   Date purchased                 16 May 2022
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G21L06
   Rating first published         September 2022
   Rating updated                 26 September 2022


                                       Klim Badlands Aero Pro Short
Page 5 of 5                                   Leather Glove                                 motocap.com.au
