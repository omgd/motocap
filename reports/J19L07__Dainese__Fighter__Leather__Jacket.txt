                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Dainese
                                                           Model:                  Fighter
                                                           Type:                   Jacket - Leather
                                                           Date purchased:         20 January 2019
                                                           Sizes tested:           54 and 56
                                                           Gender:                 M
                                                           Style:                  Sports
                                                           Test code:              J19L07
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating                 54.6
                                                           Abrasion                         7/10        5.13
                                                           Burst                           10/10       1434
                                                           Impact                           7/10        48.6
                                                           MotoCAP Comfort Rating            ⯨         0.144
                                                           Moisture Vapour Resistance                  126.7
                                                           Thermal Resistance                          0.304
                                                           Water resistance                 N/A         N/A
This garment is fitted with impact protectors for the elbows and shoulders. Pockets are provided at the
back for aftermarket impact protectors. There are vents in the sides of the jacket to allow airflow cooling in
hot weather. The thermal comfort rating is based on tests of the breathability of the garment when all vents
are closed. The thermal comfort of this product may be better when the vents can be opened.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact

                                               Dainese Fighter
Page 1 of 5                                    Leather Jacket                                 motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Leather shell, foam layer and mesh inner liner
Material B:      Leather shell, mesh layer and mesh inner liner
Material C:      Stretch fabric shell and mesh inner liner




Zone             Coverage       Abrasion time for each test (seconds)                                 Average
                 (%)                1          2          3          4           5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       20%              6.34       6.20       5.66       6.52         6.64                    6.27      G
Material B       80%              5.62       4.89       6.04       4.67         5.07       5.79         5.35      A
Zone 3 area (Medium abrasion risk)
Material B       75%              5.62       4.89       6.04       4.67         5.07       5.79         5.35      G
Material C       25%              1.36       1.09       1.06       1.13         1.15                    1.16      M
Zone 4 area (Low abrasion risk)
Material B       90%              5.62       4.89       6.04       4.67         5.07       5.79         5.35      G
Material C       10%              1.36       1.09       1.06       1.13         1.15                    1.16      A
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                          Good        Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:       > 5.6        3.0 - 5.6    1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:        > 2.5        1.8 - 2.5    0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:        >1.5         1.0 - 1.5    0.4 - 0.9          < 0.4

                                                  Dainese Fighter
Page 2 of 5                                       Leather Jacket                                   motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2                3                  4                 5              Average
Zones 1 & 2          1090           1591             1933               960               1915           1498      G
Zone EZ             1581            1105             999                1898              1087           1334      G
Zones 3 & 4         1947            1937             1242               1565              846            1507      G
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good            Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000          800 - 1000         500 - 799          < 500




                                                    Dainese Fighter
Page 3 of 5                                         Leather Jacket                                         motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The table below shows the test results for each strike on each impact protector in kilonewtons (kN) and
their area of coverage as a proportion (%) of the Zone.
Impact protector type                        Elbow                                            Shoulder
Average force (kN)                            19.9        A                                     21.2         A
Maximum force (kN)                            22.9        A                                        22.6      A
Coverage of zone 1 area                      150%                                                  90%
Coverage of zone after displacement          100%                                                  90%
Individual test results
Impact force (kN)             Elbow                                           Shoulder
Strike location                 A              B               C                 A                  B             C
Impact Protector 1             18.7           21.8            18.2                  19.4           22.6          22.3
Impact Protector 2             16.2           22.9            22.0                  19.9           21.8          21.3
Impact Protector 3             22.6           15.2            21.7                  19.7           21.9          22.3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good         Acceptable       Marginal         Poor*
Determining Criteria
Impact force                    (kN)        < 15           15 - 24        25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                                     Dainese Fighter
Page 4 of 5                                          Leather Jacket                                       motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                         1             2         Average
Moisture Vapour Resistance - Ret       120.9         132.5        126.7
              2
     (kPam /W)
                                         1             2         Average
Thermal Resistance - Rct               0.301         0.307        0.304
          2
      (Km /W)



Water spray and rain resistance
This garment has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                               Dainese Fighter
Page 5 of 5                                    Leather Jacket                              motocap.com.au
