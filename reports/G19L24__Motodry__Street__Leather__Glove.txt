                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  MotoDry
                                                           Model:                  Street
                                                           Type:                   Glove - Leather
                                                           Date purchased:         9 August 2019
                                                           Sizes tested:           XL
                                                           Gender:                 M&F
                                                           Style:                  Sports
                                                           Test code:              G19L24
                                                           Test Results Summary:
                                                                                          Rating      Result
                                                           MotoCAP Protection Rating                  2.3
                                                           Abrasion                        7/10        3.50
                                                           Seam strength                   1/10         4.9
                                                           Impact                          1/10         3.5
                                                           Water resistance                N/A         N/A
This glove is fitted with impact protection for the knuckles. There is no impact protection for the palm and
wrist areas. Perforated leather in the fingers and vents in the knuckles provide continuous airflow
movement within the glove.



Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                     Zone 4

    High risk of impact        High risk of abrasion     Medium risk of abrasion       Low risk of abrasion




                                               MotoDry Street
Page 1 of 5                                    Leather Glove                                 motocap.com.au
Abrasion Resistance
The glove was tested for abrasion resistance in accordance with MotoCAP test protocols. The table below
shows the test results for time to abrade to material failure for each sample by Zone, type and area
coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Leather shell with fabric inner liner
Material B:      Quilted leather shell with fabric inner liner
Material C:      Perforated leather shell with fabric inner liner




Zone             Coverage        Abrasion time for each test (s)                                      Average
                 (%)                 1          2          3         4            5             6        (s)
Zone 2 area (High abrasion risk)
Material A       50%               3.67       3.40       2.86       3.49                    1.42         2.97   A
Material B       50%               6.96       3.46       5.44       2.88                    7.28         5.21   G
Zone 3 area (Medium abrasion risk)
Material B       40%               6.96       3.46       5.44       2.88                    7.28         5.21   G
Material C       60%               0.29       0.85       0.87       0.88         0.03       0.51         0.57   P
Zone 4 area (Low abrasion risk)
Material A       100%              3.67       3.40       2.86       3.49                    1.42         2.97   G


Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each Zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                           Good        Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk          Zone 2:        > 4.0        2.7 - 4.0    1.2 - 2.6          < 1.2
Medium abrasion risk        Zone 3:         3.5         2.5 - 3.5    1.0 - 2.4          < 1.0
Low abrasion risk           Zone 4:        >2.5         1.8 - 2.5    0.8 - 1.7          < 0.8

                                                   MotoDry Street
Page 2 of 5                                        Leather Glove                                    motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The table below shows the seam
tensile strength in newtons per millimeter (N/mm) for each seam tested by Zone and the average result for
each Zone.
Seam tensile strength (N/mm)
Area                  1                 2                3                  4                   5               Average
Zones 2 & 3           9.22              9.28             10.62              15.77               11.13           11.20     A
Zone 4                9.43              8.96             9.21               7.21                9.54            8.87      M
The table below shows the force required to remove the restrained glove in newtons (N) for each of the five
gloves tested and the average result.
Glove restraint (N)
Glove                 1                 2                3                  4                   5               Average
Wrist restraint       273.4             212.9            261.2              98.3                105.8           190.3     P
The diagram below illustrates the tensile strength and wrist restraint results in terms of the likely
performance of the glove in a crash and is a pictorial representation of the data from the tables above.




                                                Good         Acceptable            Marginal             Poor
Determining Criteria
Seam tensile strength         (N/mm)            > 15             10 - 15            6.5 - 9.9           < 6.5
Glove restraint                   (N)           > 400           300 - 400          200 - 299            <200




                                                        MotoDry Street
Page 3 of 5                                             Leather Glove                                              motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
table below shows the test results for each strike on each impact protector in kilonewtons (kN) and their
area of coverage in percentage (%) within the Zone.
Impact protector type                       Knuckles                        Palm                   Wrist
Average force             (kN)                1.23        G                            P                      P
Maximum force             (kN)                 1.48       G                            P                      P
Coverage of zone 1 area                       120%                          0%                      0%
Impact forces are capped at a maximum of 10.0kN.
Individual test results
Impact force (kN)            Knuckles                                              Palm    No impact protector present
Strike number                   1               2               3                   1            2
Impact Protector 1               1.27          1.00            1.48
Impact Protector 2               1.11          1.45            1.21
Impact Protector 3               1.02          1.37            1.14
Impact force (kN)                Wrist    No impact protector present
Strike number                     1             2
Impact Protector 1
Impact Protector 2
Impact Protector 3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good          Acceptable        Marginal       Poor*
Determining Criteria
Knuckle and wrist Impact force (kN)          <2             2 - 4.9          5-8           >8
Palm impact force              (kN)          <4             4 - 5.9          6-8           >8
* Poor may also indicate that no impact protector is present in the glove
Areas shaded black are not considered in the impact protection ratings.

                                                      MotoDry Street
Page 4 of 5                                           Leather Glove                                  motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                              MotoDry Street
Page 5 of 5                                   Leather Glove                                 motocap.com.au
