                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Dainese
                                                           Model                    Racing 4 perforated
                                                           Type                     Leather jacket
                                                           Date purchased           8 May 2024
                                                           Sizes tested             44 and 48
                                                           Test garment gender      Female
                                                           Style                    Sports
                                                           RRP                      $999.00


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating         ★★★             49.5
                                                           Abrasion                            7/10          5.48
                                                           Burst                              10/10         1400
                                                           Impact                              4/10          26.9
                                                           MotoCAP Breathability Rating        ★            0.255
                                                           Moisture Vapour Resistance          -             53.2
                                                           Thermal Resistance                  -            0.226
                                                           Water resistance                   N/A            N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Perforated leather is located in the arms and chest to allow airflow movement
through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                         Dainese Racing 4 perforated
Page 1 of 5                                    Leather Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      7/10
                                                                     Abrasion score       5.48




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        40%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        60%              3.76   2.62     4.06     5.78     3.71     4.40                  4.06    A
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        70%              3.76   2.62     4.06     5.78     3.71     4.40                  4.06    G
Material C        30%              2.85   3.16     3.61     2.94     6.87     4.83                  4.04    G
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        100%             2.85   3.16     3.61     2.94     6.87     4.83                  4.04    G


Details of materials used in jacket
Material A       Hard-shell armour over leather shell
Material B       Leather shell with mesh inner liner
Material C       Perforated leather shell with mesh inner liner




                                        Dainese Racing 4 perforated
Page 2 of 5                                   Leather Jacket                                 motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1400




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1653           1214           1179          1482          1681          1278          1414        G
Zones 3 & 4 1999           1323           1165          1200          1328          1038          1342        G




                                         Dainese Racing 4 perforated
Page 3 of 5                                    Leather Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    4/10
                                                                               Impact score     26.9




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Elbow                                         Shoulder
Average force (kN)                             14.9      G                                    21.4         A
Maximum force (kN)                             24.5      A                                    28.5         M
Coverage of Zone 1 area                        80%                                            80%
Coverage of Zone after displacement            70%                                            80%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre             Mid           Edge             Centre             Mid           Edge
Impact Protector 1            7.1              11.6          11.4              17.5              16.0          12.5
Impact Protector 2           15.8              13.4          12.7              28.5              21.0          19.8
Impact Protector 3           17.7              24.5          19.5              26.3              27.4          24.2



                                             Dainese Racing 4 perforated
Page 4 of 5                                        Leather Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ★                               Breathability rating       N/A
Breathability score      0.255                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            53.1            53.3          53.2
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1              2          Average
Without removable liners                            0.226           0.226        0.226
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Dainese
    Model                           Racing 4 perforated
    Type                            Leather jacket
    Date purchased                  8 May 2024
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J24L18
    Rating first published          June 2024
    Rating updated                  27 June 2024


                                         Dainese Racing 4 perforated
Page 5 of 5                                    Leather Jacket                               motocap.com.au
