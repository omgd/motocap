                                                                 This MotoCAP safety rating applies to:
                                                                 Brand                      Komine
                                                                 Model                      JK-574 F-Touring RAMA II
                                                                 Type                       Jacket - Textile
                                                                 Date purchased             9 January 2023
                                                                 Sizes tested               XL
                                                                 Test garment gender        Male
                                                                 Style                      Tourer
                                                                 RRP                        $549.00
                                                                 Test Results Summary               Rating        Score
                                                                 MotoCAP Protection Rating            ★            23.5
                                                                 Abrasion                            1/10          0.44
                                                                 Burst                              10/10         1067
                                                                 Impact                              5/10          35.5
                                                                 MotoCAP Breathability Rating         ⯨           0.043
                                                                 Moisture Vapour Resistance            -          536.2
                                                                 Thermal Resistance                    -          0.387
                                                                 Water resistance                    1/10          39.2
This garment is fitted with impact protectors for the elbows, shoulders and back. Pockets are provided for aftermarket
chest protectors. Replacing the elbow and shoulder armour with higher performing impact protectors would improve
the protection levels of this garment. There are zipped vents in the chest, arms and back to allow controlled airflow
movement through the garment. The long vent in the arm increases risk of burst failure when open more than 180mm.
The breathability rating is based on tests of the garment's materials when all vents are closed. The breathability of this
product may be better when the vents can be opened. This garment has a removable water-resistant liner. The
breathability rating above was achieved with the thermal and water-resistant liners removed. When tested with the
water-resistant liner installed, the breathability rating reduced but remained within half a star.


Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.

                                                                             Removable liners
                                                                             Thermal liner                   ✓
                                                                             Water resistant liner           ✓

                                                                             Removable impact protection
                                                                                             Pockets Armour
                                                                             Elbow             ✓       ✓
                                                                             Shoulder          ✓       ✓
                                                                             Back              ✓       ✓




           Zone 1                        Zone 2                         Zone 3                         Zone 4

   High risk of abrasion          High risk of abrasion       Medium risk of abrasion           Low risk of abrasion



Page 1 of 5                       Komine JK-574 F-Touring RAMA II Textile Jacket                            motocap.com.au
    High risk of impact
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                   Abrasion Resistance Performance
                                                                   Abrasion rating     1/10
                                                                   Abrasion score      0.44




Determining Criteria      Area          Good       Acceptable    Marginal        Poor
High abrasion risk        Zone 1 & 2    > 5.6       3.0 - 5.6    1.3 - 2.9       < 1.3
Medium abrasion risk      Zone 3        > 2.5       1.8 - 2.5    0.8 - 1.7       < 0.8
Low abrasion risk         Zone 4        >1.5        1.0 - 1.5    0.4 - 0.9       < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zone 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%             0.59   0.31     0.68     0.38     0.43     0.26                0.44  P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%           0.59     0.31     0.68     0.38     0.43     0.26                0.44  P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%           0.59     0.31     0.68     0.38     0.43     0.26                0.44  M


Details of materials used in jacket
Material A      Woven fabric shell with mesh inner liner




Page 2 of 5                     Komine JK-574 F-Touring RAMA II Textile Jacket               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is
a pictorial representation of the data from the table below.




                                                                        Burst Strength Performance
                                                                        Burst rating    10/10
                                                                        Burst score     1067




Determining Criteria       Unit         Good        Acceptable    Marginal       Poor
Burst strength            (kPa)        > 1000       800 - 1000    500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2       Sample 3      Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 1221          963            651           1443          894          1398          1095       G
Zones 3 & 4 822           732            967           1027          899          1270          953        A




Page 3 of 5                   Komine JK-574 F-Touring RAMA II Textile Jacket                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                             Impact Protection Performance
                                                                             Impact rating   5/10
                                                                             Impact score    35.5




Determining Criteria        Unit           Good        Acceptable      Marginal        Poor*
Impact force                (kN)             < 15       15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each
impact protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.
Individual strike results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                        Shoulder
Average force (kN)                            23.0     A                                    25.2          M
Maximum force (kN)                            26.0     M                                    28.7          M
Coverage of Zone 1 area                      120%                                          100%
Coverage of Zone after displacement          100%                                          100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at
a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                          Shoulder
Strike location             Centre            Mid          Edge             Centre             Mid            Edge
Impact Protector 1           26.0             21.8         23.4              24.9              23.8           26.1
Impact Protector 2           23.8             22.8         22.4              24.2              28.7           27.3
Impact Protector 3           22.2             22.1         22.3              22.0              24.3           25.3



Page 4 of 5                     Komine JK-574 F-Touring RAMA II Textile Jacket                        motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                With water-resistant liner
Breathability rating     ⯨                              Breathability rating       ⯨
Breathability score    0.043                            Breathability score      0.034

Moisture Vapour Resistance - Ret (kPa.m2/W)          1            2         Average
Without removable liners                           521.3        551.2        536.2
With water-resistant liner                         872.1        753.6        812.8
                             2
Thermal Resistance - Rct (K.m /W)                    1            2         Average
Without removable liners                           0.399        0.376        0.387
With water-resistant liner                         0.445        0.465        0.455

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the
wetting proportion (%) of the garment and undergarments due to water absorption.
              Water absorbed by garment          Water absorbed by underwear
              Volume (ml) Percentage (%)         Volume (ml) Percentage (%)
Jacket 1          896             41%                115             40%
Jacket 2          682             32%                112             39%
Average           789             36%                113             39%

Location of wetting
There was major wetting to the cotton underwear present at the cuffs of the sleeves for both jackets
tested.




   Assessment Details.
   Brand                            Komine
   Model                            JK-574 F-Touring RAMA II
   Type                             Jacket - Textile
   Date purchased                   9 January 2023
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J23T02
   Rating first published           March 2023
   Rating updated                   20 March 2023



Page 5 of 5                      Komine JK-574 F-Touring RAMA II Textile Jacket               motocap.com.au
