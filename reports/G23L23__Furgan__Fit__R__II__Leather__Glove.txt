                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Furygan
                                                           Model:                       Fit R II
                                                           Type:                        Glove - Leather
                                                           Date purchased:              5 May 2023
                                                           Sizes tested:                L and XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $155.00
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating          ★★★          3.6
                                                           Abrasion                            9/10       4.75
                                                           Seam strength                       6/10        9.9
                                                           Impact                              5/10        8.1
                                                           Water resistance                    N/A         N/A
This glove is fitted with impact protectors for the knuckles and palm areas. Perforated leather on the fingers,
together with vents in the knuckle impact protectors, provides continuous airflow movement through the
glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm              




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                                Furgan Fit R II
Page 1 of 5                                     Leather Glove                                      motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      9/10
                                                                     Abrasion score       4.75




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        45%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        55%              4.60   5.06     5.64     5.45     4.85     5.80                  5.24    G
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        50%              4.60   5.06     5.64     5.45     4.85     5.80                  5.24    G
Material C        50%              1.73   1.87     0.25     0.87     3.68     2.20                  1.77    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        35%              4.60   5.06     5.64     5.45     4.85     5.80                  5.24    G
Material C        65%              1.73   1.87     0.25     0.87     3.68     2.20                  1.77    M
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell
Material B       Suede leather patch over leather shell with fabric inner liner
Material C       Leather shell with fabric inner liner




                                                Furgan Fit R II
Page 2 of 5                                     Leather Glove                                motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                    Seam Strength Performance
                                                                                    Seam strength rating  6/10
                                                                                    Seam strength score    9.9




Determining Criteria        Unit             Good        Acceptable           Marginal           Poor
Seam tensile strength       (N/mm)           > 11          9 - 11              6 - 8.9           <6
Glove restraint             (N)              > 200       100 - 200             50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                 4                 5              Average
Zones 1 & 2             6.71         15.04           14.83             13.20             8.78           11.71     G
Zone 3                  2.72         11.07           16.66             8.62              8.10           9.43      A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                 4                 5              Average
Wrist restraint         145.7        165.7           225.9             199.8             179.2          183.3     A




                                                     Furgan Fit R II
Page 3 of 5                                          Leather Glove                                         motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                      Impact Protection Performance
                                                                                      Impact rating    5/10
                                                                                      Impact score      8.1




Determining Criteria                      Unit            Good           Acceptable          Marginal         Poor
Impact force                              (kN)             <2             2 - 4.9             5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                        Palm
Average force (kN)                             1.4         G                  5.1        M
Maximum force (kN)                             1.7         G                  6.3        M
Coverage of zone 1 area                       100%                           20%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                                 Palm
Strike number                   1                2               3                     1                2
Impact Protector 1              1.4              1.2             1.2                   6.3              5.6
Impact Protector 2              1.7              1.4             1.4                   4.5              5.0
Impact Protector 3              1.4              1.5             1.3                   5.0              4.3




                                                       Furgan Fit R II
Page 4 of 5                                            Leather Glove                                          motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Furygan
   Model                          Fit R II
   Type                           Glove - Leather
   Date purchased                 5 May 2023
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G23L23
   Rating first published         September 2023
   Rating updated                 15 September 2023

                                               Furgan Fit R II
Page 5 of 5                                    Leather Glove                                motocap.com.au
