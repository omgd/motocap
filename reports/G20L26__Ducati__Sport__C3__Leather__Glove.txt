                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Ducati
                                                           Model:                       Sport C3
                                                           Type:                        Glove - Leather/Textile
                                                           Date purchased:              23 March 2021
                                                           Sizes tested:                M, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       Sports
                                                           RRP:                         $171.82
                                                           Test Results Summary:
                                                                                               Rating      Score
                                                           MotoCAP Protection Rating            ★★          2.9
                                                           Abrasion                            7/10        3.82
                                                           Seam strength                       1/10         1.8
                                                           Impact                              5/10         9.6
                                                           Water resistance                    N/A          N/A
This glove is fitted with impact protectors for the knuckles and palm areas. Perforated leather in the fingers,
provides continuous airflow within the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm              




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                               Ducati Sport C3
Page 1 of 5                                 Leather/Textile Glove                                  motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      7/10
                                                                     Abrasion score       3.82




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        50%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        50%              4.74   3.69     3.18     3.54     2.71     0.09                  2.99    A
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        25%              4.74   3.69     3.18     3.54     2.71     0.09                  2.99    A
Material C        75%              2.04   1.67     1.30     1.13     1.67     1.39                  1.53    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        55%              4.74   3.69     3.18     3.54     2.71     0.09                  2.99    G
Material D        45%              1.53   3.43     2.50     1.86     1.18     0.99                  1.92    A
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell
Material B       Suede patch over leather shell
Material C       Fabric shell
Material D       Perforated leather and foam layer over fabric shell




                                              Ducati Sport C3
Page 2 of 5                                Leather/Textile Glove                             motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                    Seam Strength Performance
                                                                                    Seam strength rating  1/10
                                                                                    Seam strength score    1.8




Determining Criteria        Unit            Good        Acceptable            Marginal          Poor
Seam tensile strength       (N/mm)          > 11             9 - 11           6 - 8.9           <6
Glove restraint             (N)             > 200          100 - 200          50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2              3                  4                 5             Average
Zones 1 & 2             9.44         6.70           6.22               8.26              7.80          7.68      M
Zone 3                  6.88         6.75           7.58               5.78              3.80          6.16      M
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2              3                  4                 5             Average
Wrist restraint         54.8         44.1           53.1               43.3              57.8          52.1      M




                                                  Ducati Sport C3
Page 3 of 5                                    Leather/Textile Glove                                      motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    5/10
                                                                                   Impact score      9.6




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9            5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             1.4        G                  6.2      M
Maximum force (kN)                             1.7        G                  6.5      M
Coverage of zone 1 area                       100%                          80%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                2              3                   1                2
Impact Protector 1              1.3              1.3            1.5                 6.3              6.0
Impact Protector 2              1.0              1.7            1.4                 6.2              6.0
Impact Protector 3              1.5              1.6            1.3                 6.5              6.4




                                                    Ducati Sport C3
Page 4 of 5                                      Leather/Textile Glove                                     motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Ducati
   Model                          Sport C3
   Type                           Glove - Leather/Textile
   Date purchased                 23 March 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L26
   Rating first published         September 2021
   Rating updated                 14 September 2021



                                              Ducati Sport C3
Page 5 of 5                                Leather/Textile Glove                            motocap.com.au
