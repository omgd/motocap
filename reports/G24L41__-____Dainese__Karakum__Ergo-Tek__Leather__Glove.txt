                                                            This MotoCAP safety rating applies to:
                                                           Brand:                       Dainese
                                                           Model:                       Karakum Ergo-Tek
                                                           Type:                        Glove - Leather
                                                           Date purchased:              19 June 2024
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       Adventure
                                                           RRP:                         $179.00
                                                            Test Results Summary:
                                                                                              Rating       Score
                                                           MotoCAP Protection Rating          ★★             2.8
                                                           Abrasion                           7/10          3.54
                                                           Seam strength                      7/10          10.2
                                                           Impact                             3/10           6.6
                                                           Water resistance                   N/A           N/A
This glove is fitted with impact protectors for the knuckles and palm areas. Mesh fabric on the back of the
gloves provides continuous airflow within the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles         ✓
                                                                             Palm             ✓




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                          Dainese Karakum Ergo-Tek
Page 1 of 5                                    Leather Glove                                      motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      7/10
                                                                     Abrasion score       3.54




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        20%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material C        80%              1.69   0.71     2.05     4.01     2.88     3.36                  2.45    M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        20%              8.11   8.50     5.56     8.90     9.27     5.57                  7.65    G
Material C        80%              1.69   0.71     2.05     4.01     2.88     3.36                  2.45    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        27%              8.11   8.50     5.56     8.90     9.27     5.57                  7.65    G
Material D        73%              1.78   1.80     2.22     1.24     3.09                           2.02    A
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over mesh fabric shell with mesh inner liner
Material B       Leather patch over leather shell with mesh inner liner
Material C       Leather shell with mesh inner liner
Material D       Mesh fabric shell with mesh inner liner




                                         Dainese Karakum Ergo-Tek
Page 2 of 5                                   Leather Glove                                  motocap.com.au
Seam Tensile Strength
The tensile strength of the glove's seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                   Seam Strength Performance
                                                                                   Seam strength rating  7/10
                                                                                   Seam strength score   10.2




Determining Criteria        Unit             Good        Acceptable          Marginal           Poor
Seam tensile strength       (N/mm)           > 11          9 - 11             6 - 8.9           <6
Glove restraint             (N)              > 200       100 - 200            50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                4                 5              Average
Zones 1 & 2             17.60        16.41           21.76            14.94             19.07          17.96     G
Zone 3                  7.09         16.77           19.25            19.87             13.77          15.35     G
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                4                 5              Average
Wrist restraint         139.3        120.4           126.1            95.4              121.5          120.5     A




                                              Dainese Karakum Ergo-Tek
Page 3 of 5                                        Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    3/10
                                                                                   Impact score      6.6




Determining Criteria                      Unit           Good         Acceptable       Marginal          Poor
Impact force                              (kN)            <2            2 - 4.9            5-8           >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             2.1         A                 8.5       P
Maximum force (kN)                             2.3         A                10.0       P
Coverage of zone 1 area                       100%                          80%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                  2             3                  1              2
Impact Protector 1              1.5               1.9           2.3                 7.3           10.0
Impact Protector 2              1.9               2.0           2.3                 7.1           8.2
Impact Protector 3              2.3               2.3           2.1                 10.0          8.4




                                                 Dainese Karakum Ergo-Tek
Page 4 of 5                                           Leather Glove                                      motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Dainese
   Model                          Karakum Ergo-Tek
   Type                           Glove - Leather
   Date purchased                 19 June 2024
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G24L41
   Rating first published         September 2024
   Rating updated                 5 September 2024


                                         Dainese Karakum Ergo-Tek
Page 5 of 5                                   Leather Glove                                 motocap.com.au
