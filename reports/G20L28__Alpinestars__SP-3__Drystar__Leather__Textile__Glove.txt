                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Alpinestars
                                                           Model:                       SP-3 Drystar
                                                           Type:                        Glove - Leather/Textile
                                                           Date purchased:              20 April 2021
                                                           Sizes tested:                L,XL and XXL
                                                           Test glove gender:           Male
                                                           Style:                       Tourer
                                                           RRP:                         $79.95
                                                           Test Results Summary:
                                                                                               Rating      Score
                                                           MotoCAP Protection Rating            ★           1.7
                                                           Abrasion                            6/10        3.39
                                                           Seam strength                       1/10         0.7
                                                           Impact                              1/10         0.0
                                                           Water resistance                    7/10         5.3
These gloves are not fitted with impact protection. There is no provision for ventilation to allow airflow
movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles
                                                                             Palm




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                           Alpinestars SP-3 Drystar
Page 1 of 5                                 Leather/Textile Glove                                 motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                             Abrasion Resistance Performance
                                                                             Abrasion rating      6/10
                                                                             Abrasion score       3.39




Determining Criteria       Area              Good         Acceptable       Marginal         Poor
High abrasion risk         Zone 1 & 2        > 4.0         2.7 - 4.0      1.2 - 2.6         < 1.2
Medium abrasion risk       Zone 3             2.5          1.8 - 2.5      0.8 - 1.7         < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                         Average
Material A        60%             10.00  10.00    10.00    10.00    10.00    10.00                            10.00        G
Material B        40%              4.22   1.36     2.15     2.21     1.85     2.92                             2.45        M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                         Average
Material B        60%              4.22   1.36     2.15     2.21     1.85     2.92                             2.45        M
Material C        40%              0.93   0.68     0.54     0.33     1.20     0.62                             0.72        P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                         Average
Material D        20%              2.47   1.21     5.49     6.51     5.29     5.54                             4.42        G
Material C        80%              0.93   0.68     0.54     0.33     1.20     0.62                             0.72        P
Details of materials used in glove - derived from manufacturer provided information
Material A         Woven fabric and foam patch over fabric shell, water-resistance layer, fibre wadding and fabric liner
Material B         Leather shell, water-resistance liner and fabric inner liner
Material C         Woven fabric shell, water-resistance liner and fabric inner liner
Material D         Suede leather patch over leather shell, water-resistance liner and fabric inner liner




                                               Alpinestars SP-3 Drystar
Page 2 of 5                                     Leather/Textile Glove                                   motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  1/10
                                                                                     Seam strength score    0.7




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             10.16        12.49           8.95               9.23              12.57          10.68     A
Zone 3                  7.50         10.03           11.50              14.37             14.73          11.63     G
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         10.5         12.4            10.3               12.1              16.1           12.3      P




                                               Alpinestars SP-3 Drystar
Page 3 of 5                                     Leather/Textile Glove                                       motocap.com.au
Impact Protection
These gloves were not tested for impact protection as impact protection was not fitted to the gloves. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    1/10
                                                                                   Impact score      0.0




Determining Criteria                      Unit           Good         Acceptable       Marginal           Poor
Knuckle Impact force                      (kN)            <2            2 - 4.9           5-8              >8
Palm impact force                         (kN)            <4            4 - 5.9           6-8              >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                                         P                          P
Maximum force (kN)                                         P                          P
Coverage of zone 1 area                          0%                         0%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles     No impact protector present              Palm         No impact protector present
Strike number                   1               2               3                   1                 2
Impact Protector 1
Impact Protector 2
Impact Protector 3




                                                 Alpinestars SP-3 Drystar
Page 4 of 5                                       Leather/Textile Glove                                   motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove           Water absorbed by cotton glove
                 Volume (ml)    Percentage (%)     Volume (ml)    Percentage (%)
Pair 1                30.7           16%                10.5           45%

Average               30.7           16%                10.5           45%
Location of wetting:
Visible wetting to the cotton under-glove was present at the wrist of one of the two gloves tested.




    Assessment Details.
   Brand                          Alpinestars
   Model                          SP-3 Drystar
   Type                           Glove - Leather/Textile
   Date purchased                 20 April 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L28
   Rating first published         October 2021
   Rating updated                 25 October 2021



                                           Alpinestars SP-3 Drystar
Page 5 of 5                                 Leather/Textile Glove                           motocap.com.au
