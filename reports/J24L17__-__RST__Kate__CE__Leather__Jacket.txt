                                                          This MotoCAP safety rating applies to:
                                                          Brand                   RST
                                                          Model                   Kate CE
                                                          Type                    Leather Jacket
                                                          Date purchased          8 May 2024
                                                          Sizes tested            9 and 10
                                                          Test garment gender     Female
                                                          Style                   All Purpose
                                                          RRP                     $549.95


                                                          Test Results Summary           Rating        Score
                                                          MotoCAP Protection Rating      ★★             37.2
                                                          Abrasion                          5/10        3.82
                                                          Burst                             9/10        981
                                                          Impact                            4/10        27.5
                                                          MotoCAP Breathability Rating      ★          0.238
                                                          Moisture Vapour Resistance         -          65.3
                                                          Thermal Resistance                 -         0.259
                                                          Water resistance                  N/A         N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Replacing the elbow and shoulder armour with higher performing impact
protectors would improve the protection levels of this garment. Perforated leather is located in the arms
and chest together with a zipped vent in the back to allow airflow movement through the garment. The
breathability rating is based on tests of the garment's materials when all vents are closed. The
breathability of this product may be better when the vents can be opened. Breathability was measured
without the removable thermal liner installed. There is the potential for burns from heat transferred
through the metal snap fasteners on the wrist of the jacket during a slide.
Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                    Removable liners
                                                                    Thermal liner                  ✓
                                                                    Water-resistant liner

                                                                    Removable impact protection
                                                                                   Pockets Armour
                                                                    Elbow            ✓       ✓
                                                                    Shoulder         ✓       ✓
                                                                    Back             ✓




          Zone 1                     Zone 2                      Zone 3                      Zone 4

   High risk of abrasion      High risk of abrasion    Medium risk of abrasion       Low risk of abrasion
    High risk of impact
                                                 RST Kate CE
Page 1 of 5                                     Leather Jacket                                     motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                    Abrasion Resistance Performance
                                                                    Abrasion rating     5/10
                                                                    Abrasion score      3.82




Determining Criteria    Area            Good       Acceptable     Marginal      Poor
High abrasion risk      Zones 1 & 2     > 5.6       3.0 - 5.6     1.3 - 2.9    < 1.3
Medium abrasion risk    Zone 3          > 2.5       1.8 - 2.5     0.8 - 1.7    < 0.8
Low abrasion risk       Zone 4          >1.5        1.0 - 1.5     0.4 - 0.9    < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2      Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6           Average
Material A       70%             10.00   9.66     2.14    10.00    10.00     3.45               7.54       G
Material B       30%              2.39   1.82     3.05     2.03     2.39     3.84               2.59       M
Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6           Average
Material B       65%              2.39   1.82     3.05     2.03     2.39     3.84               2.59       G
Material C       35%              1.28   1.00     0.98     1.46     1.13     1.04               1.15       M
Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6           Average
Material B       60%              2.39   1.82     3.05     2.03     2.39     3.84               2.59       G
Material C       40%              1.28   1.00     0.98     1.46     1.13     1.04               1.15       A
Details of materials used in jacket
Material A      Quilted leather shell with fabric inner liner
Material B      Leather shell with fabric inner liner
Material C      Stretch fabric shell with fabric inner liner




                                                  RST Kate CE
Page 2 of 5                                      Leather Jacket                              motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is
a pictorial representation of the data from the table below.




                                                                       Burst Strength Performance
                                                                       Burst rating    9/10
                                                                       Burst score      981




Determining Criteria       Unit        Good        Acceptable    Marginal        Poor
Burst strength            (kPa)        > 1000      800 - 1000    500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for
each sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2      Sample 3      Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 1404          799           1237          977           826          941           1031       G
Zones 3 & 4 677           545           723           569           1482         685           780        M




                                                 RST Kate CE
Page 3 of 5                                     Leather Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average
or maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                            Impact Protection Performance
                                                                            Impact rating   4/10
                                                                            Impact score    27.5




Determining Criteria        Unit          Good        Acceptable      Marginal        Poor*
Impact force                (kN)           < 15         15 - 24       25 - 30         > 30

* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the
Zone.
Impact protector type                      Elbow                                         Shoulder
Average force (kN)                          24.8       A                                   24.4       A
Maximum force (kN)                          27.2       M                                   28.2       M
Coverage of Zone 1 area                     95%                                            90%
Coverage of Zone after displacement         80%                                            90%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped
at a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type      Elbow                                           Shoulder
Strike location            Centre          Mid             Edge             Centre            Mid         Edge
Impact Protector 1          27.2           24.7            23.2              22.3             23.3        28.2
Impact Protector 2          25.5           24.0            25.3              23.4             23.4        25.6
Impact Protector 3          24.8           23.7            25.1              23.4             25.0        25.2



                                                     RST Kate CE
Page 4 of 5                                         Leather Jacket                                   motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★                                 Breathability rating   N/A
Breathability score    0.238                               Breathability score    N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)          1               2         Average
Without removable liners                            64.1            66.6         65.3
With water-resistant liner                          N/A             N/A       N/A
Thermal Resistance - Rct (K.m2/W)                    1               2         Average
Without removable liners                           0.266           0.253        0.259
With water-resistant liner                          N/A             N/A       N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            RST
   Model                            Kate CE
   Type                             Leather Jacket
   Date purchased                   8 May 2024
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J24L17
   Rating first published           July 2024
   Rating updated                   22 July 2024


                                                   RST Kate CE
Page 5 of 5                                       Leather Jacket                            motocap.com.au
