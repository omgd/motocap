                                                          This MotoCAP safety rating applies to:
                                                          Brand                   Richa
                                                          Model                   Kodi Legging
                                                          Type                    Pants - Textile
                                                          Date purchased          20 February 2023
                                                          Sizes tested            30
                                                          Test garment gender     Female
                                                          Style                   All Purpose
                                                          RRP                     $369.00


                                                          Test Results Summary           Rating         Score
                                                          MotoCAP Protection Rating         ★            27.6
                                                          Abrasion                          1/10         0.93
                                                          Burst                             7/10         726
                                                          Impact                            7/10         52.2
                                                          MotoCAP Breathability Rating   ★★★            0.444
                                                          Moisture Vapour Resistance         -           33.2
                                                          Thermal Resistance                 -          0.245
                                                          Water resistance                  N/A          N/A
This garment is fitted with impact protectors for the knees and hips. There are no vents to allow airflow
movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                    Removable liners
                                                                    Thermal liner
                                                                    Water-resistant liner

                                                                    Removable impact protection
                                                                                   Pockets Armour
                                                                    Knee                    
                                                                    Hip                     




         Zone 1                      Zone 2                     Zone 3                       Zone 4

   High risk of abrasion      High risk of abrasion     Medium risk of abrasion      Low risk of abrasion
    High risk of impact
                                              Richa Kodi Legging
Page 1 of 5                                      Textile Pants                                     motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating     1/10
                                                                      Abrasion score      0.93




Determining Criteria   Area            Good         Acceptable      Marginal      Poor
High abrasion risk     Zones 1 & 2     > 5.6        3.0 - 5.6       1.3 - 2.9    < 1.3
Medium abrasion risk   Zone 3          > 2.5        1.8 - 2.5       0.8 - 1.7    < 0.8
Low abrasion risk      Zone 4          >1.5         1.0 - 1.5       0.4 - 0.9    < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2      Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       70%              3.59   2.75     2.62     2.45                                 2.85   M
Material B       30%              0.68   0.49     0.78     0.66     0.71     0.71               0.67   P
Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material B       100%             0.68   0.49     0.78     0.66     0.71     0.71               0.67   P

Zone 4          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material B      100%           0.68     0.49     0.78     0.66     0.71     0.71                0.67   M


Details of materials used in pant
Material A      Knitted fabric shell, para-aramid layer and fabric impact protector pocket
Material B      Knitted fabric shell with para-aramid inner liner




                                               Richa Kodi Legging
Page 2 of 5                                       Textile Pants                              motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram
below illustrates the burst strength results in terms of the likely performance of the garment in an impact
and is a pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     7/10
                                                                          Burst score      726




Determining Criteria       Unit        Good         Acceptable      Marginal      Poor
Burst strength            (kPa)       > 1000       800 - 1000       500 - 799     < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2      Sample 3       Sample 4        Sample 5    Sample 6    Average
Zones 1 & 2 617           646           829            794             922         630         739       M
Zones 3 & 4 613           704           777            468             723         749         672       M




                                               Richa Kodi Legging
Page 3 of 5                                       Textile Pants                               motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating   7/10
                                                                               Impact score    52.2




Determining Criteria        Unit           Good          Acceptable      Marginal       Poor*
Impact force                (kN)             < 15         15 - 24        25 - 30        > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the
Zone.
Impact protector type                         Knee                                               Hip
Average force (kN)                             20.0      A                                       18.4     A
Maximum force (kN)                             21.4      A                                       18.8     A
Coverage of Zone 1 area                       105%                                              130%
Coverage of Zone after displacement           100%                                              100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at
a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Knee                                                Hip
Strike location             Centre            Mid            Edge              Centre           Mid           Edge
Impact Protector 1           18.9             20.0           20.8               18.2            18.0          18.7
Impact Protector 2           19.5             21.4           19.0               18.6            18.4          18.1
Impact Protector 3           20.0             19.5           20.7               18.8            18.7          18.4




                                                    Richa Kodi Legging
Page 4 of 5                                            Textile Pants                                    motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating  ★★★                                  Breathability rating      N/A
Breathability score    0.444                               Breathability score       N/A

                                       2
Moisture Vapour Resistance - Ret (kPa.m /W)          1               2         Average
Without removable liners                            33.1            33.2        33.2
With water-resistant liner                          N/A             N/A          N/A

Thermal Resistance - Rct (K.m2/W)                    1               2         Average
Without removable liners                           0.244           0.247        0.245
With water-resistant liner                          N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Richa
   Model                            Kodi Legging
   Type                             Pants - Textile
   Date purchased                   20 February 2023
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           P23T04
   Rating first published           May 2023
   Rating updated                   26 May 2023


                                               Richa Kodi Legging
Page 5 of 5                                       Textile Pants                            motocap.com.au
