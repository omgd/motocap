                                                           This MotoCAP safety rating applies to:
                                                           Brand                    KTM
                                                           Model                    Terra ADV
                                                           Type                     Pants - Textile
                                                           Date purchased           22 September 2023
                                                           Sizes tested             XL and 2XL
                                                           Test garment gender      Male
                                                           Style                    Tourer
                                                           RRP                      $511.15


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating          ★★           33.8
                                                           Abrasion                            2/10        1.33
                                                           Burst                              10/10       1298
                                                           Impact                              6/10        47.4
                                                           MotoCAP Breathability Rating       ★★          0.300
                                                           Moisture Vapour Resistance           -          57.2
                                                           Thermal Resistance                   -         0.286
                                                           Water resistance                   9/10          1.4
This garment is fitted with impact protectors for the knees and hips. There are zipped vents to the front of
thigh to allow controlled airflow movement through the garment. The breathability rating is based on tests
of the garment's materials when all vents are closed. The breathability of this product may be better when
the vents can be opened. This garment has a removable water-resistant liner. The breathability rating above
was achieved with the thermal and water-resistant liners removed. When tested with the water-resistant
liner installed, the breathability rating reduced to 1 star.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                   
                                                                      Water-resistant liner           

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Knee                        
                                                                      Hip                         




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               KTM Terra ADV
Page 1 of 5                                     Textile Pants                                   motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      2/10
                                                                     Abrasion score       1.33




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        40%              1.86   1.54     1.86     2.00     1.64     1.01                 1.65     M
Material B        60%              0.86   0.85     1.05     0.84     0.79     1.24                 0.94     P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        50%              4.52   6.65     5.55     3.46     8.34     4.04                 5.43     G
Material D        50%              0.80   0.44     3.99     0.27     1.01     0.35                 1.14     M
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material D        80%              0.80   0.44     3.99     0.27     1.01     0.35                 1.14     A
Material B        20%              0.86   0.85     1.05     0.84     0.79     1.24                 0.94     M
Details of materials used in pant
Material A       Coarse woven fabric shell with mesh inner liner
Material B       Woven fabric shell with mesh inner liner
Material C       Leather shell with mesh inner liner
Material D       Stretch fabric shell with mesh inner liner




                                                KTM Terra ADV
Page 2 of 5                                      Textile Pants                               motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1298




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 842            867            1738          1677          1117          1825          1344        G
Zones 3 & 4 640            1623           1364          1037          922           1102          1115        G




                                                 KTM Terra ADV
Page 3 of 5                                       Textile Pants                                motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    6/10
                                                                               Impact score     47.4




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                             12.2      G                                        21.0      A
Maximum force (kN)                             17.8      A                                        23.9      A
Coverage of Zone 1 area                       100%                                               130%
Coverage of Zone after displacement            70%                                               100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre            Mid            Edge              Centre            Mid            Edge
Impact Protector 1           10.3             12.8           15.9               23.4             19.0           19.8
Impact Protector 2            9.4             11.4           12.7               23.9             18.7           21.5
Impact Protector 3            8.9             10.5           17.8               19.3             19.9           23.2



                                                     KTM Terra ADV
Page 4 of 5                                           Textile Pants                                      motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★★                                Breathability rating        ★
Breathability score      0.300                             Breathability score       0.222

Moisture Vapour Resistance - Ret (kPa.m2/W)           1               2         Average
Without removable liners                            56.4            58.1          57.2
With water-resistant liner                          64.9            61.0          62.9

Thermal Resistance - Rct (K.m2/W)                     1               2         Average
Without removable liners                            0.294           0.278        0.286
With water-resistant liner                          0.235           0.230        0.232

Water spray and rain resistance
This pants are advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment        Water absorbed by underwear
                Volume (ml)    Percentage (%)    Volume (ml)    Percentage (%)
Pants 1             218             13%                3             1.3%
Pants 2              90              6%                4             1.4%
Average             154              9%                3             1.4%




    Assessment Details.
    Brand                           KTM
    Model                           Terra ADV
    Type                            Pants - Textile
    Date purchased                  22 September 2023
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          P24T04
    Rating first published          January 2024
    Rating updated                  10 January 2024

                                                KTM Terra ADV
Page 5 of 5                                      Textile Pants                               motocap.com.au
