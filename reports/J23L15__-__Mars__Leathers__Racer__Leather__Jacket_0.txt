                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Mars Leathers
                                                           Model                    Racer
                                                           Type                     Jacket - Leather
                                                           Date purchased           1 June 2023
                                                           Sizes tested             44 and 46
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $350.00


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating          ★★             32.6
                                                           Abrasion                            6/10          4.23
                                                           Burst                              10/10         1142
                                                           Impact                              1/10           0.0
                                                           MotoCAP Breathability Rating        ★            0.254
                                                           Moisture Vapour Resistance          -             55.5
                                                           Thermal Resistance                  -            0.235
                                                           Water resistance                   N/A            N/A
This garment is fitted with impact protectors for the elbows, shoulders and back. The limb impact protectors
in this garment were found to provide negligible impact protection. Replacing the elbow and shoulder
armour with higher performing impact protectors would improve the protection levels of this garment.
There are zipped vents in the chest, arms and back to allow controlled airflow movement through the
garment. The breathability rating is based on tests of the garment's materials when all vents are closed. The
breathability of this product may be better when the vents can be opened. Breathability was measured
without the removable thermal liner installed.
Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                   
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back                          




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                             Mars Leathers Racer
Page 1 of 5                                    Leather Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      6/10
                                                                     Abrasion score       4.23




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6          3.0 - 5.6   1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5          1.8 - 2.5   0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5           1.0 - 1.5   0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             4.05   4.64     3.88     3.76     5.44     3.64                 4.23   A

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           4.05     4.64     3.88     3.76     5.44     3.64                  4.23   G

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           4.05     4.64     3.88     3.76     5.44     3.64                  4.23   G


Details of materials used in jacket
Material A       Leather shell with mesh inner liner




                                            Mars Leathers Racer
Page 2 of 5                                   Leather Jacket                                 motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1142




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 699            1094           1061          1545          1254          1678          1222        G
Zones 3 & 4 469            849            672           632           1282          1028          822         A




                                             Mars Leathers Racer
Page 3 of 5                                    Leather Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    1/10
                                                                               Impact score      0.0




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            49.0       P                                    50.0         P
Maximum force (kN)                            50.0       P                                    50.0         P
Coverage of Zone 1 area                      125%                                            125%
Coverage of Zone after displacement          100%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           49.6             50.0           47.9              50.0              50.0          50.0
Impact Protector 2           50.0             50.0           44.7              50.0              50.0          50.0
Impact Protector 3           48.9             50.0           50.0              50.0              50.0          50.0



                                                Mars Leathers Racer
Page 4 of 5                                       Leather Jacket                                        motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ★                               Breathability rating       N/A
Breathability score      0.254                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            55.9            55.1          55.5
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.244           0.226        0.235
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Mars Leathers
    Model                           Racer
    Type                            Jacket - Leather
    Date purchased                  1 June 2023
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J23L15
    Rating first published          September 2023
    Rating updated                  4 September 2023


                                              Mars Leathers Racer
Page 5 of 5                                     Leather Jacket                              motocap.com.au
