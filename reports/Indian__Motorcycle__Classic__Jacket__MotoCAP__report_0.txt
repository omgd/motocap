                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Indian Motorcycles
                                                           Model:                  Classic Jacket 2
                                                           Type:                   Jacket - Leather
                                                           Date purchased:         23 July 2018
                                                           Sizes tested:           L and 2XL
                                                           Gender:                 M
                                                           Style:                  Cruiser
                                                           Test code:              J18L08
                                                           Test Results Summary:
                                                                                           Rating      Result
                                                           MotoCAP Protection Rating                  42.1
                                                           Abrasion                         4/10        3.48
                                                           Burst                           10/10       1577
                                                           Impact                           4/10        29.7
                                                           MotoCAP Comfort Rating                    0.294
                                                           Moisture Vapour Resistance                   52.9
                                                           Thermal Resistance                          0.259
                                                           Water Resistance                 N/A
This garment is fitted with impact protectors for the elbows and shoulders, but without a pocket for the
addition of a back protector. There are no ventilation ports for air flow control within the jacket to aid
cooling in hot weather.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact



Page 1 of 5                           Indian Motorcycle Classic Jacket 2                   motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance following the MotoCAP test protocols. The table below
shows the test results for time to abrade through all layers of the materials. Calculated for each sample by
Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Single layer of leather outer and fabric inner liner




Zone             Coverage      Abrasion time for each test (s)                                             Average
                 (%)                1         2          3           4              5             6           (s)
Zone 1 and 2 areas (High abrasion risk)
Material A       100%             2.91          2.90     3.27       2.65           6.45       2.69            3.48     A
                 0%
Zone 3 area (Medium abrasion risk)
Material A       100%                2.91       2.90     3.27       2.65           6.45       2.69            3.48     G
                 0%
Zone 4 area (Low abrasion risk)
Material A       100%                2.91       2.90     3.27       2.65           6.45       2.69            3.48     G
                 0%
The diagram below is a visual indication of the likely abrasion performance of the materials in each Zone
calculated from the data in the table above.




                                            Good       Acceptable       Marginal          Poor
Determining Criteria
High abrasion risk       Zone 1/2:          > 5.6      3.0 - 5.6     1.3 - 2.9            < 1.3
Medium abrasion risk      Zone 3:           > 2.5      1.8 - 2.5     0.8 - 1.7            < 0.8
Low abrasion risk         Zone 4:           >1.5       1.0 - 1.5     0.4 - 0.9            < 0.4




Page 2 of 5                             Indian Motorcycle Classic Jacket 2                            motocap.com.au
Burst Strength
The garment’s burst strength was tested following the MotoCAP test protocols. The table below shows the
burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each Zone.


Burst pressure (kPA)
Area                 1              2               3                4                 5              Average
Zones 1 & 2         1839            1945            1358             1965              1432           1708      G
Zone EZ             1170            1535            1673             1333              1359           1414      G
Zones 3 & 4         1275            1298            1721             1959              1951           1641      G
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good         Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000       800 - 1000       500 - 799            < 500




Page 3 of 5                             Indian Motorcycle Classic Jacket 2                            motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage following the MotoCAP test protocols. The
table below shows the test results for each strike on each impact protector in kilonewton (kN) and their area
of coverage in percentage (%) within the Zone.
Impact protector type                        Elbow                                           Shoulder
Average force                                 23.6       A                                     25.0       M
Maximum force                                 29.4       M                                        29.1    M
Coverage of zone 1 area                      110%                                                 95%
Coverage of zone after displacement           80%                                                 90%
Individual test results
Impact force (kN)             Elbow                                          Shoulder
Strike location                 A              B              C                 A                  B           C
Impact Protector 1             19.3           24.5           24.7                   23            24.1        25.1
Impact Protector 2             19.8           24.2           26.3                  24.7           24.1        25.2
Impact Protector 3             18.7           25.5           29.4                  25.6           24.4        29.1
The diagram below is a visual indication of the likely impact performance of each impact protector
calculated from the data in the table above.




                                            Good        Acceptable      Marginal          Poor*
Determining Criteria
Burst strength                  (kN)        < 15          15 - 24        25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment




Page 4 of 5                             Indian Motorcycle Classic Jacket 2                          motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                         1            2          Average
Moisture Vapour Resistance - Ret        53.0         52.8         52.9
     (kPam2/W)
                                         1             2         Average
Thermal Resistance - Rct               0.291         0.228        0.259
      (Km2/W)


Water spray and rain resistance
This garment has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




Page 5 of 5                         Indian Motorcycle Classic Jacket 2                 motocap.com.au
