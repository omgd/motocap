                                                            This MotoCAP safety rating applies to:
                                                            Brand:                  Draggin Jeans
                                                            Model:                  Cargo
                                                            Type:                   Pants - Denim
                                                            Date purchased:         7 August 2019
                                                            Sizes tested:           38 and 40
                                                            Gender:                 M
                                                            Style:                  All Purpose
                                                            Test code:              P19D04
                                                            Test Results Summary:
                                                                                            Rating      Score
                                                            MotoCAP Protection Rating                   19.5
                                                            Abrasion                         3/10        2.30
                                                            Burst                            8/10        801
                                                            Impact                           0/10         0.0
                                                            MotoCAP Comfort Rating                   0.500
                                                            Moisture Vapour Resistance                   17.9
                                                            Thermal Resistance                          0.149
                                                            Water resistance                 N/A         N/A
This garment is fitted with pockets at the knees for aftermarket impact protectors. Pockets are not provided
at the hips for aftermarket impact protectors. There are no vents to allow airflow movement through the
garment.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                         Zone 3                    Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion         Low risk of abrasion
    High risk of impact

                                               Draggin Jeans Cargo
Page 1 of 5                                       Denim Jeans                                  motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Cotton fabric shell, aramid fabric layer and mesh inner liner
Material B:      Cotton fabric shell




Zone             Coverage      Abrasion time for each test (seconds)                                     Average
                 (%)                1         2          3          4               5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       100%             4.10      3.26       2.95       3.02             4.36       4.54         3.71      A

Zone 3 area (Medium abrasion risk)
Material B       100%             0.26           0.15     0.16       0.20          0.19       0.16         0.19      P

Zone 4 area (Low abrasion risk)
Material B       100%                 0.26       0.15     0.16       0.20          0.19       0.16         0.19      P


Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                             Good       Acceptable     Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:          > 5.6      3.0 - 5.6      1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:           > 2.5      1.8 - 2.5      0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:           >1.5       1.0 - 1.5      0.4 - 0.9          < 0.4

                                                 Draggin Jeans Cargo
Page 2 of 5                                         Denim Jeans                                       motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2               3                  4                 5             Average
Zones 1 & 2          748            1068            901                1035              677           886       A
Zone EZ             636             795             757                1039              748           795       M
Zones 3 & 4         880             632             650                412               648           645       M
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good           Acceptable          Marginal         Poor
Determining Criteria
Burst strength              (kPa)          > 1000         800 - 1000         500 - 799         < 500




                                               Draggin Jeans Cargo
Page 3 of 5                                       Denim Jeans                                            motocap.com.au
Impact Protection
This garment was not tested for impact protection as impact protectors were not provided with the
garment. The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and their area of coverage as a proportion (%) of the Zone.
Impact protector type                        Knee                                                 Hip
Average force (kN)                                       P                                                 P
Maximum force (kN)                                       P                                                 P
Coverage of zone 1 area                       0%                                                  0%
Coverage of zone after displacement           0%                                                  0%
Individual test results
Impact force (kN)             Knee       No impact protector present                Hip   No impact protector present
Strike location                A               B              C                      A          B              C
Impact Protector 1
Impact Protector 2
Impact Protector 3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good        Acceptable        Marginal        Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24         25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                                Draggin Jeans Cargo
Page 4 of 5                                        Denim Jeans                                          motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                         1             2           Average
Moisture Vapour Resistance - Ret        18.0          17.7          17.9
              2
     (kPam /W)
                                         1             2           Average
Thermal Resistance - Rct               0.146         0.151          0.149
          2
      (Km /W)



Water spray and rain resistance
This garment has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                             Draggin Jeans Cargo
Page 5 of 5                                     Denim Jeans                                motocap.com.au
