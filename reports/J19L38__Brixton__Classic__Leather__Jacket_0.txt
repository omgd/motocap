                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Brixton
                                                           Model                    Classic
                                                           Type                     Jacket - Leather
                                                           Date purchased           23 November 2020
                                                           Sizes tested             2XL and 3XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $379.00


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating          ★★           41.9
                                                           Abrasion                            7/10        4.97
                                                           Burst                              10/10       1140
                                                           Impact                              3/10        18.6
                                                           MotoCAP Breathability Rating      ★★★          0.424
                                                           Moisture Vapour Resistance          -           32.0
                                                           Thermal Resistance                  -          0.227
                                                           Water resistance                   N/A          N/A
This garment is fitted with impact protectors for the elbows, shoulders and back. Replacing the elbow and
shoulder armour with higher performing impact protectors would improve the protection levels of this
garment. There are zipped vents in the chest to allow controlled airflow movement through the garment.
The breathability rating is based on tests of the garment's materials when all vents are closed. The
breathability of this product may be better when the vents can be opened. Breathability was measured
without the removable thermal liner installed.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                   
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Elbow                       
                                                                      Shoulder                    
                                                                      Back                        




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                Brixton Classic
Page 1 of 5                                     Leather Jacket                                  motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      7/10
                                                                     Abrasion score       4.97




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6          3.0 - 5.6   1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5          1.8 - 2.5   0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5           1.0 - 1.5   0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             4.78   3.71     5.27     4.59     5.89     5.61                 4.97   A

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           4.78     3.71     5.27     4.59     5.89     5.61                  4.97   G

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           4.78     3.71     5.27     4.59     5.89     5.61                  4.97   G


Details of materials used in jacket
Material A       Leather shell with mesh inner liner




                                                Brixton Classic
Page 2 of 5                                     Leather Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1140




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1652           1074           705           1159          1285          1006          1147        G
Zones 3 & 4 1288           883            1030          813           1758          910           1114        G




                                                 Brixton Classic
Page 3 of 5                                      Leather Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximium
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    3/10
                                                                               Impact score     18.6




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15          15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            25.3       M                                    30.1         P
Maximum force (kN)                            29.3       M                                    35.2         P
Coverage of Zone 1 area                       85%                                             95%
Coverage of Zone after displacement           80%                                             95%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           24.0             25.1           28.3              23.5              29.4          35.2
Impact Protector 2           22.1             25.9           29.3              25.2              30.6          33.6
Impact Protector 3           23.9             23.3           26.2              28.0              30.1          35.2



                                                     Brixton Classic
Page 4 of 5                                          Leather Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating   ★★★                                 Breathability rating       N/A
Breathability score      0.424                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            34.4            29.7          32.0
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.234           0.219        0.227
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Brixton
    Model                           Classic
    Type                            Jacket - Leather
    Date purchased                  23 November 2020
    Tested by                       AMCAF, Deakin University
    Garment test reference          J19L38
    Rating first published          January 2021
    Rating updated                  5 January 2021


                                                Brixton Classic
Page 5 of 5                                     Leather Jacket                              motocap.com.au
