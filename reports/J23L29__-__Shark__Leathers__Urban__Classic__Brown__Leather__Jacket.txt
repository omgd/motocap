                                                          This MotoCAP safety rating applies to:
                                                          Brand                   Shark Leathers
                                                          Model                   Urban Classic (Brown)
                                                          Type                    Jacket - Leather
                                                          Date purchased          1 June 2023
                                                          Sizes tested            XL and 2XL
                                                          Test garment gender     Male
                                                          Style                   All Purpose
                                                          RRP                     $399.00


                                                          Test Results Summary           Rating        Score
                                                          MotoCAP Protection Rating         ★★          33.8
                                                          Abrasion                        3/10          2.45
                                                          Burst                          10/10         1082
                                                          Impact                          5/10          35.7
                                                          MotoCAP Breathability Rating      ★★         0.328
                                                          Moisture Vapour Resistance         -          45.6
                                                          Thermal Resistance                 -         0.249
                                                          Water resistance                  N/A         N/A
This garment is fitted with impact protectors for the elbows, shoulders and back. Replacing the shoulder
armour with higher performing impact protectors would improve the protection levels of this garment.
There are zipped vents in the chest, arms and back to allow controlled airflow movement through the
garment. The breathability rating is based on tests of the garment's materials when all vents are closed.
The breathability of this product may be better when the vents can be opened. Breathability was
measured without the removable thermal liner installed.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                    Removable liners
                                                                    Thermal liner                  ✓
                                                                    Water-resistant liner

                                                                    Removable impact protection
                                                                                   Pockets Armour
                                                                    Elbow            ✓       ✓
                                                                    Shoulder         ✓       ✓
                                                                    Back             ✓       ✓




          Zone 1                     Zone 2                      Zone 3                      Zone 4

   High risk of abrasion      High risk of abrasion     Medium risk of abrasion      Low risk of abrasion
    High risk of impact
                                     Shark Leathers Urban Classic Brown
Page 1 of 5                                    Leather Jacket                                     motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                   Abrasion Resistance Performance
                                                                   Abrasion rating     3/10
                                                                   Abrasion score      2.45




Determining Criteria   Area            Good        Acceptable    Marginal       Poor
High abrasion risk     Zones 1 & 2     > 5.6       3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk   Zone 3          > 2.5       1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk      Zone 4          >1.5        1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2      Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%             2.23   2.30     2.29     3.52     2.33     2.05                2.45  M

Zone 3          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material A      100%           2.23     2.30     2.29     3.52     2.33     2.05                 2.45  A

Zone 4          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material A      100%           2.23     2.30     2.29     3.52     2.33     2.05                 2.45  G


Details of materials used in jacket
Material A      Leather shell with mesh inner liner




                                     Shark Leathers Urban Classic Brown
Page 2 of 5                                    Leather Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is
a pictorial representation of the data from the table below.




                                                                        Burst Strength Performance
                                                                        Burst rating    10/10
                                                                        Burst score     1082




Determining Criteria       Unit         Good        Acceptable    Marginal       Poor
Burst strength            (kPa)        > 1000       800 - 1000    500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2       Sample 3      Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 1256          1220           1154          1075          1006         1053          1127       G
Zones 3 & 4 646           990            850           703           1249         976           902        A




                                     Shark Leathers Urban Classic Brown
Page 3 of 5                                    Leather Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                             Impact Protection Performance
                                                                             Impact rating   5/10
                                                                             Impact score    35.7




Determining Criteria        Unit           Good        Acceptable      Marginal        Poor*
Impact force                (kN)             < 15       15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the
Zone.
Impact protector type                        Elbow                                        Shoulder
Average force (kN)                            19.9     A                                    20.0          A
Maximum force (kN)                            24.7     A                                    26.0          M
Coverage of Zone 1 area                       95%                                           95%
Coverage of Zone after displacement           90%                                           95%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at
a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                          Shoulder
Strike location             Centre            Mid          Edge             Centre             Mid            Edge
Impact Protector 1           17.2             18.4         21.4              17.3              17.6           19.3
Impact Protector 2           18.4             18.8         21.6              19.0              19.9           19.4
Impact Protector 3           19.3             19.7         24.7              17.5              23.7           26.0




                                       Shark Leathers Urban Classic Brown
Page 4 of 5                                      Leather Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating    ★★                                 Breathability rating      N/A
Breathability score    0.328                               Breathability score       N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)          1               2         Average
Without removable liners                            46.9            44.4         45.6
With water-resistant liner                          N/A             N/A          N/A

Thermal Resistance - Rct (K.m2/W)                    1               2         Average
Without removable liners                           0.263           0.236        0.249
With water-resistant liner                          N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Shark Leathers
   Model                            Urban Classic (Brown)
   Type                             Jacket - Leather
   Date purchased                   1 June 2023
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J23L29
   Rating first published           August 2023
   Rating updated                   30 August 2023


                                      Shark Leathers Urban Classic Brown
Page 5 of 5                                     Leather Jacket                              motocap.com.au
