                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Ducati
                                                           Model                    Corse Pelle C4
                                                           Type                     Jacket - Leather
                                                           Date purchased           20 October 2021
                                                           Sizes tested             56
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $999.00


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating          ★★             39.4
                                                           Abrasion                            5/10          3.88
                                                           Burst                              10/10         1554
                                                           Impact                              2/10          14.9
                                                           MotoCAP Breathability Rating        ⯨            0.118
                                                           Moisture Vapour Resistance          -            106.7
                                                           Thermal Resistance                  -            0.210
                                                           Water resistance                   N/A            N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Replacing the elbow and shoulder armour with higher performing impact
protectors would improve the protection levels of this garment. There are no vents to allow airflow
movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact

                                            Ducati Corse Pelle C4
Page 1 of 5                                    Leather Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating      5/10
                                                                      Abrasion score       3.88




Determining Criteria    Area             Good       Acceptable      Marginal      Poor
High abrasion risk      Zones 1 & 2      > 5.6       3.0 - 5.6      1.3 - 2.9     < 1.3
Medium abrasion risk    Zone 3           > 2.5       1.8 - 2.5      0.8 - 1.7     < 0.8
Low abrasion risk       Zone 4           >1.5        1.0 - 1.5      0.4 - 0.9     < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             3.38   3.99     3.92     3.80     3.94     5.02                 4.01   A

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       85%            3.38     3.99     3.92     3.80     3.94     5.02                  4.01   G
Material B       15%            0.96     1.02     0.93     0.66     0.81     0.00                  0.73   P
Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           3.38     3.99     3.92     3.80     3.94     5.02                  4.01   G


Details of materials used in jacket
Material A       Leather shell with mesh inner liner
Material B       Stretch fabric shell with mesh inner liner




                                            Ducati Corse Pelle C4
Page 2 of 5                                    Leather Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1554




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000     500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4       Sample 5     Sample 6      Average
Zones 1 & 2 1303           1640           986           1943           1623         1945          1574        G
Zones 3 & 4 1573           1526           1943          936            1941         947           1478        G




                                            Ducati Corse Pelle C4
Page 3 of 5                                    Leather Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    2/10
                                                                               Impact score     14.9




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            22.3       A                                    21.3         A
Maximum force (kN)                            44.8       P                                    29.1         M
Coverage of Zone 1 area                      120%                                             60%
Coverage of Zone after displacement          100%                                             60%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           14.1             13.7           20.0              17.1              29.1          17.3
Impact Protector 2           18.6             13.7           18.5              20.8              27.8          23.6
Impact Protector 3           33.4             44.8           24.3              19.5              19.4          17.2




                                                Ducati Corse Pelle C4
Page 4 of 5                                        Leather Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                With water-resistant liner
Breathability rating       ⯨                            Breathability rating       N/A
Breathability score      0.118                          Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1            2         Average
Without removable liners                            111.7        101.8        106.7
With water-resistant liner                           N/A          N/A          N/A

Thermal Resistance - Rct (K.m2/W)                     1            2         Average
Without removable liners                            0.211        0.210        0.210
With water-resistant liner                           N/A          N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Ducati
   Model                            Corse Pelle C4
   Type                             Jacket - Leather
   Date purchased                   20 October 2021
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J20L25
   Rating first published           January 2022
   Rating updated                   28 January 2022



                                              Ducati Corse Pelle C4
Page 5 of 5                                      Leather Jacket                             motocap.com.au
