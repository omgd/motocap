                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Black Pup Moto
                                                           Model                    Tallarook Waterproof ADV
                                                           Type                     Pants - Textile
                                                           Date purchased           2 February 2024
                                                           Sizes tested             M and XL
                                                           Test garment gender      Male
                                                           Style                    Adventure
                                                           RRP                      $399.00


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating        ★★★★             56.3
                                                           Abrasion                            4/10          3.34
                                                           Burst                              10/10         1377
                                                           Impact                             10/10          86.2
                                                           MotoCAP Breathability Rating        ⯨            0.147
                                                           Moisture Vapour Resistance           -           101.0
                                                           Thermal Resistance                   -           0.247
                                                           Water resistance                   9/10            1.9
This garment is fitted with impact protectors for the knees and hips. There are zipped vents in the upper legs
to allow controlled airflow movement through the garment. The breathability rating is based on tests of the
garment's materials when all vents are closed. The breathability of this product may be better when the
vents are open. This garment has a removable water-resistant liner. The breathability rating above was
achieved with the thermal and water-resistant liners removed. When tested with the water-resistant liner
installed, the breathability rating reduced but remained within half a star.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                   
                                                                      Water-resistant liner           

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Knee                          
                                                                      Hip                           




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                 Black Pup Moto Tallarook Waterproof ADV
Page 1 of 5                                   Textile Pants                                     motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                             Abrasion Resistance Performance
                                                                             Abrasion rating      4/10
                                                                             Abrasion score       3.34




Determining Criteria       Area              Good         Acceptable      Marginal          Poor
High abrasion risk         Zones 1 & 2       > 5.6         3.0 - 5.6      1.3 - 2.9        < 1.3
Medium abrasion risk       Zone 3            > 2.5         1.8 - 2.5      0.8 - 1.7        < 0.8
Low abrasion risk          Zone 4            >1.5          1.0 - 1.5      0.4 - 0.9        < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                        Average
Material A        80%             10.00  10.00    10.00    10.00    10.00    10.00                           10.00 G
Material B        20%              0.91   0.62     0.71     0.63     0.63     0.78                            0.71  P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                        Average
Material B        100%             0.91   0.62     0.71     0.63     0.63     0.78                            0.71  P

Zone 4             Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                       Average
Material B         100%           0.91     0.62     0.71     0.63     0.63     0.78                          0.71   M

Details of materials used in pant
Material A         Leather shell, laminated woven fabric layer, foam layer, para-aramid layer with mesh inner liner
Material B         Laminated woven fabric shell with mesh inner liner




                                    Black Pup Moto Tallarook Waterproof ADV
Page 2 of 5                                      Textile Pants                                         motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1377




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)         > 1000      800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1685           1300           1349          1576          1441          1572          1487        G
Zones 3 & 4 923            449            537           1124          1355          1223          935         A




                                   Black Pup Moto Tallarook Waterproof ADV
Page 3 of 5                                     Textile Pants                                  motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating   10/10
                                                                               Impact score     86.2




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                              7.4      G                                        16.3      A
Maximum force (kN)                             14.8      G                                        22.5      A
Coverage of Zone 1 area                       200%                                               140%
Coverage of Zone after displacement           100%                                               100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre             Mid           Edge              Centre            Mid            Edge
Impact Protector 1            2.6              5.9           12.4               14.4             15.6           22.5
Impact Protector 2            3.9              5.8           14.8               10.5             15.3           22.1
Impact Protector 3            3.3              6.5           10.9               10.7             15.4           20.1



                                    Black Pup Moto Tallarook Waterproof ADV
Page 4 of 5                                      Textile Pants                                           motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ⯨                               Breathability rating        ⯨
Breathability score      0.147                             Breathability score       0.133

Moisture Vapour Resistance - Ret (kPa.m2/W)            1              2         Average
Without removable liners                             102.5           99.4         101.0
With water-resistant liner                           198.7          200.7         199.7

Thermal Resistance - Rct (K.m2/W)                      1              2         Average
Without removable liners                             0.253          0.240         0.247
With water-resistant liner                           0.436          0.446         0.441

Water spray and rain resistance
This pants are advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment         Water absorbed by underwear
                Volume (ml)    Percentage (%)     Volume (ml)    Percentage (%)
Pants 1             809             42%                 2             0.8%
Pants 2             745             40%                 7             2.9%
Average             777             41%                 5             1.9%

Location of wetting
Major wetting to the cotton underwear was present at the waistband and crotch for one pair of pants and
no visible wetting on the other pair of pants tested.




    Assessment Details.
    Brand                            Black Pup Moto
    Model                            Tallarook Waterproof ADV
    Type                             Pants - Textile
    Date purchased                   2 February 2024
    Tested by                        AMCAF, Deakin University
    Report approved by               MotoCAP Chief Scientist
    Garment test reference           P24T13
    Rating first published           May 2024
    Rating updated                   9 May 2024

                                    Black Pup Moto Tallarook Waterproof ADV
Page 5 of 5                                      Textile Pants                               motocap.com.au
