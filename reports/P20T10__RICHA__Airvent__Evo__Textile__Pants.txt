                                                           This MotoCAP safety rating applies to:
                                                           Brand                    RICHA
                                                           Model                    Airvent Evo
                                                           Type                     Pants - Textile
                                                           Date purchased           25 May 2021
                                                           Sizes tested             L and XL
                                                           Test garment gender      Male
                                                           Style                    Tourer
                                                           RRP                      $379.00


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating           ⯨             12.3
                                                           Abrasion                           1/10           0.52
                                                           Burst                              9/10           977
                                                           Impact                             1/10            0.0
                                                           MotoCAP Breathability Rating     ★★★★            0.629
                                                           Moisture Vapour Resistance           -            18.8
                                                           Thermal Resistance                   -           0.197
                                                           Water resistance                   7/10            7.0
This garment is fitted with impact protectors for the knees. Pockets are provided at the hips for fitting
aftermarket impact protectors. Adding hip impact protectors would improve the protection levels of this
garment. Mesh panels are located in the front of the upper and lower legs and the backs of the knees to
allow airflow movement through the garment. This garment has a removable water-resistant liner. The
breathability rating above was achieved with the water-resistant liner removed. When tested with the water-
resistant liner installed, the breathability rating reduced to two stars.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner           

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Knee                          
                                                                      Hip              




          Zone 1                      Zone 2                       Zone 3                       Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               RICHA Airvent Evo
Page 1 of 5                                       Textile Pants                                 motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating      1/10
                                                                      Abrasion score       0.52




Determining Criteria    Area            Good          Acceptable    Marginal      Poor
High abrasion risk      Zones 1 & 2     > 5.6         3.0 - 5.6     1.3 - 2.9     < 1.3
Medium abrasion risk    Zone 3          > 2.5         1.8 - 2.5     0.8 - 1.7     < 0.8
Low abrasion risk       Zone 4          >1.5          1.0 - 1.5     0.4 - 0.9     < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        25%              0.52   0.59              0.45                                   0.52     P
Material B        75%              0.76   0.42     0.58     0.52     0.42     0.42                 0.52     P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        50%              0.76   0.42     0.58     0.52     0.42     0.42                 0.52     P
Material C        50%              0.53   0.54     0.33     0.54     0.56     0.36                 0.48     P
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        50%              0.76   0.42     0.58     0.52     0.42     0.42                 0.52     M
Material C        50%              0.53   0.54     0.33     0.54     0.56     0.36                 0.48     M
Details of materials used in jacket
Material A       Heavy woven fabric shell with mesh inner liner
Material B       Mesh fabric shell with mesh inner liner
Material C       Woven fabric shell with mesh inner liner




                                                RICHA Airvent Evo
Page 2 of 5                                        Textile Pants                             motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     9/10
                                                                          Burst score       977




Determining Criteria        Unit         Good         Acceptable    Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000     500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3       Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 1054           995            1077           665           698          1335          971         A
Zones 3 & 4 900            1236           688            1223          833          1132          1002        G




                                                RICHA Airvent Evo
Page 3 of 5                                        Textile Pants                               motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    1/10
                                                                               Impact score      0.0




Determining Criteria         Unit            Good         Acceptable    Marginal         Poor*
Impact force                 (kN)            < 15          15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                               Hip
Average force (kN)                             16.5       A                                               P
Maximum force (kN)                             18.0       A                                               P
Coverage of Zone 1 area                       110%                                               0%
Coverage of Zone after displacement            90%                                               0%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip       No impact protector present
Strike location             Centre            Mid             Edge             Centre          Mid           Edge
Impact Protector 1           16.8             15.1            17.3
Impact Protector 2           17.1             15.0            16.7
Impact Protector 3           18.0             15.9            16.1



                                                    RICHA Airvent Evo
Page 4 of 5                                            Textile Pants                                   motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                    With water-resistant liner
Breathability rating  ★★★★                                  Breathability rating       ★★
Breathability score      0.629                              Breathability score       0.304

Moisture Vapour Resistance - Ret (kPa.m2/W)           1                2         Average
Without removable liners                             18.3            19.3          18.8
With water-resistant liner                           46.9            51.5          49.2
                              2
Thermal Resistance - Rct (K.m /W)                     1                2         Average
Without removable liners                            0.192            0.202        0.197
With water-resistant liner                          0.246            0.252        0.249

Water spray and rain resistance
This pants are advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment         Water absorbed by underwear
                Volume (ml)    Percentage (%)     Volume (ml)    Percentage (%)
Pants 1             269             24%                16              6%
Pants 2             139             10%                21              8%
Average             204             17%                18              7%

Location of wetting:
Minor wetting to the cotton underwear was present at the lower legs for one pair of pants and minor
wetting to the crotch on the other pair of pants tested.




    Assessment Details.
    Brand                           RICHA
    Model                           Airvent Evo
    Type                            Pants - Textile
    Date purchased                  25 May 2021
    Tested by                       AMCAF, Deakin University
    Garment test reference          P20T10
    Rating first published          October 2021
    Rating updated                  11 October 2021


                                                RICHA Airvent Evo
Page 5 of 5                                        Textile Pants                              motocap.com.au
