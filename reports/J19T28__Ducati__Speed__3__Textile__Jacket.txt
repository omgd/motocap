                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Ducati
                                                           Model:                  Speed 3
                                                           Type:                   Jacket - Textile
                                                           Date purchased:         29 November 2019
                                                           Sizes tested:           L and XL
                                                           Gender:                 M
                                                           Style:                  Sports
                                                           Test code:              J19T28
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating                   28.7
                                                           Abrasion                         1/10        1.00
                                                           Burst                           10/10       1261
                                                           Impact                           5/10        37.0
                                                           MotoCAP Comfort Rating                   0.472
                                                           Moisture Vapour Resistance                   29.4
                                                           Thermal Resistance                          0.232
                                                           Water resistance                 N/A         N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Mesh panels are located in the arms, chest and back to allow airflow movement
through the garment.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact

                                                Ducati Speed 3
Page 1 of 5                                     Textile Jacket                                motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Woven fabric shell, fabric layer and mesh inner liner
Material B:      Woven fabric shell with mesh inner liner
Material C:      Mesh fabric shell with mesh inner liner




Zone             Coverage      Abrasion time for each test (seconds)                                    Average
                 (%)                1         2          3          4              5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       100%             1.34      0.88       1.63       2.41            1.10       1.07         1.41      M

Zone 3 area (Medium abrasion risk)
Material B       20%              0.39        0.49         0.42      0.33         0.00       0.00         0.41      P
Material C       80%              0.39        0.26         0.46      0.43         0.33       0.00         0.37      P
Zone 4 area (Low abrasion risk)
Material B       20%              0.39        0.49         0.42      0.33         0.00       0.00         0.41      M
Material C       80%              0.39        0.26         0.46      0.43         0.33       0.00         0.37      P
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                          Good          Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:       > 5.6          3.0 - 5.6    1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:        > 2.5          1.8 - 2.5    0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:        >1.5           1.0 - 1.5    0.4 - 0.9          < 0.4

                                                    Ducati Speed 3
Page 2 of 5                                         Textile Jacket                                   motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2                3               4                 5              Average
Zones 1 & 2          1659           1345             1087            1673              914            1336      G
Zone EZ             1541            1319             1233            1236              1058           1277      G
Zones 3 & 4         893             1286             1057            996               1159           1078      G
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good         Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000       800 - 1000         500 - 799          < 500




                                                    Ducati Speed 3
Page 3 of 5                                         Textile Jacket                                      motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The table below shows the test results for each strike on each impact protector in kilonewtons (kN) and
their area of coverage as a proportion (%) of the Zone.
Impact protector type                        Elbow                                            Shoulder
Average force (kN)                            24.9       A                                      22.0          A
Maximum force (kN)                            27.1       M                                          25.4      M
Coverage of zone 1 area                      120%                                                  100%
Coverage of zone after displacement           90%                                                  100%
Individual test results
Impact force (kN)             Elbow                                           Shoulder
Strike location                 A              B               C                 A                  B              C
Impact Protector 1             23.6           25.6           27.1                   18.2           20.6           22.1
Impact Protector 2             22.5           25.2           26.1                   20.3           22.8           23.8
Impact Protector 3             23.3           25.0           25.7                   21.5           23.0           25.4
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good         Acceptable       Marginal         Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24         25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                                     Ducati Speed 3
Page 4 of 5                                          Textile Jacket                                        motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                         1             2         Average
Moisture Vapour Resistance - Ret        29.8         29.0          29.4
              2
     (kPam /W)
                                         1             2         Average
Thermal Resistance - Rct               0.233         0.231        0.232
          2
      (Km /W)



Water spray and rain resistance
This garment has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                               Ducati Speed 3
Page 5 of 5                                    Textile Jacket                              motocap.com.au
