                                                          This MotoCAP safety rating applies to:
                                                          Brand:                   Rideract
                                                          Model:                   Adventure Riding TRIGEL
                                                          Type:                    Glove - Leather
                                                          Date purchased:          1 June 2023
                                                          Sizes tested:            XL and 2XL
                                                          Test glove gender:       Male
                                                          Style:                   Tourer
                                                          RRP:                     $59.99
                                                          Test Results Summary:
                                                                                         Rating      Score
                                                          MotoCAP Protection Rating       ★★           3.1
                                                          Abrasion                        7/10        3.97
                                                          Seam strength                   1/10         3.2
                                                          Impact                          5/10         9.6
                                                          Water resistance               10/10         0.6
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                        Impact protection
                                                                        Knuckles         ✓
                                                                        Palm             ✓




              Zone 1                    Zone 2                      Zone 3

       High risk of impact       High risk of abrasion     Medium risk of abrasion
      High risk of abrasion


                                          Adventure Riding TRIGEL
Page 1 of 5                                   Leather Glove                                    motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                           Abrasion Resistance Performance
                                                                           Abrasion rating     7/10
                                                                           Abrasion score      3.97




Determining Criteria      Area              Good        Acceptable      Marginal         Poor
High abrasion risk        Zone 1 & 2        > 4.0        2.7 - 4.0      1.2 - 2.6        < 1.2
Medium abrasion risk      Zone 3             2.5         1.8 - 2.5      0.8 - 1.7        < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                      Average
Material A       80%             10.00  10.00    10.00    10.00    10.00    10.00                         10.00        G
Material B       20%              1.74   1.38     1.70     2.43     1.80     3.26                          2.05        M
Zone 2           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                      Average
Material C       30%              2.04   3.29     5.82     2.98     6.88    10.48                          5.25        G
Material B       70%              1.74   1.38     1.70     2.43     1.80     3.26                          2.05        M
Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                      Average
Material C       10%              2.04   3.29     5.82     2.98     6.88    10.48                          5.25        G
Material D       90%              0.38   0.80     0.93     0.66     0.43     0.29                          0.58        P
Details of materials used in glove - derived from manufacturer provided information
Material A        Hard-shell armour, foam/mesh layer, fabric layer, water-resistant layer with foam/mesh inner liner
Material B        Suede leather shell, water-resistant layer with foam and mesh inner liner
Material C        Leather patch over suede leather shell, water-resistant layer with foam and mesh inner liner
Material D        Woven fabric shell, water-resistant layer with foam and mesh inner liner




                                              Adventure Riding TRIGEL
Page 2 of 5                                       Leather Glove                                        motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates
the tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash
and is a pictorial representation of the data from the tables below.




                                                                                   Seam Strength Performance
                                                                                   Seam strength rating 1/10
                                                                                   Seam strength score   3.2




Determining Criteria        Unit             Good        Acceptable          Marginal           Poor
Seam tensile strength       (N/mm)           > 11          9 - 11            6 - 8.9            <6
Glove restraint             (N)              > 200       100 - 200           50 - 99            <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                4                 5              Average
Zones 1 & 2             5.15         2.22            4.75             5.53              4.48           4.43       P
Zone 3                  4.05         13.84           0.61             4.71              2.46           5.13       P
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                4                 5              Average
Wrist restraint         129.8        143.7           126.3            133.3             142.4          135.1      A




                                               Adventure Riding TRIGEL
Page 3 of 5                                        Leather Glove                                              motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered in the impact protection
ratings.




                                                                                  Impact Protection Performance
                                                                                  Impact rating   5/10
                                                                                  Impact score     9.6




Determining Criteria                     Unit           Good         Acceptable          Marginal         Poor
Impact force                             (kN)            <2           2 - 4.9             5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces
are capped at a maximum of 10.0kN.
Impact protector type                       Knuckles                     Palm
Average force (kN)                             1.6       G                4.9        A
Maximum force (kN)                             1.9       G                6.0        M
Coverage of zone 1 area                       95%                        75%

Individual test results: - The table below shows the test results for each strike on each impact protector in
kilonewtons (kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type       Knuckles                                              Palm
Strike number                  1                2              3                   1                2
Impact Protector 1              1.6             1.4            1.4                 6.0              3.9
Impact Protector 2              1.6             1.3            1.9                 5.4              5.5
Impact Protector 3              1.8             1.5            1.7                 4.6              3.7




                                                Adventure Riding TRIGEL
Page 4 of 5                                         Leather Glove                                           motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the
wetting proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove           Water absorbed by cotton glove
                 Volume (ml) Percentage (%)        Volume (ml) Percentage (%)
Pair 1                0.9           0.3%                0.9            4%
Pair 2                1.6           0.6%                1.6            8%
Average               1.2           0.4%                1.2            6%
Location of wetting:
There was no visible wetting to the cotton under-glove in all four of the gloves tested.




   Assessment Details.
   Brand                          Rideract
   Model                          Adventure Riding TRIGEL
   Type                           Glove - Leather
   Date purchased                 1 June 2023
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G23L33
   Rating first published         August 2023
   Rating updated                 22 August 2023


                                           Adventure Riding TRIGEL
Page 5 of 5                                    Leather Glove                                 motocap.com.au
