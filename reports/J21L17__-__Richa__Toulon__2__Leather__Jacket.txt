                                                          This MotoCAP safety rating applies to:
                                                          Brand                   Richa
                                                          Model                   Toulon 2
                                                          Type                    Jacket - Leather
                                                          Date purchased          5 December 2022
                                                          Sizes tested            XL
                                                          Test garment gender     Male
                                                          Style                   All Purpose
                                                          RRP                     $679.00


                                                          Test Results Summary           Rating         Score
                                                          MotoCAP Protection Rating      ★★★             44.3
                                                          Abrasion                          5/10         4.02
                                                          Burst                             9/10         951
                                                          Impact                            7/10         48.8
                                                          MotoCAP Breathability Rating      ★           0.269
                                                          Moisture Vapour Resistance         -           51.2
                                                          Thermal Resistance                 -          0.229
                                                          Water resistance                  N/A          N/A
This garment is fitted with impact protectors for the elbows, shoulders and back. There are no vents to
allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                    Removable liners
                                                                    Thermal liner
                                                                    Water-resistant liner

                                                                    Removable impact protection
                                                                                   Pockets Armour
                                                                    Elbow                   
                                                                    Shoulder                
                                                                    Back                    




         Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion      High risk of abrasion     Medium risk of abrasion      Low risk of abrasion
    High risk of impact
                                                Richa Toulon 2
Page 1 of 5                                     Leather Jacket                                     motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                   Abrasion Resistance Performance
                                                                   Abrasion rating     5/10
                                                                   Abrasion score      4.02




Determining Criteria    Area            Good       Acceptable    Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6       3.0 - 5.6    1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5       1.8 - 2.5    0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5        1.0 - 1.5    0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2      Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%             3.44   4.12     4.70     3.99     3.73     4.14               4.02   A

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%           3.44     4.12     4.70     3.99     3.73     4.14               4.02   G

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%           3.44     4.12     4.70     3.99     3.73     4.14               4.02   G


Details of materials used in jacket
Material A      Leather shell with fabric inner liner




                                                Richa Toulon 2
Page 2 of 5                                     Leather Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is
a pictorial representation of the data from the table below.




                                                                        Burst Strength Performance
                                                                        Burst rating     9/10
                                                                        Burst score      951




Determining Criteria       Unit         Good        Acceptable    Marginal       Poor
Burst strength            (kPa)        > 1000       800 - 1000    500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2       Sample 3      Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 624           813            1145          1125          814          1171          949        A
Zones 3 & 4 802           956            1116          959           974                        961        A




                                                Richa Toulon 2
Page 3 of 5                                     Leather Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                             Impact Protection Performance
                                                                             Impact rating   7/10
                                                                             Impact score    48.8




Determining Criteria        Unit           Good         Acceptable     Marginal        Poor*
Impact force                (kN)             < 15        15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the
Zone.
Impact protector type                        Elbow                                        Shoulder
Average force (kN)                            18.8      A                                   18.1          A
Maximum force (kN)                            20.0      A                                   18.8          A
Coverage of Zone 1 area                      110%                                          105%
Coverage of Zone after displacement           90%                                          100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at
a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                          Shoulder
Strike location             Centre            Mid           Edge            Centre             Mid            Edge
Impact Protector 1           19.3             18.9          20.0             18.6              18.1           18.5
Impact Protector 2           19.6             18.6          17.7             18.0              18.0           18.8
Impact Protector 3           18.3             19.0          17.9             17.2              17.4           18.2




                                                     Richa Toulon 2
Page 4 of 5                                          Leather Jacket                                   motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★                                 Breathability rating      N/A
Breathability score    0.269                               Breathability score       N/A

                                       2
Moisture Vapour Resistance - Ret (kPa.m /W)          1               2         Average
Without removable liners                            51.8            50.6        51.2
With water-resistant liner                          N/A             N/A          N/A

Thermal Resistance - Rct (K.m2/W)                    1               2         Average
Without removable liners                           0.219           0.240        0.229
With water-resistant liner                          N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Richa
   Model                            Toulon 2
   Type                             Jacket - Leather
   Date purchased                   5 December 2022
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J21L17
   Rating first published           February 2023
   Rating updated                   6 February 2023


                                                 Richa Toulon 2
Page 5 of 5                                      Leather Jacket                             motocap.com.au
