                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Ducati
                                                           Model                    Outdoor C3
                                                           Type                     Jacket - Textile
                                                           Date purchased           22 September 2023
                                                           Sizes tested             XL and 2XL
                                                           Test garment gender      Male
                                                           Style                    Tourer
                                                           RRP                      $599.00


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating          ★★             28.9
                                                           Abrasion                           1/10           0.65
                                                           Burst                              7/10           753
                                                           Impact                             8/10           60.5
                                                           MotoCAP Breathability Rating        ★            0.281
                                                           Moisture Vapour Resistance           -            48.8
                                                           Thermal Resistance                   -           0.228
                                                           Water resistance                    N/A           N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are no vents to allow airflow movement through the garment.
Breathability was measured without the removable thermal liner installed. The fit is tight with garments
approximately one size smaller than indicated.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                       Zone 3                       Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               Ducati Outdoor C3
Page 1 of 5                                      Textile Jacket                                  motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.65




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.72   0.58     0.69     0.66     0.63     0.61                 0.65   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.72     0.58     0.69     0.66     0.63     0.61                  0.65   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.72     0.58     0.69     0.66     0.63     0.61                  0.65   M


Details of materials used in jacket
Material A       Woven fabric shell with fleece backing and mesh inner liner




                                             Ducati Outdoor C3
Page 2 of 5                                    Textile Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     7/10
                                                                          Burst score       753




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 735            861            684           537           609           878           717         M
Zones 3 & 4 774            925            720           1113          942           909           897         A




                                             Ducati Outdoor C3
Page 3 of 5                                    Textile Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    8/10
                                                                               Impact score     60.5




Determining Criteria         Unit            Good         Acceptable    Marginal         Poor*
Impact force                 (kN)            < 15          15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            10.1        G                                    9.4        G
Maximum force (kN)                            14.1        G                                   11.1        G
Coverage of Zone 1 area                      130%                                             90%
Coverage of Zone after displacement           80%                                             80%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre             Mid            Edge            Centre             Mid          Edge
Impact Protector 1            8.1              8.2            14.1             8.3               8.2          11.1
Impact Protector 2            8.0              8.8            11.4             9.5               9.1           9.8
Impact Protector 3           10.3              9.7            12.3             9.0               9.3           9.9



                                                    Ducati Outdoor C3
Page 4 of 5                                           Textile Jacket                                   motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ★                               Breathability rating       N/A
Breathability score      0.281                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            49.4            48.2          48.8
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1              2          Average
Without removable liners                            0.230           0.227        0.228
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Ducati
    Model                           Outdoor C3
    Type                            Jacket - Textile
    Date purchased                  22 September 2023
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J24T09
    Rating first published          November 2023
    Rating updated                  23 November 2023



                                              Ducati Outdoor C3
Page 5 of 5                                     Textile Jacket                              motocap.com.au
