                                                            This MotoCAP safety rating applies to:
                                                           Brand:                       Five
                                                           Model:                       WFX Skin EVO GTX
                                                           Type:                        Glove - Leather
                                                           Date purchased:              4 October 2024
                                                           Sizes tested:                L,XL,2XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $180.00
                                                            Test Results Summary:
                                                                                              Rating       Score
                                                           MotoCAP Protection Rating          ★★            3.97
                                                           Abrasion                           10/10         5.75
                                                           Seam strength                       1/10          4.7
                                                           Impact                              5/10          9.0
                                                           Water resistance                   10/10          1.6
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove. A marginal wrist restraint score reduced this
gloves protection rating from three stars.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles         ✓
                                                                             Palm             ✓




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                           Five WFX Skin EVO GTX
Page 1 of 5                                     Leather Glove                                     motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                            Abrasion Resistance Performance
                                                                            Abrasion rating     10/10
                                                                            Abrasion score       5.75




Determining Criteria      Area              Good         Acceptable      Marginal         Poor
High abrasion risk        Zone 1 & 2        > 4.0         2.7 - 4.0      1.2 - 2.6        < 1.2
Medium abrasion risk      Zone 3             2.5          1.8 - 2.5      0.8 - 1.7        < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                       Average
Material A        75%             10.00  10.00    10.00    10.00    10.00    10.00                          10.00     G
Material B        25%              6.12   3.68     4.74     5.09     4.39     3.94                           4.66     G
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                       Average
Material B        90%              6.12   3.68     4.74     5.09     4.39     3.94                           4.66     G
Material C        10%              1.60   2.52     2.12     2.54     1.49     1.32                           1.93     M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                       Average
Material B        60%              6.12   3.68     4.74     5.09     4.39     3.94                           4.66     G
Material C        40%              1.60   2.52     2.12     2.54     1.49     1.32                           1.93     A
Details of materials used in glove - derived from manufacturer provided information
Material A       Leather over hard-shell armour, foam layer, water-resistant layer, fabric cushion and mesh inner liner
Material B       Leather shell, water-resistant layer, fabric cushion and mesh inner liner
Material C       Woven fabric shell, water-resistant layer, fabric cushion and mesh inner liner




                                               Five WFX Skin EVO GTX
Page 2 of 5                                         Leather Glove                                     motocap.com.au
Seam Tensile Strength
The tensile strength of the glove's seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  1/10
                                                                                     Seam strength score    4.7




Determining Criteria        Unit             Good        Acceptable            Marginal          Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9          <6
Glove restraint             (N)              > 200          100 - 200           50 - 99          <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5             Average
Zones 1 & 2             5.59         16.28           13.21              13.51             9.28          11.58     G
Zone 3                  7.44         14.36           10.81              10.49             8.31          10.28     A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5             Average
Wrist restraint         111.7        87.9            99.2               58.6              65.3          84.5      M




                                               Five WFX Skin EVO GTX
Page 3 of 5                                         Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    5/10
                                                                                   Impact score      9.0




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9            5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                              1.5       G                  3.5      A
Maximum force (kN)                              3.4       A                  4.1      A
Coverage of zone 1 area                        90%                          60%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                 2              3                  1                2
Impact Protector 1              1.4              3.4            1.5                 3.2              3.7
Impact Protector 2              1.5              1.0            1.2                 3.1              3.5
Impact Protector 3              1.6              1.3            1.1                 4.1              3.6




                                                 Five WFX Skin EVO GTX
Page 4 of 5                                           Leather Glove                                        motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove            Water absorbed by cotton glove
                 Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)      Water Resistance
Pair 1                0.6             0%                 0.6             3%            Performance
Pair 2                0.1             0%                 0.1             0%            Water rating   10/10
Average               0.4             0%                 0.4             2%            Water score     1.61
Location of wetting:
There was no visible wetting to the cotton under-glove in all four of the gloves tested.




   Assessment Details.
   Brand                           Five
   Model                           WFX Skin EVO GTX
   Type                            Glove - Leather
   Date purchased                  4 October 2024
   Tested by                       AMCAF, Deakin University
   Report approved by              MotoCAP Chief Scientist
   Garment test reference          G25L02
   Rating first published          January 2025
   Rating updated                  13 January 2025

                                           Five WFX Skin EVO GTX
Page 5 of 5                                     Leather Glove                               motocap.com.au
