                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Five
                                                           Model:                       RFX4 Evo
                                                           Type:                        Glove - Leather
                                                           Date purchased:              13 March 2024
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       Sports
                                                           RRP:                         $149.99
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating           ★★          2.2
                                                           Abrasion                            4/10       2.20
                                                           Seam strength                       7/10       10.9
                                                           Impact                              3/10        6.7
                                                           Water resistance                    N/A         N/A
This glove is fitted with impact protectors for the knuckles and palm areas. Perforated leather in the back of
the hand provide continuous airflow within the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm              




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                                Five RFX4 Evo
Page 1 of 5                                     Leather Glove                                      motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      4/10
                                                                     Abrasion score       2.20




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8



Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        40%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        60%              2.21   2.62     0.22     0.86     1.88     1.57                  1.56    M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        20%              3.26   2.97     3.19     4.84     2.98     2.76                  3.33    A
Material B        80%              2.21   2.62     0.22     0.86     1.88     1.57                  1.56    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        41%              3.26   2.97     3.19     4.84     2.98     2.76                  3.33    G
Material D        59%              1.89   0.94     0.61     2.27     0.38     3.82                  1.65    M
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell
Material B       Leather shell with fabric inner liner
Material C       Suede patch over leather shell
Material D       Perforated leather shell with fabric inner liner




                                                Five RFX4 Evo
Page 2 of 5                                     Leather Glove                                motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  7/10
                                                                                     Seam strength score   10.9




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             10.66        9.96            13.13              11.84             13.32          11.78     G
Zone 3                  12.35        8.66            9.45               7.54              9.74           9.55      A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         328.7        278.8           290.9              284.9             363.3          309.3     G




                                                     Five RFX4 Evo
Page 3 of 5                                          Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                    Impact Protection Performance
                                                                                    Impact rating    3/10
                                                                                    Impact score      6.7




Determining Criteria                      Unit           Good          Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9             5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                              2.5        A                 4.8       A
Maximum force (kN)                              3.1        A                 6.5       M
Coverage of zone 1 area                        95%                          25%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                               Palm
Strike number                   1                2              3                    1                2
Impact Protector 1              2.3              2.6            2.1                  6.5              5.8
Impact Protector 2              2.9              2.8            2.2                  2.9              4.1
Impact Protector 3              2.6              2.3            3.1                  0.0              0.0




                                                       Five RFX4 Evo
Page 4 of 5                                            Leather Glove                                        motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Five
   Model                          RFX4 Evo
   Type                           Glove - Leather
   Date purchased                 13 March 2024
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G24L30
   Rating first published         April 2024
   Rating updated                 30 April 2024


                                               Five RFX4 Evo
Page 5 of 5                                    Leather Glove                                motocap.com.au
