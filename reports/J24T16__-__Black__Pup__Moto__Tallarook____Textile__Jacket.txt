                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Black Pup Moto
                                                           Model                    Tallarook
                                                           Type                     Textile Jacket
                                                           Date purchased           11 January 2024
                                                           Sizes tested             M and XL
                                                           Test garment gender      Male
                                                           Style                    Adventure
                                                           RRP                      $450.00


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating        ★★★★             56.9
                                                           Abrasion                            6/10          4.37
                                                           Burst                              10/10         1176
                                                           Impact                             10/10          77.6
                                                           MotoCAP Breathability Rating        ⯨            0.041
                                                           Moisture Vapour Resistance           -           480.8
                                                           Thermal Resistance                   -           0.328
                                                           Water resistance                   1/10           50.8
This garment is fitted with impact protectors for the elbows, shoulders and back. There are zipped vents in
the upper arms and sides of back to allow controlled airflow movement through the garment. The
breathability rating is based on tests of the garment's materials when all vents are closed. The breathability
of this product may be better when the vents can be opened. Breathability was measured without the
removable thermal liner installed.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back                          




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                          Black Pup Moto Tallarook
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      6/10
                                                                     Abrasion score       4.37




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             8.17   6.76     7.02     5.96     4.46     7.72                 6.68   G

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.91     0.67     1.08     1.05     0.75     0.93                  0.90   M

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.91     0.67     1.08     1.05     0.75     0.93                  0.90   M


Details of materials used in jacket
Material A       Leather shell, water-resistant layer and mesh inner liner
Material B       Fabric shell, water-resistant layer and mesh inner liner




                                         Black Pup Moto Tallarook
Page 2 of 5                                    Textile Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1176




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1526           1143           1301          1152          1283          756           1193        G
Zones 3 & 4 1519           465            1609          489           1081          1468          1105        G




                                          Black Pup Moto Tallarook
Page 3 of 5                                     Textile Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating   10/10
                                                                               Impact score     77.6




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                             6.4       G                                    7.0         G
Maximum force (kN)                             7.9       G                                    9.1         G
Coverage of Zone 1 area                      120%                                            110%
Coverage of Zone after displacement           90%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre             Mid           Edge             Centre             Mid          Edge
Impact Protector 1            5.0              6.1            7.2              5.2               6.0           8.0
Impact Protector 2            5.1              5.8            7.1              6.0               6.5           8.3
Impact Protector 3            6.7              6.5            7.9              6.2               7.9           9.1



                                             Black Pup Moto Tallarook
Page 4 of 5                                        Textile Jacket                                      motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                  With water-resistant liner
Breathability rating       ⯨                              Breathability rating       N/A
Breathability score      0.041                            Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1             2          Average
Without removable liners                            538.6          423.0         480.8
With water-resistant liner                           N/A            N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1             2          Average
Without removable liners                            0.336          0.319         0.328
With water-resistant liner                           N/A            N/A           N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment        Water absorbed by underwear
                Volume (ml)    Percentage (%)    Volume (ml)    Percentage (%)
Jacket 1            871             42%              157             58%
Jacket 2            766             35%              116             43%
Average             818             39%              136             51%

Location of wetting
There was major wetting to the cotton underwear present at the sleeves and tummy of both jackets and at
the neck of one without liner. When the liner was added, wetting at the sleeve and tummy became minor
and no wetting visible at the neck.




    Assessment Details.
    Brand                           Black Pup Moto
    Model                           Tallarook
    Type                            Textile Jacket
    Date purchased                  11 January 2024
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J24T16
    Rating first published          March 2024
    Rating updated                  25 March 2024

                                           Black Pup Moto Tallarook
Page 5 of 5                                      Textile Jacket                            motocap.com.au
