                                                           This MotoCAP safety rating applies to:
                                                           Brand                   Alpinestars
                                                           Model                   GP plus R V4 Rideknit
                                                           Type                    Leather Jacket
                                                           Date purchased          8 May 2024
                                                           Sizes tested            54 and 58
                                                           Test garment gender     Male
                                                           Style                   Sports
                                                           RRP                     $899.99


                                                           Test Results Summary          Rating         Score
                                                           MotoCAP Protection Rating    ★★★★★            64.4
                                                           Abrasion                       8/10           5.77
                                                           Burst                         10/10          1521
                                                           Impact                         9/10           67.9
                                                           MotoCAP Breathability Rating    ⯨            0.153
                                                           Moisture Vapour Resistance       -           100.1
                                                           Thermal Resistance               -           0.255
                                                           Water resistance               N/A            N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Mesh panels are located in the arms and back to allow airflow movement
through the garment. Perforated leather is located in the chest to allow further airflow movement through
the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                    Pockets    Armour
                                                                      Elbow                     
                                                                      Shoulder                  
                                                                      Back            




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion       Low risk of abrasion
    High risk of impact
                                      Alpinestars GP plus R V4 Rideknit
Page 1 of 5                                    Leather Jacket                                 motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      8/10
                                                                     Abrasion score       5.77




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        35%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material B        65%              5.35   5.41     4.31     6.28     5.43     3.90                  5.11    A
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        70%              5.35   5.41     4.31     6.28     5.43     3.90                  5.11    G
Material C        30%              1.50   1.30     1.44     1.23     1.23     1.47                  1.36    M
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material D        90%              4.39   4.62     4.20     4.54     4.67     6.61                  4.84    G
Material E        10%              1.26   1.06     0.80     1.58     0.99     2.72                  1.40    A
Details of materials used in jacket
Material A       Hard-shell armour over leather shell
Material B       Leather shell with mesh inner liner
Material C       Coated woven fabric shell with mesh inner liner
Material D       Perforated leather shell with mesh inner liner
Material E       Thick mesh fabric shell with mesh inner liner



                                      Alpinestars GP plus R V4 Rideknit
Page 2 of 5                                    Leather Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1521




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1805           1895           845           1922          1877          1791          1689        G
Zones 3 & 4 1119           744            437           599           1361          836           849         A




                                      Alpinestars GP plus R V4 Rideknit
Page 3 of 5                                    Leather Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    9/10
                                                                               Impact score     67.9




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            4.7        G                                    16.4         A
Maximum force (kN)                            7.8        G                                    20.1         A
Coverage of Zone 1 area                      150%                                            100%
Coverage of Zone after displacement          100%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre             Mid           Edge             Centre             Mid           Edge
Impact Protector 1            3.6              3.7            3.3              14.1              14.7          15.0
Impact Protector 2            3.8              6.6            4.1              16.0              15.9          16.9
Impact Protector 3            4.7              7.8            4.9              16.5              20.1          18.6



                                         Alpinestars GP plus R V4 Rideknit
Page 4 of 5                                       Leather Jacket                                        motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                  With water-resistant liner
Breathability rating       ⯨                              Breathability rating       N/A
Breathability score      0.153                            Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1             2          Average
Without removable liners                            103.6          96.6         100.1
With water-resistant liner                           N/A           N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1             2          Average
Without removable liners                            0.247          0.263        0.255
With water-resistant liner                           N/A            N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Alpinestars
    Model                           GP plus R V4 Rideknit
    Type                            Leather Jacket
    Date purchased                  8 May 2024
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J24L19
    Rating first published          June 2024
    Rating updated                  27 June 2024


                                       Alpinestars GP plus R V4 Rideknit
Page 5 of 5                                     Leather Jacket                              motocap.com.au
