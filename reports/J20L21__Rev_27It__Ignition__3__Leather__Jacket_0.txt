                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Rev'It
                                                           Model                    Ignition 3
                                                           Type                     Jacket - Leather
                                                           Date purchased           17 May 2021
                                                           Sizes tested             54 and 56
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $769.00


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating        ★★★★           60.4
                                                           Abrasion                            8/10        5.83
                                                           Burst                              10/10       1556
                                                           Impact                              7/10        52.1
                                                           MotoCAP Breathability Rating       ★★          0.336
                                                           Moisture Vapour Resistance           -          44.9
                                                           Thermal Resistance                   -         0.251
                                                           Water resistance                   1/10         46.5
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Mesh panels are located in the arms, chest and back to allow airflow movement
through the garment. This garment has a removable water-resistant liner. The breathability rating above
was achieved with the thermal and water-resistant liners removed. When tested with the water resistant-
liner installed, the breathability rating reduced to 1 star



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                   
                                                                      Water-resistant liner           

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Elbow                       
                                                                      Shoulder                    
                                                                      Back             




          Zone 1                      Zone 2                       Zone 3                       Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact

                                               Rev'It Ignition 3
Page 1 of 5                                     Leather Jacket                                  motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                       Abrasion Resistance Performance
                                                                       Abrasion rating      8/10
                                                                       Abrasion score       5.83




Determining Criteria    Area             Good         Acceptable     Marginal      Poor
High abrasion risk      Zones 1 & 2      > 5.6         3.0 - 5.6     1.3 - 2.9     < 1.3
Medium abrasion risk    Zone 3           > 2.5         1.8 - 2.5     0.8 - 1.7     < 0.8
Low abrasion risk       Zone 4           >1.5          1.0 - 1.5     0.4 - 0.9     < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             8.32   6.63     7.32    11.39     5.49     9.34                 8.08   G

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       30%           10.00    10.00    10.00    10.00                                    10.00 G
Material C       70%            0.70     1.14     0.45     0.67     0.70     0.69                   0.72  P
Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material C       100%           0.70     1.14     0.45     0.67     0.70     0.69                   0.72  M


Details of materials used in jacket
Material A       Leather shell with mesh inner liner
Material B       Leather shell, foam layer, fabric layer and mesh inner liner
Material C       Mesh fabric shell with mesh inner liner




                                                 Rev'It Ignition 3
Page 2 of 5                                       Leather Jacket                             motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                           Burst Strength Performance
                                                                           Burst rating    10/10
                                                                           Burst score      1556




Determining Criteria        Unit         Good         Acceptable     Marginal      Poor
Burst strength             (kPa)        > 1000        800 - 1000     500 - 799     < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3       Sample 4       Sample 5    Sample 6      Average
Zones 1 & 2 1627           1948           1472           1953           1642        1366          1668        G
Zones 3 & 4 1193           967            1287           1254           755         1195          1109        G




                                                 Rev'It Ignition 3
Page 3 of 5                                       Leather Jacket                               motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     52.1




Determining Criteria         Unit            Good         Acceptable     Marginal        Poor*
Impact force                 (kN)            < 15           15 - 24      25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            18.0        A                                   16.2         A
Maximum force (kN)                            27.8        M                                   21.0         A
Coverage of Zone 1 area                      130%                                            130%
Coverage of Zone after displacement          100%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid             Edge            Centre             Mid           Edge
Impact Protector 1           14.5             16.0            27.8             13.9              16.2          21.0
Impact Protector 2           14.3             17.1            20.0             13.5              15.6          17.4
Impact Protector 3           15.7             18.0            18.3             15.5              16.5          16.7




                                                     Rev'It Ignition 3
Page 4 of 5                                           Leather Jacket                                    motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating      ★★                               Breathability rating        ★
Breathability score      0.336                             Breathability score       0.216

Moisture Vapour Resistance - Ret (kPa.m2/W)          1               2          Average
Without removable liners                            48.7            41.0         44.9
With water-resistant liner                          82.5            86.3         84.4

Thermal Resistance - Rct (K.m2/W)                     1               2         Average
Without removable liners                            0.253           0.249        0.251
With water-resistant liner                          0.294           0.315        0.304

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
              Water absorbed by garment          Water absorbed by underwear
              Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Jacket 1           521            17%                169             58%
Jacket 2          2014            67%                 99             35%
Average           1268            42%                134             46%

Location of wetting:
There was major wetting to the cotton underwear present at the neck and chest for one jacket and at the
cuffs of the sleeves of the other jacket tested.




   Assessment Details.
   Brand                            Rev'It
   Model                            Ignition 3
   Type                             Jacket - Leather
   Date purchased                   17 May 2021
   Tested by                        AMCAF, Deakin University
   Garment test reference           J20L21
   Rating first published           October 2021
   Rating updated                   11 October 2021



                                                Rev'It Ignition 3
Page 5 of 5                                      Leather Jacket                              motocap.com.au
