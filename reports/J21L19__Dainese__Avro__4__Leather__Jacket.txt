                                                          This MotoCAP safety rating applies to:
                                                          Brand                   Dainese
                                                          Model                   Avro 4
                                                          Type                    Jacket - Leather
                                                          Date purchased          4 December 2022
                                                          Sizes tested            56 and 60
                                                          Test garment gender     Male
                                                          Style                   Sports
                                                          RRP                     $1,099.00


                                                          Test Results Summary          Rating       Score
                                                          MotoCAP Protection Rating    ★★★★           54.4
                                                          Abrasion                       8/10         5.87
                                                          Burst                         10/10        1370
                                                          Impact                         5/10         37.9
                                                          MotoCAP Breathability Rating    ⯨          0.136
                                                          Moisture Vapour Resistance       -         127.3
                                                          Thermal Resistance               -         0.288
                                                          Water resistance               N/A          N/A
This garment is fitted with impact protectors for the elbows and shoulders. Shoulder impact protectors
are held in place with velcro fastening. A pocket is provided for an aftermarket back protector. Replacing
the elbow and shoulder armour with higher performing impact protectors would improve the protection
levels of this garment. There are zipped vents in the sides to allow controlled airflow movement through
the garment. The breathability rating is based on tests of the garment's materials when all vents are
closed. The breathability of this product may be better when the vents can be opened. Breathability was
measured without the removable thermal liner installed.
Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                    Removable liners
                                                                    Thermal liner              
                                                                    Water-resistant liner

                                                                    Removable impact protection
                                                                                  Pockets Armour
                                                                    Elbow                  
                                                                    Shoulder                
                                                                    Back            




         Zone 1                      Zone 2                      Zone 3                     Zone 4

   High risk of abrasion      High risk of abrasion     Medium risk of abrasion     Low risk of abrasion
    High risk of impact
                                                Dainese Avro 4
Page 1 of 5                                     Leather Jacket                                motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                   Abrasion Resistance Performance
                                                                   Abrasion rating     8/10
                                                                   Abrasion score      5.87




Determining Criteria   Area            Good        Acceptable    Marginal       Poor
High abrasion risk     Zones 1 & 2     > 5.6       3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk   Zone 3          > 2.5       1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk      Zone 4          >1.5        1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2      Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A       100%             5.81   6.25     7.74     5.37     5.07     5.40               5.94   G

Zone 3          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material A      100%           5.81     6.25     7.74     5.37     5.07     5.40                5.94   G

Zone 4          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6             Average
Material A      90%            5.81     6.25     7.74     5.37     5.07     5.40                5.94   G
Material B      10%            1.20     1.59     1.45     1.30                                  1.38   A
Details of materials used in jacket
Material A      Leather shell with mesh inner liner
Material B      Stretch knitted fabric with mesh inner liner




                                               Dainese Avro 4
Page 2 of 5                                    Leather Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is
a pictorial representation of the data from the table below.




                                                                        Burst Strength Performance
                                                                        Burst rating    10/10
                                                                        Burst score     1370




Determining Criteria       Unit         Good        Acceptable    Marginal       Poor
Burst strength            (kPa)        > 1000       800 - 1000    500 - 799      < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2       Sample 3      Sample 4      Sample 5     Sample 6      Average
Zones 1 & 2 1364          1422           1440          1332          1404         1413          1396      G
Zones 3 & 4 1437          1554           1504          1234          1392         469           1265      G




                                                Dainese Avro 4
Page 3 of 5                                     Leather Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                             Impact Protection Performance
                                                                             Impact rating   5/10
                                                                             Impact score    37.9




Determining Criteria        Unit           Good         Acceptable     Marginal        Poor*
Impact force                (kN)             < 15        15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the
Zone.
Impact protector type                        Elbow                                        Shoulder
Average force (kN)                            20.4      A                                   23.4          A
Maximum force (kN)                            26.5      M                                   31.5          P
Coverage of Zone 1 area                      140%                                          100%
Coverage of Zone after displacement          100%                                          100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at
a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                          Shoulder
Strike location             Centre            Mid           Edge            Centre             Mid            Edge
Impact Protector 1           20.3             24.4          26.5             15.9              25.5           31.5
Impact Protector 2           20.4             25.3          12.5             20.0              19.0           25.2
Impact Protector 3           15.4             20.4          18.4             20.1              25.3           28.0




                                                     Dainese Avro 4
Page 4 of 5                                          Leather Jacket                                   motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                With water-resistant liner
Breathability rating     ⯨                              Breathability rating      N/A
Breathability score    0.136                            Breathability score       N/A

                                       2
Moisture Vapour Resistance - Ret (kPa.m /W)          1              2       Average
Without removable liners                           131.4          123.1      127.3
With water-resistant liner                          N/A            N/A        N/A

Thermal Resistance - Rct (K.m2/W)                    1              2       Average
Without removable liners                           0.285          0.291      0.288
With water-resistant liner                          N/A            N/A        N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Dainese
   Model                            Avro 4
   Type                             Jacket - Leather
   Date purchased                   4 December 2022
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J21L19
   Rating first published           February 2023
   Rating updated                   28 February 2023


                                                 Dainese Avro 4
Page 5 of 5                                      Leather Jacket                             motocap.com.au
