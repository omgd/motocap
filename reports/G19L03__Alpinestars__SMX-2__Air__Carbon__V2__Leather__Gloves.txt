                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Alpinestars
                                                           Model:                  SMX-2 Air Carbon V2
                                                           Type:                   Gloves - Leather
                                                           Date purchased:         4 February 2019
                                                           Sizes tested:           2XL and XL
                                                           Gender:                 M&F
                                                           Style:                  All Purpose
                                                           Test code:              G19L03
                                                           Test Results Summary:
                                                                                          Rating      Result
                                                           MotoCAP Protection Rating                  2.0
                                                           Abrasion                        5/10        2.61
                                                           Seam strength                   3/10         7.6
                                                           Impact                          1/10         4.0
                                                           Water resistance                N/A         N/A
This glove is fitted with impact protectors for the knuckles and palm areas but there is no impact protection
in the wrist area. There is mesh fabric on the tops of the fingers and thumb, mesh on top of the wrist and
vented rubber inserts in the fingers provided in the glove to aid cooling in hot weather.



Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                     Zone 4

    High risk of impact        High risk of abrasion     Medium risk of abrasion       Low risk of abrasion




                                      Alpinestars SMX-2 Air Carbon V2
Page 1 of 5                                    Leather Glove                                 motocap.com.au
Abrasion Resistance
The glove was tested for abrasion resistance in accordance with MotoCAP test protocols. The table below
shows the test results for time to abrade to material failure for each sample by Zone, type and area
coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Single layer of suede leather outer
Material B:      Single layer of leather outer
Material C:      Polyester mesh fabric outer




Zone             Coverage        Abrasion time for each test (s)                                       Average
                 (%)                 1          2          3         4            5             6         (s)
Zone 2 area (High abrasion risk)
Material A       100%              0.44       2.28       0.61       0.94         1.65       9.09         2.50    M

Zone 3 area (Medium abrasion risk)
Material B       80%              0.79         0.84       3.56      2.07         1.81       1.46         1.75    M
Material A       20%              0.44         2.28       0.61      0.94         1.65       9.09         2.50    A
Zone 4 area (Low abrasion risk)
Material C       90%              7.34         2.52       4.47      2.69         6.54       3.54         4.51    G
Material B       10%              0.79         0.84       3.56      2.07         1.81       1.46         1.75    M
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each Zone
calculated from the data in the table above.




                                           Good        Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk          Zone 2:        > 4.0        2.7 - 4.0    1.2 - 2.6          < 1.2
Medium abrasion risk        Zone 3:         3.5         2.5 - 3.5    1.0 - 2.4          < 1.0
Low abrasion risk           Zone 4:        >2.5         1.8 - 2.5    0.8 - 1.7          < 0.8


                                         Alpinestars SMX-2 Air Carbon V2
Page 2 of 5                                       Leather Glove                                     motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly worn
glove) were tested in accordance with MotoCAP test protocols. The table below shows the seam tensile
strength in newtons per millimeter (N/mm) for each seam tested by Zone and the average result for each
Zone.
Seam tensile strength (N/mm)
Area                  1                 2               3                  4                5               Average
Zones 2 & 3           8.76              8.93            7.32               10.29            12.89           9.64      M
Zone 4                10.21             13.22           11.09              10.14            11.42           11.22     A
The table below shows the force required to remove the restrained glove in newtons (N) for each of the five
gloves tested and the average result.
Glove restraint (N)
Glove                 1                 2               3                  4                5               Average
Wrist restraint       317.4             328.8           260.4              278.1            296.5           296.2     M
The diagram below illustrates the tensile strength and wrist restraint results in terms of the likely
performance of the glove in a crash and is a pictorial representation of the data from the tables above.




                                                Good        Acceptable         Marginal             Poor
Determining Criteria
Seam tensile strength         (N/mm)            > 15            10 - 15         6.5 - 9.9           < 6.5
Glove restraint                   (N)           > 400          300 - 400       200 - 299            <200




                                            Alpinestars SMX-2 Air Carbon V2
Page 3 of 5                                          Leather Glove                                             motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
table below shows the test results for each strike on each impact protector in kilonewton (kN) and their area
of coverage in percentage (%) within the Zone.
Impact protector type                       Knuckles                        Palm                        Wrist
Average force             (kN)                1.03        G                  7.2       M                         P
Maximum force             (kN)                 1.20      G                  12.10      P                         P
Coverage of zone 1 area                        95%                          40%                          0%
Individual test results
Impact force (kN)            Knuckles                                               Palm
Strike location                 A               B              C                     A              B
Impact Protector 1               1.00          1.10           1.00                  5.60           12.10
Impact Protector 2               1.00          1.00           1.00                  7.30            5.70
Impact Protector 3               1.10          0.90           1.20                  7.60            4.90
Impact force (kN)                Wrist    No impact protector present
Strike location                   A             B
Impact Protector 1
Impact Protector 2
Impact Protector 3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above.




                                            Good         Acceptable         Marginal       Poor*
Determining Criteria
Knuckle and wrist Impact force (kN)          <2            2 - 4.9            5-8          >8
Palm impact force              (kN)          <4            4 - 5.9            6-8          >8
* Poor may also indicate that no impact protector is present in the glove
Areas shaded black are not considered in the impact protection ratings.



                                          Alpinestars SMX-2 Air Carbon V2
Page 4 of 5                                        Leather Glove                                           motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




                                     Alpinestars SMX-2 Air Carbon V2
Page 5 of 5                                   Leather Glove                                 motocap.com.au
