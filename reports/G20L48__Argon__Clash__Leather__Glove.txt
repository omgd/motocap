                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Argon
                                                           Model:                       Clash
                                                           Type:                        Glove - Leather
                                                           Date purchased:              25 November 2021
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $69.95
                                                           Test Results Summary:
                                                                                             Rating        Score
                                                           MotoCAP Protection Rating           ★            1.3
                                                           Abrasion                           3/10         1.98
                                                           Seam strength                      4/10          7.5
                                                           Impact                             1/10          0.0
                                                           Water resistance                   N/A           N/A
These gloves are not fitted with impact protection. Perforated leather on the back of the hand provides
continuous airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles
                                                                             Palm




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                                 Argon Clash
Page 1 of 5                                     Leather Glove                                    motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      3/10
                                                                     Abrasion score       1.98




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8



Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             1.34   1.79     2.14     1.56     1.47     1.42                 1.62   M

Zone 2           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           1.34     1.79     2.14     1.56     1.47     1.42                  1.62   M

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       30%            4.83     4.01     5.80     7.53     7.09     8.41                  6.28   G
Material C       70%            2.46     2.96     2.46     2.00     1.76     1.37                  2.17   A
Details of materials used in glove - derived from manufacturer provided information
Material A       Leather shell with fabric inner liner
Material B       Leather patch over leather shell with fabric inner liner
Material C       Perforated leather shell, foam layer with fabric inner liner




                                                 Argon Clash
Page 2 of 5                                     Leather Glove                                motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                   Seam Strength Performance
                                                                                   Seam strength rating  4/10
                                                                                   Seam strength score    7.5




Determining Criteria        Unit             Good        Acceptable          Marginal           Poor
Seam tensile strength       (N/mm)           > 11          9 - 11             6 - 8.9           <6
Glove restraint             (N)              > 200       100 - 200            50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                4                 5              Average
Zones 1 & 2             9.08         13.40           14.59            10.45             8.38           11.18     G
Zone 3                  8.04         8.20            12.76            8.24              13.48          10.14     A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                4                 5              Average
Wrist restraint         147.0        121.1           107.3            207.2             115.9          139.7     A




                                                      Argon Clash
Page 3 of 5                                          Leather Glove                                        motocap.com.au
Impact Protection
These gloves were not tested for impact protection as impact protection was not fitted to the gloves. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    1/10
                                                                                   Impact score      0.0




Determining Criteria                      Unit           Good         Acceptable       Marginal           Poor
Impact force                              (kN)            <2            2 - 4.9           5-8              >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                                         p                          P
Maximum force (kN)                                         p                          P
Coverage of zone 1 area                          0%                         0%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles     No impact protector present              Palm         No impact protector present
Strike number                   1               2               3                   1                 2
Impact Protector 1
Impact Protector 2
Impact Protector 3




                                                       Argon Clash
Page 4 of 5                                           Leather Glove                                       motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Argon
   Model                          Clash
   Type                           Glove - Leather
   Date purchased                 25 November 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L48
   Rating first published         April 2022
   Rating updated                 29 April 2022


                                                Argon Clash
Page 5 of 5                                    Leather Glove                                motocap.com.au
