                                                            This MotoCAP safety rating applies to:
                                                           Brand                    Dainese
                                                           Model                    Ladakh 3L
                                                           Type                     Jacket - Textile
                                                           Date purchased           19 June 2024
                                                           Sizes tested             56 and 58
                                                           Test garment gender      Male
                                                           Style                    Adventure
                                                           RRP                      $699.00


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating         ★★★             46.8
                                                           Abrasion                           5/10           4.07
                                                           Burst                              8/10           833
                                                           Impact                             8/10           60.5
                                                           MotoCAP Breathability Rating        ⯨            0.107
                                                           Moisture Vapour Resistance           -           158.9
                                                           Thermal Resistance                   -           0.284
                                                           Water resistance                   8/10            3.6
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back and chest protector. There are zipped vents in the upper arms and sides of back as well as
velcro to the upper chest to allow controlled airflow movement through the garment. The breathability
rating is based on tests of the garment's materials when all vents are closed. The breathability of this
product may be better when the vents are opened. The breathability rating above was achieved with the
thermal and water-resistant liners removed. When tested with the water-resistant liner installed, the
breathability rating remained at half a star.
Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    ✓
                                                                      Water resistant liner            ✓

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow            ✓             ✓
                                                                      Shoulder         ✓             ✓
                                                                      Back             ✓
                                                                      Chest            ✓




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact

                                                   Dainese
Page 1 of 5                                       Ladakh 3L                                      motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      5/10
                                                                     Abrasion score       4.07




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             5.56   4.43     4.39     5.90    10.00     7.03                  6.22  G

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           1.00     0.72     0.76     0.90     0.78     0.95                   0.85  M

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           1.00     0.72     0.76     0.90     0.78     0.95                   0.85  M


Details of materials used in jacket
Material A       Woven fabric shell, foam layer and mesh inner liner
Material B       Woven fabric shell and mesh inner liner




                                                   Dainese
Page 2 of 5                                       Ladakh 3L                                  motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     8/10
                                                                          Burst score       833




Determining Criteria        Unit         Good        Acceptable     Marginal        Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1013           541            559           523           1654          947           873         A
Zones 3 & 4 346            623            599           1036          947           491           674         M




                                                   Dainese
Page 3 of 5                                       Ladakh 3L                                    motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    8/10
                                                                               Impact score     60.5




Determining Criteria         Unit            Good       Acceptable       Marginal        Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            12.6       G                                    13.1         G
Maximum force (kN)                            14.6       G                                    15.7         A
Coverage of Zone 1 area                      120%                                            100%
Coverage of Zone after displacement           90%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           12.1             12.3           14.6              12.0              11.3          15.7
Impact Protector 2           12.9             12.7           13.8              13.6              14.0          11.1
Impact Protector 3           11.7             12.4           10.9              12.2              15.1          13.4




                                                       Dainese
Page 4 of 5                                           Ladakh 3L                                         motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                With water-resistant liner
Breathability rating       ⯨                            Breathability rating        ⯨
Breathability score      0.107                          Breathability score       0.110

Moisture Vapour Resistance - Ret (kPa.m2/W)           1            2         Average
Without removable liners                            156.1        161.6        158.9
With water-resistant liner                          189.2        194.4        191.8

Thermal Resistance - Rct (K.m2/W)                     1            2         Average
Without removable liners                            0.283        0.286           0.284
With water-resistant liner                          0.347        0.359           0.353

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
              Water absorbed by garment          Water absorbed by underwear
              Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)       Water Resistance
Jacket 1          528             27%                 11              4%             Performance
Jacket 2          546             28%                  9              3%             Water rating   8/10
Average           537             28%                 10              4%             Water Score      3.64

Location of wetting
There was minor wetting to the cotton underwear present at the neck for both jackets tested.




   Assessment Details.
   Brand                            Dainese
   Model                            Ladakh 3L
   Type                             Jacket - Textile
   Date purchased                   19 June 2024
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J24T40
   Rating first published           September 2024
   Rating updated                   5 September 2024


                                                    Dainese
Page 5 of 5                                        Ladakh 3L                                  motocap.com.au
