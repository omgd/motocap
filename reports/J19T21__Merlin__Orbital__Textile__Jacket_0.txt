                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Merlin
                                                           Model:                  Orbital
                                                           Type:                   Jacket - Textile
                                                           Date purchased:         9 August 2019
                                                           Sizes tested:           XL and 2XL
                                                           Gender:                 M
                                                           Style:                  Streetwear
                                                           Test code:              J19T21
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating                   29.1
                                                           Abrasion                         1/10        0.23
                                                           Burst                            8/10        860
                                                           Impact                           8/10        64.5
                                                           MotoCAP Comfort Rating            ⯨         0.132
                                                           Moisture Vapour Resistance                  128.4
                                                           Thermal Resistance                          0.282
                                                           Water resistance                 1/10         54
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided at the back
for an aftermarket impact protector. There are no vents to allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact

                                                Merlin Orbital
Page 1 of 5                                     Textile Jacket                                motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Woven fabric shell, water resistant layer, foam layer and mesh inner liner
Material B:      Woven fabric shell, water resistant layer and mesh inner liner




Zone             Coverage      Abrasion time for each test (seconds)                                     Average
                 (%)                1         2          3          4               5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       50%              1.03      1.01       1.36       0.97             0.00       0.00         1.09      P
Material B       50%              0.48      0.38       0.36       0.49             0.35       0.32         0.40      P
Zone 3 area (Medium abrasion risk)
Material B       100%             0.48      0.38       0.36       0.49             0.35       0.32         0.40      P

Zone 4 area (Low abrasion risk)
Material B       100%                 0.48       0.38       0.36      0.49         0.35       0.32         0.40      M


Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                             Good        Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:          > 5.6        3.0 - 5.6    1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:           > 2.5        1.8 - 2.5    0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:           >1.5         1.0 - 1.5    0.4 - 0.9          < 0.4

                                                     Merlin Orbital
Page 2 of 5                                          Textile Jacket                                   motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2              3                  4                 5              Average
Zones 1 & 2          793            934            1100               1173              883            977       A
Zone EZ             704             570            728                1162              1007           834       A
Zones 3 & 4         573             758            647                705               697            676       M
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                          Good           Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)         > 1000         800 - 1000         500 - 799          < 500




                                                   Merlin Orbital
Page 3 of 5                                        Textile Jacket                                        motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The table below shows the test results for each strike on each impact protector in kilonewtons (kN) and
their area of coverage as a proportion (%) of the Zone.
Impact protector type                        Elbow                                            Shoulder
Average force (kN)                            14.1       G                                      15.2          A
Maximum force (kN)                            20.6       A                                          20.7      A
Coverage of zone 1 area                      140%                                                  120%
Coverage of zone after displacement          100%                                                  100%
Individual test results
Impact force (kN)             Elbow                                           Shoulder
Strike location                 A              B               C                 A                  B              C
Impact Protector 1              7.7           14.4           20.6                   11.4           16.1           20.7
Impact Protector 2             10.8           11.0           20.1                   13.2           14.1           16.8
Impact Protector 3              9.4           13.4           19.4                   12.7           14.0           17.9
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good         Acceptable       Marginal         Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24         25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                                     Merlin Orbital
Page 4 of 5                                          Textile Jacket                                        motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                                1             2        Average
Moisture Vapour Resistance - Ret              133.5         123.2        128.4
                2
     (kPam /W)
                                                1             2        Average
Thermal Resistance - Rct                      0.279         0.285        0.282
            2
      (Km /W)



Water spray and rain resistance
This garment is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                    Water absorbed by garment         Water absorbed by underwear
                    Volume (ml)    Percentage (%)     Volume (ml)    Percentage (%)
Garment 1               368             39%               98.3            36%
Garment 2               307            33%                 201           72%
Average                 337            36%                 149           54%

Location of wetting:
Visible wetting to the cotton underwear worn under the motorcycle water resistant garment was present
over the neck and chest of both garments measured and on the back of one of the garments measured.




                                                      Merlin Orbital
Page 5 of 5                                           Textile Jacket                       motocap.com.au
