                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  BMW
                                                           Model:                  Rider
                                                           Type:                   Pants - Textile
                                                           Date purchased:         23 October 2018
                                                           Sizes tested:           56
                                                           Gender:                 M&F
                                                           Style:                  All Purpose
                                                           Test code:              P18T01
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating                  30.6
                                                           Abrasion                         1/10        0.62
                                                           Burst                           10/10       1041
                                                           Impact                           8/10        56.9
                                                           MotoCAP Comfort Rating                   0.421
                                                           Moisture Vapour Resistance                   24.5
                                                           Thermal Resistance                          0.215
                                                           Water resistance                 4/10        13.1
This garment is fitted with impact protectors in the knee and hip areas. This garment does not provide vents
to aid cooling in hot weather. Comfort measurements were conducted with the removable waterproof
membrane installed. The thermal comfort of this product would be better in dry conditions without the
waterproof liner installed.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact
Abrasion Resistance
                                                  BMW Rider
Page 1 of 5                                      Textile Pants                                motocap.com.au
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table below
shows the test results for time to abrade through all layers of the materials. Calculated for each sample by
Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Nylon/cotton woven fabric shell with mesh inner liner




Zone             Coverage      Abrasion time for each test (seconds)                                   Average
                 (%)                1         2          3          4             5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       100%             0.50      0.39       0.45       1.22           0.56       0.59         0.62      P

Zone 3 area (Medium abrasion risk)
Material A       100%             0.50          0.39      0.45      1.22         0.56       0.59         0.62      P

Zone 4 area (Low abrasion risk)
Material A       100%                0.50       0.39      0.45      1.22         0.56       0.59         0.62      M


The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                            Good       Acceptable    Marginal           Poor
Determining Criteria
High abrasion risk       Zone 1/2:          > 5.6       3.0 - 5.6    1.3 - 2.9          < 1.3
Medium abrasion risk      Zone 3:           > 2.5       1.8 - 2.5    0.8 - 1.7          < 0.8
Low abrasion risk         Zone 4:           >1.5        1.0 - 1.5    0.4 - 0.9          < 0.4



Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
                                                     BMW Rider
Page 2 of 5                                         Textile Pants                                   motocap.com.au
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2               3                  4                 5              Average
Zones 1 & 2          938            1239            876                1019              761            967       A
Zone EZ             975             1087            1007               694               1172           987       A
Zones 3 & 4         1690            1132            1053               1472              1149           1299      G
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good           Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000         800 - 1000         500 - 799          < 500




Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.

                                                     BMW Rider
Page 3 of 5                                         Textile Pants                                         motocap.com.au
The table below shows the test results for each strike on each impact protector in kilonewton (kN) and their
area of coverage as a proportion (%) of the Zone.
Impact protector type                        Knee                                                 Hip
Average force (kN)                           16.5        A                                        17.3       A
Maximum force (kN)                            25.3       M                                         24.0      A
Coverage of zone 1 area                      130%                                                 150%
Coverage of zone after displacement           50%                                                 130%
Individual test results
Impact force (kN)             Knee                                                 Hip
Strike location                A               B              C                     A              B              C
Impact Protector 1            14.6            13.2           14.2                  11.8           14.0           14.5
Impact Protector 2            17.1            12.2           13.3                  15.2           13.9           17.5
Impact Protector 3            18.1            25.3           20.2                  24.0           23.1           21.9
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good        Acceptable      Marginal          Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24        25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment




Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                                      BMW Rider
Page 4 of 5                                          Textile Pants                                        motocap.com.au
                                          1             2         Average
Moisture Vapour Resistance - Ret         25.9          23.1        24.5
     (kPam2/W)
                                           1            2         Average
Thermal Resistance - Rct                 0.213        0.217        0.215
      (Km2/W)


Water spray and rain resistance
This garment is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the increased weight (g) and proportion
(%) of the garment and undergarments due to water absorption.
              Water absorbed by garment          Water absorbed by underwear
              Mass (g)       Percentage (%)      Mass (g)       Percentage (%)
Pants 1          237.7            18%                14.3             1%
Pants 2           209.2            16%               404.0          30%
Average           248.1            19%               173.9          13%

Location of wetting:
Visible wetting to the cotton undergarment worn under the motorcycle water resistant pants was present at
the waistband, the crotch, the upper legs and lower legs




                                                  BMW Rider
Page 5 of 5                                      Textile Pants                             motocap.com.au
