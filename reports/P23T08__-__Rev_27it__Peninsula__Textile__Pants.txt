                                                          This MotoCAP safety rating applies to:
                                                          Brand                   Rev'it
                                                          Model                   Peninsula
                                                          Type                    Pants - Textile
                                                          Date purchased          22 February 2023
                                                          Sizes tested            XL
                                                          Test garment gender     Male
                                                          Style                   All Purpose
                                                          RRP                     $399.00


                                                          Test Results Summary          Rating        Score
                                                          MotoCAP Protection Rating       ★            22.5
                                                          Abrasion                       1/10          0.56
                                                          Burst                          9/10          979
                                                          Impact                         5/10          32.8
                                                          MotoCAP Breathability Rating ★★★★★          0.743
                                                          Moisture Vapour Resistance       -           21.0
                                                          Thermal Resistance               -          0.260
                                                          Water resistance               N/A           N/A
This garment is fitted with impact protectors for the knees and hips. Replacing the hip armour with higher
performing impact protectors would improve the protection levels of this garment. There are zipped
vents and perforated textile in the upper legs to allow controlled airflow movement through the
garment. The breathability rating is based on tests of the garment's materials when all vents are closed.
The breathability of this product may be better when the vents can be opened.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                     Removable liners
                                                                     Thermal liner
                                                                     Water-resistant liner

                                                                     Removable impact protection
                                                                                   Pockets Armour
                                                                     Knee            ✓       ✓
                                                                     Hip             ✓       ✓




          Zone 1                     Zone 2                       Zone 3                     Zone 4

   High risk of abrasion      High risk of abrasion     Medium risk of abrasion      Low risk of abrasion
    High risk of impact
                                               Rev'it Peninsula
Page 1 of 5                                     Textile Pants                                  motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated
from the data in the table below. The colour coding is based on the worst performing material in each
zone.




                                                                    Abrasion Resistance Performance
                                                                    Abrasion rating     1/10
                                                                    Abrasion score      0.56




Determining Criteria   Area            Good        Acceptable     Marginal      Poor
High abrasion risk     Zones 1 & 2     > 5.6       3.0 - 5.6      1.3 - 2.9    < 1.3
Medium abrasion risk   Zone 3          > 2.5       1.8 - 2.5      0.8 - 1.7    < 0.8
Low abrasion risk      Zone 4          >1.5        1.0 - 1.5      0.4 - 0.9    < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade
through all layers of the materials. Calculated for each sample by Zone, type and area coverage of each
material as a proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2      Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6           Average
Material A       100%             0.58   0.50     0.68     0.48     0.51     0.79               0.59  P

Zone 3          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A      90%            0.58     0.50     0.68     0.48     0.51     0.79                0.59      P
Material B      10%            0.13     0.29     0.32     0.35                                  0.27      P
Zone 4          Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6            Average
Material A      10%            0.58     0.50     0.68     0.48     0.51     0.79                0.59      M
Material C      90%            0.49     0.47     0.43     0.36     0.38     0.55                0.45      M
Details of materials used in pant
Material A      Woven fabric shell with mesh inner liner
Material B      Perforated woven fabric shell with mesh inner liner
Material C      Knitted fabric shell with mesh inner liner




                                               Rev'it Peninsula
Page 2 of 5                                     Textile Pants                               motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram
below illustrates the burst strength results in terms of the likely performance of the garment in an impact
and is a pictorial representation of the data from the table below.




                                                                        Burst Strength Performance
                                                                        Burst rating     9/10
                                                                        Burst score      979




Determining Criteria       Unit        Good        Acceptable     Marginal      Poor
Burst strength            (kPa)       > 1000       800 - 1000     500 - 799     < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1     Sample 2      Sample 3      Sample 4       Sample 5    Sample 6     Average
Zones 1 & 2 578           1077          628           1479           1552                     1063       G
Zones 3 & 4 892           455           825           530            943         234          646        M




                                               Rev'it Peninsula
Page 3 of 5                                     Textile Pants                                motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated
from the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection
ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating   5/10
                                                                               Impact score    32.8




Determining Criteria        Unit           Good           Acceptable     Marginal       Poor*
Impact force                (kN)             < 15          15 - 24       25 - 30        > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the
Zone.
Impact protector type                         Knee                                               Hip
Average force (kN)                             20.6       A                                      27.1     M
Maximum force (kN)                             22.3       A                                      29.8     M
Coverage of Zone 1 area                       105%                                              110%
Coverage of Zone after displacement            90%                                              100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at
a maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Knee                                                Hip
Strike location             Centre            Mid             Edge             Centre           Mid           Edge
Impact Protector 1           20.5             20.7            22.3              24.2            27.8          29.8
Impact Protector 2           19.9             21.0            19.2              25.3            27.4          27.0
Impact Protector 3           20.5             20.2            21.0              26.0            27.8          28.8




                                                      Rev'it Peninsula
Page 4 of 5                                            Textile Pants                                    motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating ★★★★★                                 Breathability rating      N/A
Breathability score    0.743                               Breathability score       N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)          1               2         Average
Without removable liners                            21.0            21.0         21.0
With water-resistant liner                          N/A             N/A          N/A

Thermal Resistance - Rct (K.m2/W)                    1               2         Average
Without removable liners                           0.259           0.261        0.260
With water-resistant liner                          N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            Rev'it
   Model                            Peninsula
   Type                             Pants - Textile
   Date purchased                   22 February 2023
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           P23T08
   Rating first published           June 2023
   Rating updated                   19 June 2023


                                                Rev'it Peninsula
Page 5 of 5                                      Textile Pants                             motocap.com.au
