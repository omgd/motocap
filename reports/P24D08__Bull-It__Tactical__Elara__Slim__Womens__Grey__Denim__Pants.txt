                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Bull-It
                                                           Model                    Tactical Elara Slim women's
                                                           Type                     Pants-Denim
                                                           Date purchased           22 November 2023
                                                           Sizes tested             12 and 14
                                                           Test garment gender      Female
                                                           Style                    All Purpose
                                                           RRP                      $199.95


                                                           Test Results Summary              Rating       Score
                                                           MotoCAP Protection Rating          ★★           31.3
                                                           Abrasion                           1/10         1.12
                                                           Burst                              8/10         899
                                                           Impact                             8/10         55.8
                                                           MotoCAP Breathability Rating     ★★★★          0.531
                                                           Moisture Vapour Resistance          -           20.5
                                                           Thermal Resistance                  -          0.181
                                                           Water resistance                   N/A          N/A
This garment is fitted with impact protectors for the knees and hips. There are no vents to allow airflow
movement through the garment. There is the potential for burns from heat transferred through the fly
button and pocket studs of the pants during a slide.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets     Armour
                                                                      Knee                        
                                                                      Hip                         




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                   Bull-It Tactical Elara Slim Womens Grey
Page 1 of 5                                       Denim Pants                                   motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       1.12




Determining Criteria    Area             Good       Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2      > 5.6       3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3           > 2.5       1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4           >1.5        1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        40%              2.55   3.19     2.07     3.05     1.76     2.15                 2.46     M
Material C        60%              2.11   1.43     0.97     1.53     1.30     0.94                 1.38     M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        30%              1.44   1.13     2.06     0.95     1.06     1.18                 1.30     M
Material C        70%              2.11   1.43     0.97     1.53     1.30     0.94                 1.38     M
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        30%              1.44   1.13     2.06     0.95     1.06     1.18                 1.30     A
Material C        70%              2.11   1.43     0.97     1.53     1.30     0.94                 1.38     A
Details of materials used in pant
Material A       Denim fabric shell, knitted fabric layer and mesh inner liner
Material B       Denim fabric shell with mesh inner liner
Material C       Denim fabric shell




                                   Bull-It Tactical Elara Slim Womens Grey
Page 2 of 5                                       Denim Pants                                motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     8/10
                                                                          Burst score       899




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 639            956            945           985           912           898           889         A
Zones 3 & 4 917            946            922           1018          933           888           937         A




                                   Bull-It Tactical Elara Slim Womens Grey
Page 3 of 5                                       Denim Pants                                  motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    8/10
                                                                               Impact score     55.8




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                             6.6       G                                        14.2      G
Maximum force (kN)                             9.4       G                                        19.5      A
Coverage of Zone 1 area                       95%                                                130%
Coverage of Zone after displacement           60%                                                100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre             Mid           Edge              Centre            Mid            Edge
Impact Protector 1            5.2              5.7            8.7               11.3             12.1           19.5
Impact Protector 2            5.1              5.9            9.4               11.8             12.4           18.3
Impact Protector 3            5.3              7.1            7.2               12.1             13.4           17.0



                                      Bull-It Tactical Elara Slim Womens Grey
Page 4 of 5                                          Denim Pants                                         motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                    With water-resistant liner
Breathability rating  ★★★★                                  Breathability rating       N/A
Breathability score      0.531                              Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)            1              2          Average
Without removable liners                             20.5            20.5          20.5
With water-resistant liner                           N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                      1              2          Average
Without removable liners                             0.182           0.181        0.181
With water-resistant liner                            N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Bull-It
    Model                           Tactical Elara Slim women's
    Type                            Pants-Denim
    Date purchased                  22 November 2023
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          P24D08
    Rating first published          February 2024
    Rating updated                  12 February 2024


                                    Bull-It Tactical Elara Slim Womens Grey
Page 5 of 5                                        Denim Pants                               motocap.com.au
