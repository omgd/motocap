                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Alpinestars
                                                           Model:                       SP-365 Drystar
                                                           Type:                        Glove - Leather
                                                           Date purchased:              19 October 2021
                                                           Sizes tested:                XL ans 2XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $179.99
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating        ★★★★           4.7
                                                           Abrasion                           10/10       6.34
                                                           Seam strength                      10/10       14.8
                                                           Impact                              5/10        9.7
                                                           Water resistance                   10/10        0.3
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles         
                                                                             Palm             




              Zone 1                     Zone 2                        Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                          Alpinestars SP-365 Drystar
Page 1 of 5                                     Leather Glove                                     motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                              Abrasion Resistance Performance
                                                                              Abrasion rating     10/10
                                                                              Abrasion score       6.34




Determining Criteria       Area               Good         Acceptable       Marginal          Poor
High abrasion risk         Zone 1 & 2         > 4.0         2.7 - 4.0       1.2 - 2.6        < 1.2
Medium abrasion risk       Zone 3              2.5          1.8 - 2.5       0.8 - 1.7        < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                           Average
Material A        90%             10.00  10.00    10.00    10.00    10.00    10.00                              10.00    G
Material B        10%              1.08   1.37     2.01     4.40     2.03     5.19                               2.68    M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                           Average
Material C        30%             10.00   4.08     7.77    10.00     7.18     5.41                               7.41    G
Material B        70%              1.08   1.37     2.01     4.40     2.03     5.19                               2.68    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                           Average
Material C        40%             10.00   4.08     7.77    10.00     7.18     5.41                               7.41    G
Material D        60%              1.54   1.12     1.25     0.95     0.94     1.39                               1.20    M
Details of materials used in glove - derived from manufacturer provided information
Material A         Hard-shell armour over leather shell, foam layer, water-resistant layer and fabric inner liner
Material B         Leather shell, water-resistant layer and fabric inner liner
Material C         Leather patch over leather shell, water-resistant layer and fabric inner liner
Material D         Woven fabric shell, foam layer, water-resistant layer and fabric inner liner




                                               Alpinestars SP-365 Drystar
Page 2 of 5                                          Leather Glove                                        motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating 10/10
                                                                                     Seam strength score   14.8




Determining Criteria        Unit             Good        Acceptable         Marginal              Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             12.75        23.03           8.19               12.06             19.83          15.17     G
Zone 3                  21.77        8.02            22.04              10.65             18.21          16.14     G
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         188.8        187.7           159.7              164.8             248.1          189.8     A




                                             Alpinestars SP-365 Drystar
Page 3 of 5                                        Leather Glove                                            motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    5/10
                                                                                   Impact score      9.7




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9            5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             1.2        G                  7.3      M
Maximum force (kN)                             1.5        G                  8.9      P
Coverage of zone 1 area                       110%                          150%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                2              3                   1                2
Impact Protector 1              1.1              1.2            1.3                 7.0              7.3
Impact Protector 2              1.3              1.2            1.1                 6.3              7.3
Impact Protector 3              1.4              1.5            1.2                 7.4              8.9




                                              Alpinestars SP-365 Drystar
Page 4 of 5                                         Leather Glove                                          motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove           Water absorbed by cotton glove
                 Volume (ml)    Percentage (%)     Volume (ml)    Percentage (%)
Pair 1                75.2           32%                0.5             2%
Pair 2                88.9           38%                1.2             6%
Average               77.1           35%                0.7             4%
Location of wetting:
Minor visible wetting to the cotton under-glove was present over the wrist in three of the four gloves
tested.




   Assessment Details.
   Brand                          Alpinestars
   Model                          SP-365 Drystar
   Type                           Glove - Leather
   Date purchased                 19 October 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L39
   Rating first published         February 2022
   Rating updated                 18 February 2022



                                        Alpinestars SP-365 Drystar
Page 5 of 5                                   Leather Glove                                 motocap.com.au
