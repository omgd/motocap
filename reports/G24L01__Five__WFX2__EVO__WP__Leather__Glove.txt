                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Five
                                                           Model:                       WFX2 EVO WP
                                                           Type:                        Glove - Leather
                                                           Date purchased:              29 June 2023
                                                           Sizes tested:                XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $134.95
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating          ★★★          3.2
                                                           Abrasion                             7/10      3.94
                                                           Seam strength                        7/10      10.6
                                                           Impact                               4/10       7.6
                                                           Water resistance                    10/10       0.0
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm              




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                             Five WFX2 EVO WP
Page 1 of 5                                     Leather Glove                                      motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                            Abrasion Resistance Performance
                                                                            Abrasion rating      7/10
                                                                            Abrasion score       3.94




Determining Criteria      Area              Good         Acceptable       Marginal         Poor
High abrasion risk        Zone 1 & 2        > 4.0         2.7 - 4.0      1.2 - 2.6        < 1.2
Medium abrasion risk      Zone 3             2.5          1.8 - 2.5      0.8 - 1.7        < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                        Average
Material A        70%             10.00  10.00    10.00    10.00    10.00    10.00                           10.00    G
Material B        30%              1.27   4.02     1.80     3.34                                              2.61    M
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                        Average
Material C        90%              1.72   2.12     2.27     1.90     1.59     2.37                            2.00    M
Material D        10%              0.90   0.96     2.02     0.40     0.99     0.46                            0.95    P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                        Average
Material E        30%              0.05   0.07     3.20     0.11     1.46     1.24                            1.02    M
Material D        70%              0.90   0.96     2.02     0.40     0.99     0.46                            0.95    M
Details of materials used in glove - derived from manufacturer provided information
Material A         Woven fabric over hard-shell armour, foam layer, water-resistant layer and woven fabric shell
Material B         Leather shell, water-resistant layer, fabric cushion and fabric inner liner
Material C         Suede leather shell with fabric inner liner
Material D         Woven fabric shell with fabric inner liner
Material E         Suede leather patch over suede leather shell




                                                 Five WFX2 EVO WP
Page 2 of 5                                         Leather Glove                                      motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  7/10
                                                                                     Seam strength score   10.6




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             12.75        10.51           9.02               14.24             14.10          12.12     G
Zone 3                  7.30         15.26           6.86               5.87              6.44           8.35      M
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         400.0        400.0           400.0              400.0             400.0          400.0     G




                                                 Five WFX2 EVO WP
Page 3 of 5                                         Leather Glove                                           motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    4/10
                                                                                   Impact score      7.6




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9            5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                              1.3       G                  3.8      A
Maximum force (kN)                              1.5       G                  4.3      A
Coverage of zone 1 area                        90%                          15%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                2              3                   1                2
Impact Protector 1              1.3              1.0            1.1                 3.6              3.6
Impact Protector 2              1.4              1.4            1.3                 4.0              4.3
Impact Protector 3              1.5              1.2            1.3                 3.4              4.2




                                                  Five WFX2 EVO WP
Page 4 of 5                                          Leather Glove                                         motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove            Water absorbed by cotton glove
                 Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Pair 1                0.1             0%                 0.1             0%
Pair 2                0.4             0%                 0.4             2%
Average               0.3             0%                 0.3             1%
Location of wetting:
There was no visible wetting to the cotton under-glove over the entire hand in all four of the gloves tested.




   Assessment Details.
   Brand                          Five
   Model                          WFX2 EVO WP
   Type                           Glove - Leather
   Date purchased                 29 June 2023
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G24L01
   Rating first published         November 2023
   Rating updated                 29 November 2023

                                             Five WFX2 EVO WP
Page 5 of 5                                     Leather Glove                                motocap.com.au
