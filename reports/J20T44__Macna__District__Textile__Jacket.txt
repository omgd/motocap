                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Macna
                                                           Model                    District
                                                           Type                     Jacket - Textile
                                                           Date purchased           22 February 2022
                                                           Sizes tested             M and XL
                                                           Test garment gender      Male
                                                           Style                    Streetwear
                                                           RRP                      $249.95


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating           ★             18.1
                                                           Abrasion                           1/10           0.86
                                                           Burst                              8/10           822
                                                           Impact                             3/10           18.6
                                                           MotoCAP Breathability Rating      ★★★            0.427
                                                           Moisture Vapour Resistance           -            26.3
                                                           Thermal Resistance                   -           0.187
                                                           Water resistance                    N/A           N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Replacing the elbow and shoulder armour with higher performing impact
protectors would improve the protection levels of this garment. There are no vents to allow airflow
movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                Macna District
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.86




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        70%              1.96   1.91     0.00     1.85     2.02     1.93                 1.61     M
Material B        30%              0.04   0.66     1.01     0.73     0.82     0.66                 0.65     P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        40%              1.96   1.91     0.00     1.85     2.02     1.93                 1.61     M
Material B        60%              0.04   0.66     1.01     0.73     0.82     0.66                 0.65     P
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             0.04   0.66     1.01     0.73     0.82     0.66                 0.65     M


Details of materials used in jacket
Material A       Woven fabric shell, para-aramid fabric layer and mesh inner liner
Material B       Woven fabric shell with mesh inner liner




                                                Macna District
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     8/10
                                                                          Burst score       822




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 955            990            700           1121          512           845           854         A
Zones 3 & 4 648            526            741           613           521           1131          697         M




                                                 Macna District
Page 3 of 5                                      Textile Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    3/10
                                                                               Impact score     18.6




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            24.8       A                                    27.3         M
Maximum force (kN)                            28.1       M                                    29.7         M
Coverage of Zone 1 area                       70%                                            100%
Coverage of Zone after displacement           50%                                             50%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           23.6             24.2           27.1              26.1              27.2          28.7
Impact Protector 2           23.2             23.3           28.1              25.0              26.5          28.0
Impact Protector 3           23.4             24.8           25.5              25.8              29.2          29.7



                                                     Macna District
Page 4 of 5                                          Textile Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating   ★★★                                 Breathability rating       N/A
Breathability score      0.427                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            26.4            26.1          26.3
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.183           0.191        0.187
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Macna
    Model                           District
    Type                            Jacket - Textile
    Date purchased                  22 February 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J20T44
    Rating first published          May 2022
    Rating updated                  13 May 2022


                                                Macna District
Page 5 of 5                                     Textile Jacket                              motocap.com.au
