                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Ricondi
                                                           Model:                       Urban
                                                           Type:                        Glove - Leather
                                                           Date purchased:              22 September 2023
                                                           Sizes tested:                L, 2XL and 3XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $99.00
                                                           Test Results Summary:
                                                                                              Rating     Score
                                                           MotoCAP Protection Rating         ★★★          3.9
                                                           Abrasion                           9/10       4.86
                                                           Seam strength                      9/10       12.1
                                                           Impact                             5/10        9.4
                                                           Water resistance                   N/A         N/A
This garment is fitted with impact protectors to the knuckles and palm. There are no vents to allow airflow
movement through the garment.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles         
                                                                             Palm             




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                                Ricondi Urban
Page 1 of 5                                     Leather Glove                                     motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      9/10
                                                                     Abrasion score       4.86




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 4.0        2.7 - 4.0     1.2 - 2.6      < 1.2
Medium abrasion risk    Zone 3           2.5         1.8 - 2.5     0.8 - 1.7      < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        70%             10.00  10.00    10.00    10.00    10.00    10.00                 10.00    G
Material C        30%              4.23   4.38     4.78     4.68     7.00     5.49                  5.09    G
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        20%              4.23   4.38     4.78     4.68     7.00     5.49                  5.09    G
Material B        80%              0.17   0.31     1.27     2.31     0.96     2.08                  1.18    P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material C        60%              4.23   4.38     4.78     4.68     7.00     5.49                  5.09    G
Material B        40%              0.17   0.31     1.27     2.31     0.96     2.08                  1.18    M
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell
Material B       Leather shell with fabric inner liner
Material C       Leather patch over leather shell with fabric inner liner




                                                Ricondi Urban
Page 2 of 5                                     Leather Glove                                motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                     Seam Strength Performance
                                                                                     Seam strength rating  9/10
                                                                                     Seam strength score   12.1




Determining Criteria        Unit             Good        Acceptable            Marginal           Poor
Seam tensile strength       (N/mm)           > 11             9 - 11            6 - 8.9           <6
Glove restraint             (N)              > 200          100 - 200           50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                  4                 5              Average
Zones 1 & 2             11.04        15.76           9.50               12.10             17.54          13.19     G
Zone 3                  6.06         12.44           10.62              8.38              15.34          10.57     A
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                  4                 5              Average
Wrist restraint         261.3        224.3           179.0              253.6             209.3          225.5     G




                                                     Ricondi Urban
Page 3 of 5                                          Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                    Impact Protection Performance
                                                                                    Impact rating    5/10
                                                                                    Impact score      9.4




Determining Criteria                      Unit           Good          Acceptable          Marginal         Poor
Impact force                              (kN)            <2            2 - 4.9             5-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             1.2         G                 6.7       M
Maximum force (kN)                             1.4         G                 7.6       M
Coverage of zone 1 area                       110%                          50%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                               Palm
Strike number                   1                2              3                    1                2
Impact Protector 1              1.4              0.9            1.1                  5.7              7.6
Impact Protector 2              1.1              1.0            1.3                  6.4              6.0
Impact Protector 3              1.3              1.1            1.2                  7.6              6.8




                                                       Ricondi Urban
Page 4 of 5                                            Leather Glove                                        motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          Ricondi
   Model                          Urban
   Type                           Glove - Leather
   Date purchased                 22 September 2023
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G24L08
   Rating first published         November 2023
   Rating updated                 27 November 2023

                                               Ricondi Urban
Page 5 of 5                                    Leather Glove                                motocap.com.au
