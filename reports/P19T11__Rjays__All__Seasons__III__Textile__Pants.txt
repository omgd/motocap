                                                             This MotoCAP safety rating applies to:
                                                             Brand                     Rjays
                                                             Model                     All Seasons III
                                                             Type                      Pants - Textile
                                                             Date purchased            16 November 2019
                                                             Sizes tested              XL and 2XL
                                                             Test garment gender       Male
                                                             Style                     All Purpose
                                                             RRP                       $189.95


                                                             Test Results Summary              Rating          Score
                                                             MotoCAP Protection Rating            ⯨             11.1
                                                             Abrasion                             1/10          0.36
                                                             Burst                                9/10          935
                                                             Impact                               1/10           0.0
                                                             MotoCAP Breathability Rating         ★★           0.382
                                                             Moisture Vapour Resistance             -           29.5
                                                             Thermal Resistance                     -          0.188
                                                             Water resistance                     1/10          69.7
This garment is fitted with impact protectors for the knees. There are no pockets provided at the hips for
fitting aftermarket impact protectors. Adding hip impact protectors would improve the protection levels of
this garment. Mesh panels are located in the front of the upper and lower legs and the backs of the knees to
allow airflow movement through the garment. This garment has a removable water-resistant liner. The
breathability rating above was achieved with the water-resistant liner removed. When tested with the water
resistant-liner installed, the breathability rating reduced to half a star.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                          Removable liners
                                                                          Thermal liner
                                                                          Water-resistant liner          

                                                                          Removable impact protection
                                                                                        Pockets       Armour
                                                                          Knee                         
                                                                          Hip




          Zone 1                      Zone 2                           Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion       Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               Rjays All Seasons III
Page 1 of 5                                       Textile Pants                                     motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                       Abrasion Resistance Performance
                                                                       Abrasion rating      1/10
                                                                       Abrasion score       0.36




Determining Criteria    Area            Good        Acceptable       Marginal      Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6       1.3 - 2.9     < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5       0.8 - 1.7     < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5       0.4 - 0.9     < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        70%              0.39   0.36     0.34     0.31     0.46     0.43                 0.38     P
Material B        30%              0.39   0.37     0.31     0.34     0.39     0.28                 0.35     P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        50%              0.39   0.36     0.34     0.31     0.46     0.43                 0.38     P
Material B        50%              0.39   0.37     0.31     0.34     0.39     0.28                 0.35     P
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        70%              0.39   0.36     0.34     0.31     0.46     0.43                 0.38     P
Material B        30%              0.39   0.37     0.31     0.34     0.39     0.28                 0.35     P
Details of materials used in jacket
Material A       Woven fabric shell with mesh inner liner
Material B       Mesh fabric shell with mesh inner liner




                                             Rjays All Seasons III
Page 2 of 5                                     Textile Pants                                motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                           Burst Strength Performance
                                                                           Burst rating     9/10
                                                                           Burst score       935




Determining Criteria        Unit         Good        Acceptable      Marginal      Poor
Burst strength             (kPa)        > 1000       800 - 1000      500 - 799     < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4        Sample 5    Sample 6      Average
Zones 1 & 2 892            808            910           912             802         1148          912         A
Zones 3 & 4 1039           1027           1010          965             1047        1068          1026        G




                                             Rjays All Seasons III
Page 3 of 5                                     Textile Pants                                  motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximium force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    1/10
                                                                               Impact score      0.0




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                               Hip
Average force (kN)                            23.1       A                                                P
Maximum force (kN)                            29.5       M                                                P
Coverage of Zone 1 area                       95%                                                0%
Coverage of Zone after displacement           70%                                                0%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip       No impact protector present
Strike location             Centre            Mid            Edge              Centre          Mid           Edge
Impact Protector 1           19.6             23.8           24.9
Impact Protector 2           18.7             20.6           26.4
Impact Protector 3           20.4             24.0           29.5



                                                Rjays All Seasons III
Page 4 of 5                                        Textile Pants                                       motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                    With water-resistant liner
Breathability rating     ★★                                 Breathability rating        ⯨
Breathability score      0.382                              Breathability score       0.044

Moisture Vapour Resistance - Ret (kPa.m2/W)             1               2        Average
Without removable liners                               33.2           25.8          29.5
With water-resistant liner                            299.6          355.1         327.3
                              2
Thermal Resistance - Rct (K.m /W)                       1               2        Average
Without removable liners                              0.194          0.182         0.188
With water-resistant liner                            0.213          0.262         0.237

Water spray and rain resistance
This pants are advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment          Water absorbed by underwear
                Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Pants 1             467             35%                259             105%
Pants 2             458             34%                 85              34%
Average             462             35%                172              70%

Location of wetting
Visible wetting to the cotton underwear was present at the crotch and lower legs of one pair of pants and
the upper and lower legs of the other pair of pants tested.




    Assessment Details.
    Brand                           Rjays
    Model                           All Seasons III
    Type                            Pants - Textile
    Date purchased                  16 November 2019
    Tested by                       AMCAF, Deakin University
    Garment test reference          P19T11
    Rating first published          September 2020
    Rating updated                  4 September 2020


                                                Rjays All Seasons III
Page 5 of 5                                        Textile Pants                              motocap.com.au
