                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Rev'It
                                                           Model                    Component H2O
                                                           Type                     Jacket - Textile
                                                           Date purchased           28 July 2022
                                                           Sizes tested             L and XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $648.95


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating           ★             16.6
                                                           Abrasion                           1/10           0.32
                                                           Burst                              7/10           773
                                                           Impact                             3/10           24.2
                                                           MotoCAP Breathability Rating        ⯨            0.100
                                                           Moisture Vapour Resistance           -           121.1
                                                           Thermal Resistance                   -           0.203
                                                           Water resistance                   2/10           23.6
This garment is fitted with impact protectors for the elbows and shoulders. Replacing the elbow and
shoulder armour with higher performing impact protectors would improve the protection levels of this
garment. There are zipped vents in the upper arms, lower arms and sides of the back to allow controlled
airflow movement through the garment. The breathability rating is based on tests of the garment's materials
when all vents are closed. The breathability of this product may be better when the vents can be opened.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact

                                           Rev'It Component H2O
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.32




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.27   0.27     0.37     0.44     0.41     0.37                 0.35   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       30%            0.27     0.27     0.37     0.44     0.41     0.37                  0.35   P
Material B       70%            0.21     0.28     0.29     0.24     0.25     0.29                  0.26   P
Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.21     0.28     0.29     0.24     0.25     0.29                  0.26   P


Details of materials used in jacket
Material A       Heavy woven fabric shell with laminated water-resistant layer
Material B       Woven fabric shell with laminated water-resistant layer




                                           Rev'It Component H2O
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     7/10
                                                                          Burst score       773




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 996            1255           831           605           836           653           863         A
Zones 3 & 4 308            351            450           506           566           318           416         P




                                           Rev'It Component H2O
Page 3 of 5                                     Textile Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    3/10
                                                                               Impact score     24.2




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            27.1       M                                    27.1         M
Maximum force (kN)                            30.2       P                                    30.2         P
Coverage of Zone 1 area                       90%                                            110%
Coverage of Zone after displacement           60%                                             90%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           30.2             27.2           25.7              30.2              27.2          25.7
Impact Protector 2           26.4             25.7           29.3              26.4              25.7          29.3
Impact Protector 3           26.0             27.0           26.8              26.0              27.0          26.8




                                               Rev'It Component H2O
Page 4 of 5                                         Textile Jacket                                      motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                With water-resistant liner
Breathability rating       ⯨                            Breathability rating       N/A
Breathability score      0.100                          Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1            2          Average
Without removable liners                            124.6        117.6         121.1
With water-resistant liner                           N/A          N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1            2          Average
Without removable liners                            0.191        0.215            0.203
With water-resistant liner                           N/A          N/A              N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
              Water absorbed by garment           Water absorbed by underwear
              Volume (ml)    Percentage (%)       Volume (ml)    Percentage (%)
Jacket 1          112             12%                  29             10%
Jacket 2          100             11%                  69             23%
Average            98             10%                  69             24%

Location of wetting
There was minor wetting to the cotton underwear present at the waistband for one jacket and major
wetting to the cotton underwear at the cuffs of the sleeves of the other jacket tested.




   Assessment Details.
   Brand                            Rev'It
   Model                            Component H2O
   Type                             Jacket - Textile
   Date purchased                   28 July 2022
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J21T08
   Rating first published           October 2022
   Rating updated                   28 October 2022

                                              Rev'It Component H2O
Page 5 of 5                                        Textile Jacket                         motocap.com.au
