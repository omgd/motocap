                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Segura
                                                           Model                    Jericho
                                                           Type                     Jacket - Textile
                                                           Date purchased           17 June 2022
                                                           Sizes tested             XL and 2XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $449.95


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating         ★★★             41.0
                                                           Abrasion                            3/10          2.23
                                                           Burst                              10/10         1255
                                                           Impact                              8/10          57.7
                                                           MotoCAP Breathability Rating        ⯨            0.089
                                                           Moisture Vapour Resistance           -           163.3
                                                           Thermal Resistance                   -           0.243
                                                           Water resistance                   10/10           0.9
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are no vents to allow airflow movement through the garment.
Breathability was measured without the removable thermal liner installed.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                Segura Jericho
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      3/10
                                                                     Abrasion score       2.23




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        80%              5.26   3.99     8.09     7.09     3.58     4.06                 5.35   A
Material B        20%              0.88   0.76     0.96     1.17     0.89     0.89                 0.92   P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             0.88   0.76     0.96     1.17     0.89     0.89                 0.92   M

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.88     0.76     0.96     1.17     0.89     0.89                  0.92   M


Details of materials used in jacket
Material A       Quilted woven fabric shell with water-resistant inner liner
Material B       Woven fabric shell with water-resistant inner liner




                                                Segura Jericho
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1255




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1283           1297           1098          1584          1685          1032          1330        G
Zones 3 & 4 997            683            1178          1082          894           896           955         A




                                                 Segura Jericho
Page 3 of 5                                      Textile Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    8/10
                                                                               Impact score     57.7




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            12.6       G                                    13.3         G
Maximum force (kN)                            16.0       A                                    19.6         A
Coverage of Zone 1 area                      120%                                            100%
Coverage of Zone after displacement          100%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           10.0             14.1           15.4              10.7              12.2          19.6
Impact Protector 2           10.5              9.9           12.4              10.8              12.3          14.4
Impact Protector 3           10.8             14.0           16.0              12.3              14.3          13.6



                                                     Segura Jericho
Page 4 of 5                                          Textile Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                  With water-resistant liner
Breathability rating       ⯨                              Breathability rating       N/A
Breathability score      0.089                            Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1             2          Average
Without removable liners                            156.3          170.4         163.3
With water-resistant liner                           N/A            N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1             2          Average
Without removable liners                            0.243          0.242         0.243
With water-resistant liner                           N/A            N/A           N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.

                Water absorbed by garment        Water absorbed by underwear
                Volume (ml)    Percentage (%)    Volume (ml)    Percentage (%)
Jacket 1            294             19%               1.9            0.7%
Jacket 2            238             16%               3.4            1.1%
Average             266             17%               2.7            0.9%

Location of wetting
There was no visible wetting to the cotton underwear for either jackets tested.




    Assessment Details.
    Brand                           Segura
    Model                           Jericho
    Type                            Jacket - Textile
    Date purchased                  17 June 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J21T02
    Rating first published          September 2022
    Rating updated                  26 September 2022


                                                Segura Jericho
Page 5 of 5                                     Textile Jacket                             motocap.com.au
