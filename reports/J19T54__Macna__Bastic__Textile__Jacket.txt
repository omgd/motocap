                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Macna
                                                           Model                    Bastic
                                                           Type                     Jacket - Textile
                                                           Date purchased           8 July 2020
                                                           Sizes tested             XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $329.95


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating           ★             29.3
                                                           Abrasion                            1/10          0.43
                                                           Burst                              10/10         1261
                                                           Impact                              7/10          48.3
                                                           MotoCAP Breathability Rating        ★            0.218
                                                           Moisture Vapour Resistance           -            85.8
                                                           Thermal Resistance                   -           0.311
                                                           Water resistance                   1/10           83.0
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are zipped vents in the chest and arms and there is mesh in the upper
back to allow airflow movement through the garment. The breathability rating is based on tests of the
garment's materials when all vents are closed. The breathability of this product may be better when the
vents can be opened.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                Macna Bastic
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.43




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.59   0.39     0.53     0.37     0.39     0.33                 0.43   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.59     0.39     0.53     0.37     0.39     0.33                  0.43   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.59     0.39     0.53     0.37     0.39     0.33                  0.43   M


Details of materials used in jacket
Material A       Woven fabric shell, water-resistant layer and mesh inner liner




                                                Macna Bastic
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1261




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1055           1087           1945          1671          1137          1155.1        1342        G
Zones 3 & 4 1193           415            946           1126          1158          805.7         941         A




                                                 Macna Bastic
Page 3 of 5                                      Textile Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximium
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     48.3




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            15.2       A                                    12.5         G
Maximum force (kN)                            23.7       A                                    18.4         A
Coverage of Zone 1 area                      110%                                            110%
Coverage of Zone after displacement           70%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           11.3             13.1           18.0              10.0              11.2          15.3
Impact Protector 2           11.4             16.6           23.7              10.1              10.8          18.4
Impact Protector 3            8.7             10.9           22.9              10.3              12.3          14.5



                                                     Macna Bastic
Page 4 of 5                                          Textile Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ★                               Breathability rating       N/A
Breathability score      0.218                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            87.1            84.5          85.8
With water-resistant liner                          N/A             N/A           N/A
                              2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.321           0.302        0.311
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) by weight of the garment and undergarments due to water absorption.
                Water absorbed by garment        Water absorbed by underwear
                Volume (ml)    Percentage (%)    Volume (ml)    Percentage (%)
Jacket 1            1609            129%             277             99%
Jacket 2             578             49%             192             67%
Average             1094             89%             234             83%

Location of wetting
There was major wetting to the cotton underwear present at the chest and waistband for both jackets
tested.




    Assessment Details.
    Brand                           Macna
    Model                           Bastic
    Type                            Jacket - Textile
    Date purchased                  8 July 2020
    Tested by                       AMCAF, Deakin University
    Garment test reference          J19T54
    Rating first published          October 2020
    Rating updated                  15 October 2020


                                                 Macna Bastic
Page 5 of 5                                      Textile Jacket                             motocap.com.au
