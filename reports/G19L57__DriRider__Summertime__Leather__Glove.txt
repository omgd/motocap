                                                           This MotoCAP safety rating applies to:
                                                           Brand                   DriRider
                                                           Model                   Summertime
                                                           Type                    Glove - Leather
                                                           Date purchased          6 July 2020
                                                           Sizes tested            XL and 2XL
                                                           Test garment gender     M&F
                                                           Style                   All Purpose
                                                           RRP                     $99.95
                                                           Test Results Summary:
                                                                                          Rating      Result
                                                           MotoCAP Protection Rating                   2.0
                                                           Abrasion                        6/10        3.01
                                                           Seam strength                   1/10         2.0
                                                           Impact                          1/10         3.9
                                                           Water resistance                N/A         N/A
These gloves are fitted with impact protectors for the knuckles and palms. There is no impact protection
provided for the wrists. Mesh fabric in the fingers, wrist and thumb provide continuous airflow within the
glove.



Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                     Zone 4

    High risk of impact        High risk of abrasion     Medium risk of abrasion       Low risk of abrasion




                                            DriRider Summertime
Page 1 of 5                                     Leather Glove                                motocap.com.au
Abrasion Resistance
The glove was tested for abrasion resistance in accordance with MotoCAP test protocols. The table below
shows the test results for time to abrade to material failure for each sample by Zone, type and area
coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Leather shell
Material B:      Leather patch over leather shell
Material C:      Mesh fabric shell with fabric inner liner




Zone             Coverage        Abrasion time for each test (s)                                       Average
                 (%)                 1          2          3         4            5             6         (s)
Zone 2 area (High abrasion risk)
Material A       100%              1.20       1.90       1.19       1.65         1.65       1.49         1.51    M

Zone 3 area (Medium abrasion risk)
Material A       20%              1.20         1.90       1.19      1.65         1.65       1.49         1.51    M
Material B       80%              9.80         6.54       5.52      6.38         7.20       6.43         6.98    G
Zone 4 area (Low abrasion risk)
Material B       50%              9.80         6.54       5.52      6.38         7.20       6.43         6.98    G
Material C       50%              1.30         1.42       0.95      1.01         3.26       1.08         1.50    M
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each Zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




Determining Criteria     Area              Good        Acceptable    Marginal           Poor
High abrasion risk       Zone 2            > 4.0        2.7 - 4.0    1.2 - 2.6          < 1.2
Medium abrasion risk     Zone 3             3.5         2.5 - 3.5    1.0 - 2.4          < 1.0
Low abrasion risk        Zone 4            >2.5         1.8 - 2.5    0.8 - 1.7          < 0.8

                                              DriRider Summertime
Page 2 of 5                                       Leather Glove                                     motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The table below shows the seam
tensile strength in newtons per millimeter (N/mm) for each seam tested by Zone and the average result for
each Zone.
Seam tensile strength (N/mm)
Area                  1                  2               3                  4                   5               Average
Zones 2 & 3           9.77               11.84           3.19               8.32                10.28           8.68      M
Zone 4                16.00              16.00           9.74               7.10                10.31           11.83     A
The table below shows the force required to remove the restrained glove in newtons (N) for each of the five
gloves tested and the average result.
Glove restraint (N)
Glove                 1                  2               3                  4                   5               Average
Wrist restraint       83.9               71.6            98.6               92.4                58.5            81.0      P
The diagram below illustrates the tensile strength and wrist restraint results in terms of the likely
performance of the glove in a crash and is a pictorial representation of the data from the tables above.




Determining Criteria          Unit               Good        Acceptable            Marginal             Poor
Seam tensile strength         (N/mm)             > 15            10 - 15            6.5 - 9.9           < 6.5
Glove restraint                    (N)           > 400          300 - 400          200 - 299            <200




                                                    DriRider Summertime
Page 3 of 5                                             Leather Glove                                              motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
table below shows the test results for each strike on each impact protector in kilonewtons (kN) and their
area of coverage in percentage (%) within the Zone.
Impact protector type                       Knuckles                        Palm                        Wrist
Average force             (kN)                2.4         A                  6.5         M                       P
Maximum force             (kN)                 3.5        A                 6.8          M                       P
Coverage of zone 1 area                       120%                          20%                           0%
Impact forces are capped at a maximum of 10.0kN.
Individual test results
Impact force (kN)            Knuckles                                              Palm
Strike number                   1               2               3                   1               2
Impact Protector 1                2.3          2.5             3.5                     6.4          6.4
Impact Protector 2                2.0          2.4             2.0                     6.6          6.4
Impact Protector 3                2.2          2.4             2.0                     6.3          6.8
Impact force (kN)                Wrist    No impact protector present
Strike number                     1             2
Impact Protector 1
Impact Protector 2
Impact Protector 3
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




Determining Criteria          Unit          Good         Acceptable         Marginal         Poor
Knuckle and wrist Impact force (kN)          <2            2 - 4.9           5-8             >8
Palm impact force              (kN)          <4            4 - 5.9           6-8             >8
* Poor may also indicate that no impact protector is present in the glove
Areas shaded black are not considered in the impact protection ratings.

                                                DriRider Summertime
Page 4 of 5                                         Leather Glove                                          motocap.com.au
Water spray and rain resistance
This glove has not been advertised as water resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                          DriRider
   Model                          Summertime
   Type                           Glove - Leather
   Date purchased                 6 July 2020
   Tested by                      AMCAF, Deakin University
   Garment test reference         G19L57
   Rating first published         October 2020
   Rating updated                 15 October 2020



                                           DriRider Summertime
Page 5 of 5                                    Leather Glove                                motocap.com.au
