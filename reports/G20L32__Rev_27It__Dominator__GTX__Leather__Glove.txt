                                                           This MotoCAP safety rating applies to:
                                                           Brand:                       Rev'It
                                                           Model:                       Dominator GTX
                                                           Type:                        Glove - Leather
                                                           Date purchased:              25 May 2021
                                                           Sizes tested:                L, XL and 2XL
                                                           Test glove gender:           Male
                                                           Style:                       All Purpose
                                                           RRP:                         $378.95
                                                           Test Results Summary:
                                                                                              Rating      Score
                                                           MotoCAP Protection Rating         ★★★★          4.9
                                                           Abrasion                            10/10      6.42
                                                           Seam strength                        9/10      12.1
                                                           Impact                               7/10      11.7
                                                           Water resistance                     2/10      14.3
This glove is fitted with impact protectors for the knuckles and palm areas. There is no provision for
ventilation to allow airflow movement through the glove.




Gloves - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




                                                                             Impact protection
                                                                             Knuckles          
                                                                             Palm              




              Zone 1                     Zone 2                       Zone 3

       High risk of impact         High risk of abrasion     Medium risk of abrasion
      High risk of abrasion



                                            Rev'It Dominator GTX
Page 1 of 5                                    Leather Glove                                       motocap.com.au
Abrasion Resistance
The gloves were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each Zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating     10/10
                                                                      Abrasion score       6.42




Determining Criteria    Area             Good       Acceptable     Marginal        Poor
High abrasion risk      Zone 1 & 2       > 4.0       2.7 - 4.0     1.2 - 2.6       < 1.2
Medium abrasion risk    Zone 3            2.5        1.8 - 2.5     0.8 - 1.7       < 0.8


Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                  Average
Material A        45%             10.00  10.00    10.00    10.00    10.00    10.00                     10.00    G
Material B        55%              6.78   6.05     6.00     4.05     3.28     3.05                      4.87    G
Zone 2            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                  Average
Material C        40%             11.38   5.41     3.64    10.17     6.61     9.98                      7.87    G
Material B        60%              6.78   6.05     6.00     4.05     3.28     3.05                      4.87    G
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                  Average
Material C        15%             11.38   5.41     3.64    10.17     6.61     9.98                      7.87    G
Material B        85%              6.78   6.05     6.00     4.05     3.28     3.05                      4.87    G
Details of materials used in glove - derived from manufacturer provided information
Material A       Hard-shell armour over leather shell, water-resistant layer and fabric inner liner
Material B       Leather shell, water-resistance layer and fabric inner liner
Material C       Leather patch over leather shell, water-resistance layer and fabric inner liner




                                            Rev'It Dominator GTX
Page 2 of 5                                    Leather Glove                                  motocap.com.au
Seam Tensile Strength
The tensile strength of the gloves seams and glove restraint (the force required to drag off a properly
fastened glove) were tested in accordance with MotoCAP test protocols. The diagram below illustrates the
tensile strength and wrist restraint results in terms of the likely performance of the glove in a crash and is a
pictorial representation of the data from the tables below.




                                                                                   Seam Strength Performance
                                                                                   Seam strength rating  9/10
                                                                                   Seam strength score   12.1




Determining Criteria        Unit             Good        Acceptable          Marginal           Poor
Seam tensile strength       (N/mm)           > 11          9 - 11             6 - 8.9           <6
Glove restraint             (N)              > 200       100 - 200            50 - 99           <50

Individual Seam Strength Results: - The table below shows the seam tensile strength in newtons per
millimeter (N/mm) for each seam tested by Zone and the average result for each Zone.
Seam tensile strength (N/mm)
Area                    1            2               3                4                 5              Average
Zones 1 & 2             9.86         12.04           14.89            5.13              15.42          11.47     G
Zone 3                  11.39        16.00           11.08            10.57             16.00          13.01     G
Individual Glove Restraint Results: - The table below shows the force required to remove the restrained
glove in newtons (N) for each of the five gloves tested and the average result.
Glove restraint (N)
Glove                   1            2               3                4                 5              Average
Wrist restraint         400.0        400.0           400.0            400.0             400.0          400.0     G




                                                Rev'It Dominator GTX
Page 3 of 5                                        Leather Glove                                          motocap.com.au
Impact Protection
The glove was tested for impact protection and coverage In accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered in the impact protection ratings.




                                                                                   Impact Protection Performance
                                                                                   Impact rating    7/10
                                                                                   Impact score     11.7




Determining Criteria                      Unit           Good         Acceptable          Marginal         Poor
Knuckle Impact force                      (kN)            <2            2 - 4.9            5-8             >8
Palm impact force                         (kN)            <4            4 - 5.9            6-8             >8
* Poor may also indicate that no impact protector is present in the glove


Impact Protector Results: - The table below shows the test results for each strike on each impact protector
in kilonewtons (kN) and their area of coverage in percentage (%) within the Zone. Impact forces are capped
at a maximum of 10.0kN.
Impact protector type                        Knuckles                       Palm
Average force (kN)                             1.8        G                  2.5      G
Maximum force (kN)                             1.9        G                  3.7      G
Coverage of zone 1 area                       130%                          40%

Individual test results: - The table below shows the test results for each strike on each impact protector in kilonewtons
(kN) and the position of the strike. Impact forces are capped at a maximum of 10.0kN.
Impact protector type        Knuckles                                              Palm
Strike number                   1                2              3                   1                2
Impact Protector 1              1.9              1.9            1.9                 1.9              1.5
Impact Protector 2              1.9              1.9            1.3                 2.6              3.7
Impact Protector 3              1.9              1.9            1.3                 3.0              2.3




                                                 Rev'It Dominator GTX
Page 4 of 5                                         Leather Glove                                          motocap.com.au
Water spray and rain resistance
This glove is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the glove and under-glove due to water absorption.
                 Water absorbed by glove           Water absorbed by cotton glove
                 Volume (ml)    Percentage (%)     Volume (ml)    Percentage (%)
Pair 1                56.4          19%                28.6            138%
Pair 2                49.0          16%                28.5            128%
Average               52.7          17%                28.6            133%
Location of wetting:
Major visible wetting to the cotton under-glove was present over the entire hand in one of the four gloves
tested. There was major wetting to the wrist and minor wetting to the fingers of the second glove, minor
wetting to the fingers of third glove and no visible wetting in the fourth glove.




   Assessment Details.
   Brand                          Rev'It
   Model                          Dominator GTX
   Type                           Glove - Leather
   Date purchased                 25 May 2021
   Tested by                      AMCAF, Deakin University
   Report approved by             MotoCAP Chief Scientist
   Garment test reference         G20L32
   Rating first published         October 2021
   Rating updated                 25 October 2021



                                           Rev'It Dominator GTX
Page 5 of 5                                   Leather Glove                                motocap.com.au
