                                                                  This MotoCAP safety rating applies to:
                                                                  Brand                      Alpinestars
                                                                  Model                      Halo Drystar
                                                                  Type                       Jacket - Textile
                                                                  Date purchased             17 June 2022
                                                                  Sizes tested               XL and 2XL
                                                                  Test garment gender        Male
                                                                  Style                      Tourer
                                                                  RRP                        $699.99


                                                                  Test Results Summary                Rating         Score
                                                                  MotoCAP Protection Rating         ★★★★              54.1
                                                                  Abrasion                             6/10           4.21
                                                                  Burst                               10/10          1771
                                                                  Impact                               7/10           51.2
                                                                  MotoCAP Breathability Rating          ⯨            0.036
                                                                  Moisture Vapour Resistance            -            418.1
                                                                  Thermal Resistance                    -            0.248
                                                                  Water resistance                    10/10            0.7
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an aftermarket back
protector. There are zipped vents in the chest and back, together with mesh panels in the arms to allow airflow movement
through the garment. The breathability rating is based on tests of the garment's materials when all vents are closed. The
breathability of this product may be better when the vents can be opened. This garment has a removable water-resistant
liner. The breathability rating above was achieved with the water-resistant liner removed. When tested with the water-
resistant liner installed, the breathability rating reduced but remained within the 1 star range. This garment has removable
sleeves. All test results in this report are for the garment with its sleeves installed.


Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.

                                                                              Removable liners
                                                                              Thermal liner
                                                                              Water resistant liner            

                                                                              Removable impact protection
                                                                                              Pockets       Armour
                                                                              Elbow                          
                                                                              Shoulder                       
                                                                              Back              




           Zone 1                         Zone 2                         Zone 3                          Zone 4

   High risk of abrasion          High risk of abrasion        Medium risk of abrasion           Low risk of abrasion
    High risk of impact
                                               Alpinestars Halo Drystar
Page 1 of 5                                         Textile Jacket                                       motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                            Abrasion Resistance Performance
                                                                            Abrasion rating      6/10
                                                                            Abrasion score       4.21




Determining Criteria      Area              Good         Acceptable      Marginal         Poor
High abrasion risk        Zone 1 & 2        > 5.6         3.0 - 5.6      1.3 - 2.9        < 1.3
Medium abrasion risk      Zone 3            > 2.5         1.8 - 2.5      0.8 - 1.7        < 0.8
Low abrasion risk         Zone 4            >1.5          1.0 - 1.5      0.4 - 0.9        < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                       Average
Material A        100%             4.50   4.08     6.30     5.23     8.31    10.00                          6.40   G

Zone 3             Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                      Average
Material A         10%            4.50     4.08     6.30     5.23     8.31    10.00                         6.40   G
Material B         90%            0.61     0.54     0.45     0.67     0.48     0.59                         0.56   P
Zone 4             Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                      Average
Material B         100%           0.61     0.54     0.45     0.67     0.48     0.59                         0.56   M


Details of materials used in jacket
Material A         Coarse woven fabric shell, woven fabric shell, mesh inner liner, mesh panel and mesh inner liner
Material B         Woven fabric shell with mesh inner liner




                                              Alpinestars Halo Drystar
Page 2 of 5                                        Textile Jacket                                     motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1771




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 2010           1993           1998          1607          1074          2008          1782        G
Zones 3 & 4 1996           1861           1776          2000          1799          934           1727        G




                                           Alpinestars Halo Drystar
Page 3 of 5                                     Textile Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     51.2




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            16.2       A                                    17.7         A
Maximum force (kN)                            19.3       A                                    18.6         A
Coverage of Zone 1 area                      105%                                            105%
Coverage of Zone after displacement          100%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           16.0             17.0           13.9              16.0              17.0          17.9
Impact Protector 2           14.5             19.3           15.0              17.9              17.7          18.6
Impact Protector 3           16.1             16.8           16.9              17.5              18.6          17.9



                                              Alpinestars Halo Drystar
Page 4 of 5                                        Textile Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                  With water-resistant liner
Breathability rating       ⯨                              Breathability rating        ⯨
Breathability score      0.036                            Breathability score       0.083

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2         Average
Without removable liners                            444.2          391.9         418.1
With water-resistant liner                          331.5          324.0         327.8

Thermal Resistance - Rct (K.m2/W)                     1              2         Average
Without removable liners                            0.096          0.400         0.248
With water-resistant liner                          0.461          0.450         0.456

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment        Water absorbed by underwear
                Volume (ml)    Percentage (%)    Volume (ml)    Percentage (%)
Jacket 1            181              5%               0.5            0.2%
Jacket 2            159              4%               3.8            1.3%
Average             170              5%               2.2            0.7%

Location of wetting
There was no visible wetting to the cotton underwear for either jackets tested.




    Assessment Details.
    Brand                           Alpinestars
    Model                           Halo Drystar
    Type                            Jacket - Textile
    Date purchased                  17 June 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J21T04
    Rating first published          September 2022
    Rating updated                  26 September 2022

                                            Alpinestars Halo Drystar
Page 5 of 5                                      Textile Jacket                             motocap.com.au
