                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Belstaff
                                                           Model                    Ariel Pro Waxed Cotton
                                                           Type                     Jacket - Textile
                                                           Date purchased           1 June 2023
                                                           Sizes tested             XL and 2XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $959.99


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating         ★★★             42.0
                                                           Abrasion                            3/10          2.09
                                                           Burst                              10/10         1649
                                                           Impact                              7/10          50.0
                                                           MotoCAP Breathability Rating        ⯨            0.084
                                                           Moisture Vapour Resistance           -           202.6
                                                           Thermal Resistance                   -           0.282
                                                           Water resistance                   10/10           0.4
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are vent holes in the armpits to allow airflow movement through the
garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact

                                       Belstaff Ariel Pro Waxed Cotton
Page 1 of 5                                      Textile Jacket                                  motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      3/10
                                                                     Abrasion score       2.09




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        70%              3.64   5.65     5.11     6.55     5.26     6.43                 5.44   A
Material B        30%              1.37   1.00     1.64     1.10     1.31     1.68                 1.35   M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             1.37   1.00     1.64     1.10     1.31     1.68                 1.35   M

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           1.37     1.00     1.64     1.10     1.31     1.68                  1.35   A


Details of materials used in jacket
Material A       Double waxed woven fabric shell with fabric inner liner
Material B       Waxed woven fabric shell with fabric inner liner




                                      Belstaff Ariel Pro Waxed Cotton
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1649




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1644           1819           1686          2001          1406          1831          1731        G
Zones 3 & 4 1575           1014           1405          1572          1049          1312          1321        G




                                       Belstaff Ariel Pro Waxed Cotton
Page 3 of 5                                      Textile Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     50.0




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            16.8       A                                    15.8         A
Maximum force (kN)                            19.9       A                                    18.3         A
Coverage of Zone 1 area                      110%                                            100%
Coverage of Zone after displacement           90%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           17.0             17.5           15.8              14.2              16.3          16.8
Impact Protector 2           15.5             17.4           19.2              15.0              15.9          17.4
Impact Protector 3           14.6             14.3           19.9              13.8              14.6          18.3




                                         Belstaff Ariel Pro Waxed Cotton
Page 4 of 5                                        Textile Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                With water-resistant liner
Breathability rating       ⯨                            Breathability rating       N/A
Breathability score      0.084                          Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1            2         Average
Without removable liners                            205.5        199.6        202.6
With water-resistant liner                           N/A          N/A          N/A

Thermal Resistance - Rct (K.m2/W)                     1            2         Average
Without removable liners                            0.282        0.282           0.282
With water-resistant liner                           N/A          N/A             N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
              Water absorbed by garment          Water absorbed by underwear
              Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Jacket 1          362             25%                 1.0            0.3%
Jacket 2          334             23%                 1.1            0.4%
Average           348             24%                 1.1            0.4%

Location of wetting
There was no visible wetting to the cotton underwear for either jackets tested.




   Assessment Details.
   Brand                            Belstaff
   Model                            Ariel Pro Waxed Cotton
   Type                             Jacket - Textile
   Date purchased                   1 June 2023
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           J23T38
   Rating first published           September 2023
   Rating updated                   15 September 2023


                                        Belstaff Ariel Pro Waxed Cotton
Page 5 of 5                                       Textile Jacket                          motocap.com.au
