                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  Dainese
                                                           Model:                  Air Frame D1
                                                           Type:                   Jacket - Textile
                                                           Date purchased:         16 November 2019
                                                           Sizes tested:           42 and 46
                                                           Gender:                 F
                                                           Style:                  All Purpose
                                                           Test code:              J19T34
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating         ⯨          10.6
                                                           Abrasion                         1/10        0.35
                                                           Burst                            8/10        834
                                                           Impact                           1/10         1.8
                                                           MotoCAP Comfort Rating                   0.470
                                                           Moisture Vapour Resistance                   23.4
                                                           Thermal Resistance                          0.183
                                                           Water resistance                 1/10         35
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Mesh panels are located in the arms, chest and back to allow airflow movement
through the garment. This garment has a removable water-resistant liner. The comfort rating above was
achieved with the liner removed. When tested with the liner installed, the comfort rating reduced but
remained within the 3 star range.
Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact

                                            Dainese Air-Frame D1
Page 1 of 5                                     Textile Jacket                                motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Woven fabric shell with mesh inner liner
Material B:      Mesh fabric shell with mesh inner liner




Zone             Coverage       Abrasion time for each test (seconds)                                 Average
                 (%)                1          2          3          4           5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       90%              0.53       0.42       0.66       0.48         0.37       0.49         0.49      P
Material B       10%              0.31       0.17       0.24       0.22         0.16       0.27         0.23      P
Zone 3 area (Medium abrasion risk)
Material A       10%              0.53       0.42       0.66       0.48         0.37       0.49         0.49      P
Material B       90%              0.31       0.17       0.24       0.22         0.16       0.27         0.23      P
Zone 4 area (Low abrasion risk)
Material A       10%              0.53       0.42       0.66       0.48         0.37       0.49         0.49      M
Material B       90%              0.31       0.17       0.24       0.22         0.16       0.27         0.23      P
Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                          Good       Acceptable     Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:       > 5.6       3.0 - 5.6     1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:        > 2.5       1.8 - 2.5     0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:        >1.5        1.0 - 1.5     0.4 - 0.9          < 0.4

                                             Dainese Air-Frame D1
Page 2 of 5                                      Textile Jacket                                    motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2               3                  4                 5              Average
Zones 1 & 2          706            630             1017               1058              1114           905       A
Zone EZ             818             1113            735                662               839            834       A
Zones 3 & 4         915             562             701                660               625            693       M
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good           Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000         800 - 1000         500 - 799          < 500




                                              Dainese Air-Frame D1
Page 3 of 5                                       Textile Jacket                                          motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The table below shows the test results for each strike on each impact protector in kilonewtons (kN) and
their area of coverage as a proportion (%) of the Zone.
Impact protector type                        Elbow                                            Shoulder
Average force (kN)                            36.9       P                                      25.4          M
Maximum force (kN)                            52.1       P                                          35.7      P
Coverage of zone 1 area                      115%                                                   85%
Coverage of zone after displacement          100%                                                  100%
Individual test results
Impact force (kN)             Elbow                                           Shoulder
Strike location                 A              B              C                  A                  B              C
Impact Protector 1             19.8           39.3           41.1                   18.4           20.1           32.7
Impact Protector 2             22.4           41.3           52.1                   16.2           31.7           35.7
Impact Protector 3             20.7           52.1           43.4                   18.2           24.0           31.6
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good        Acceptable        Marginal         Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24         25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                               Dainese Air-Frame D1
Page 4 of 5                                        Textile Jacket                                          motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                               1                 2        Average
Moisture Vapour Resistance - Ret             21.3            25.5          23.4
               2
     (kPam /W)
                                               1                 2        Average
Thermal Resistance - Rct                     0.170          0.196          0.183
           2
      (Km /W)



Water spray and rain resistance
This garment is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                   Water absorbed by garment           Water absorbed by underwear
                   Volume (ml)    Percentage (%)       Volume (ml)    Percentage (%)
Jacket 1               348             38%                  78             28%
Jacket 2               241            28%                  116             42%
Average                294            33%                   97             35%

Location of wetting:
Visible wetting to the cotton underwear was present at the neck of both garments tested.




                                                   Dainese Air-Frame D1
Page 5 of 5                                            Textile Jacket                      motocap.com.au
