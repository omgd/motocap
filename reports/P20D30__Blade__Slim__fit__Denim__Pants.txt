                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Blade
                                                           Model                    Slim fit
                                                           Type                     Pants - Denim
                                                           Date purchased           1 March 2022
                                                           Sizes tested             36 and 38
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $189.00


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating           ★             14.5
                                                           Abrasion                           1/10           1.00
                                                           Burst                              9/10           947
                                                           Impact                             1/10            0.0
                                                           MotoCAP Breathability Rating     ★★★★            0.542
                                                           Moisture Vapour Resistance          -             20.0
                                                           Thermal Resistance                  -            0.181
                                                           Water resistance                   N/A            N/A
Pockets are provided at the knees and hips for fitting aftermarket impact protectors. Adding knee and hip
impact protectors would improve the protection levels of this garment. There are no vents to allow airflow
movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Knee             
                                                                      Hip              




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                 Blade Slim fit
Page 1 of 5                                      Denim Pants                                    motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       1.00




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             1.12   1.16     1.06     1.35     1.11     1.06                 1.14   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.54     0.66     0.69     1.15     0.94                           0.80   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.54     0.66     0.69     1.15     0.94                           0.80   M


Details of materials used in jacket
Material A       Denim fabric shell with para-aramid fabric inner liner
Material B       Denim fabric shell




                                                Blade Slim fit
Page 2 of 5                                     Denim Pants                                  motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     9/10
                                                                          Burst score       947




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1154           1654           971           993           676           561           1002        G
Zones 3 & 4 728            655            702           616           895           773           728         M




                                                 Blade Slim fit
Page 3 of 5                                      Denim Pants                                   motocap.com.au
Impact Protection
These pants was not tested for impact protection as impact protectors were not provided with the garment.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    1/10
                                                                               Impact score      0.0




Determining Criteria         Unit           Good        Acceptable      Marginal         Poor*
Impact force                 (kN)           < 15          15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Knee                                                Hip
Average force (kN)                                       P                                                P
Maximum force (kN)                                       P                                                P
Coverage of Zone 1 area                       0%                                                 0%
Coverage of Zone after displacement           0%                                                 0%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee       No impact protector present             Hip       No impact protector present
Strike location             Centre           Mid           Edge                Centre          Mid           Edge
Impact Protector 1
Impact Protector 2
Impact Protector 3



                                                    Blade Slim fit
Page 4 of 5                                         Denim Pants                                        motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating  ★★★★                                 Breathability rating       N/A
Breathability score      0.542                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            19.7            20.2          20.0
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1              2          Average
Without removable liners                            0.179           0.182        0.181
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Blade
    Model                           Slim fit
    Type                            Pants - Denim
    Date purchased                  1 March 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          P20D30
    Rating first published          June 2022
    Rating updated                  23 June 2022


                                                 Blade Slim fit
Page 5 of 5                                      Denim Pants                                motocap.com.au
