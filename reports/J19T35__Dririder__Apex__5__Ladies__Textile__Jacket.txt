                                                           This MotoCAP safety rating applies to:
                                                           Brand:                  DriRider
                                                           Model:                  Apex 5 Ladies
                                                           Type:                   Jacket - Textile
                                                           Date purchased:         16 November 2019
                                                           Sizes tested:           14 and 18
                                                           Gender:                 F
                                                           Style:                  Tourer
                                                           Test code:              J19T35
                                                           Test Results Summary:
                                                                                           Rating      Score
                                                           MotoCAP Protection Rating                   26.2
                                                           Abrasion                         2/10        1.54
                                                           Burst                           10/10       1371
                                                           Impact                           2/10        16.1
                                                           MotoCAP Comfort Rating                      0.18
                                                           Moisture Vapour Resistance                  109.8
                                                           Thermal Resistance                          0.338
                                                           Water resistance                 1/10         31
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are vents in the chest and back to allow airflow movement through the
garment. The thermal comfort rating is based on tests of the breathability of the garment when all vents are
closed. The thermal comfort of this product may be better when the vents can be opened.

Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion        Low risk of abrasion
    High risk of impact

                                            Dririder Apex 5 Ladies
Page 1 of 5                                      Textile Jacket                               motocap.com.au
Abrasion Resistance
The garment was tested for abrasion resistance in accordance with MotoCAP test protocols. The table
below shows the test results for time to abrade through all layers of the materials. Calculated for each
sample by Zone, type and area coverage of each material as a proportion of that Zone.
Details of materials used in garment:
Material A:      Heavy woven fabric shell, foam layer, water resistant layer and mesh inner liner
Material B:      Woven fabric shell, water resistant layer and mesh inner liner




Zone             Coverage      Abrasion time for each test (seconds)                                       Average
                 (%)                1         2          3          4                 5             6      (seconds)
Zone 1 and 2 areas (High abrasion risk)
Material A       90%              3.31      1.95       2.60       2.52               2.40       3.48         2.71      M
Material B       10%              0.73      0.80       0.52       0.52               0.70       0.45         0.62      P
Zone 3 area (Medium abrasion risk)
Material B       100%             0.73      0.80       0.52       0.52               0.70       0.45         0.62      P

Zone 4 area (Low abrasion risk)
Material B       100%                 0.73       0.80      0.52      0.52            0.70       0.45         0.62      M


Abrasion times are capped at a maximum of 10.00s.
The diagram below is a visual indication of the likely abrasion performance of the materials in each zone
calculated from the data in the table above. The colour coding is based on the worst performing material in
each zone.




                                             Good       Acceptable       Marginal           Poor
Determining Criteria
High abrasion risk        Zone 1/2:          > 5.6       3.0 - 5.6       1.3 - 2.9          < 1.3
Medium abrasion risk       Zone 3:           > 2.5       1.8 - 2.5       0.8 - 1.7          < 0.8
Low abrasion risk          Zone 4:           >1.5        1.0 - 1.5       0.4 - 0.9          < 0.4

                                                Dririder Apex 5 Ladies
Page 2 of 5                                          Textile Jacket                                     motocap.com.au
Burst Strength
The garment’s burst strength was tested in accordance with MotoCAP test protocols. The table below shows
the burst pressure in kilopascals (kPA) for each sample tested by Zone and the average result for each zone.


Burst pressure (kPA)
Area                 1              2               3                  4                 5              Average
Zones 1 & 2          1946           1956            1948               1525              1129           1701      G
Zone EZ             1436            1503            823                1008              1266           1207      G
Zones 3 & 4         1411            976             1113               1046              633            1035      G
The diagram below illustrates the burst strength results in terms of the likely performance of the garment in
an impact and is a pictorial representation of the data from the table above.




                                           Good           Acceptable          Marginal          Poor
Determining Criteria
Burst strength              (kPa)          > 1000         800 - 1000      500 - 799             < 500




                                              Dririder Apex 5 Ladies
Page 3 of 5                                        Textile Jacket                                         motocap.com.au
Impact Protection
The garment was tested for impact protection and coverage in accordance with MotoCAP test protocols.
The table below shows the test results for each strike on each impact protector in kilonewtons (kN) and
their area of coverage as a proportion (%) of the Zone.
Impact protector type                        Elbow                                            Shoulder
Average force (kN)                            28.3       M                                      28.0          M
Maximum force (kN)                            39.0       P                                          38.7      P
Coverage of zone 1 area                       80%                                                  100%
Coverage of zone after displacement          100%                                                  100%
Individual test results
Impact force (kN)             Elbow                                           Shoulder
Strike location                 A              B              C                  A                  B              C
Impact Protector 1             19.8           29.3           39.0                   17.1           26.8           38.7
Impact Protector 2             22.3           25.0           38.1                   22.8           23.3           36.5
Impact Protector 3             23.7           26.6           30.8                   20.0           29.3           37.2
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table above. The colour coding is based on the worst performing score for average or
maximium force for each impact zone.




                                            Good        Acceptable        Marginal         Poor*
Determining Criteria
Impact force                    (kN)        < 15          15 - 24         25 - 30          > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment
Areas shaded black are not considered in the impact protection ratings.




                                               Dririder Apex 5 Ladies
Page 4 of 5                                         Textile Jacket                                         motocap.com.au
Thermal comfort
The garment was tested for thermal comfort following the MotoCAP test protocols. The table below shows
the moisture vapour resistance and the thermal resistance values obtained.
                                               1                 2          Average
Moisture Vapour Resistance - Ret             113.9           105.7           109.8
               2
     (kPam /W)
                                               1                 2          Average
Thermal Resistance - Rct                     0.329           0.347           0.338
           2
      (Km /W)



Water spray and rain resistance
This garment is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                   Water absorbed by garment           Water absorbed by underwear
                   Volume (ml)    Percentage (%)       Volume (ml)    Percentage (%)
Jacket 1               507             34%                 101             36%
Jacket 2               496            33%                   71               25%
Average                501            34%                   86               31%

Location of wetting:
Visible wetting to the cotton underwear was present over the chest and cuffs of one jacket and the neck of
the other jacket tested.




                                                   Dririder Apex 5 Ladies
Page 5 of 5                                             Textile Jacket                     motocap.com.au
