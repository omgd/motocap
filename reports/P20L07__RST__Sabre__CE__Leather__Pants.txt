                                                           This MotoCAP safety rating applies to:
                                                           Brand                   RST
                                                           Model                   Sabre CE
                                                           Type                    Pants - Leather
                                                           Date purchased          15 February 2022
                                                           Sizes tested            46 and 58
                                                           Test garment gender     Male
                                                           Style                   Sports
                                                           RRP                     $399.99


                                                           Test Results Summary          Rating         Score
                                                           MotoCAP Protection Rating    ★★★★★            67.2
                                                           Abrasion                      10/10           7.28
                                                           Burst                         10/10          1535
                                                           Impact                         7/10           51.5
                                                           MotoCAP Breathability Rating   ★★            0.345
                                                           Moisture Vapour Resistance       -            35.2
                                                           Thermal Resistance               -           0.202
                                                           Water resistance               N/A            N/A
This garment is fitted with impact protectors for the knees and hips. Replacing the knee armour with higher
performing impact protectors would improve the protection levels of this garment. Perforated leather is
located in the front of upper legs to allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                    Pockets    Armour
                                                                      Knee                      
                                                                      Hip                       




          Zone 1                      Zone 2                      Zone 3                      Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion       Low risk of abrasion
    High risk of impact

                                                RST Sabre CE
Page 1 of 5                                     Leather Pants                                 motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating     10/10
                                                                      Abrasion score       7.28




Determining Criteria    Area             Good        Acceptable     Marginal        Poor
High abrasion risk      Zones 1 & 2      > 5.6       3.0 - 5.6      1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3           > 2.5       1.8 - 2.5      0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4           >1.5        1.0 - 1.5      0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.

Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                 Average
Material A        30%             10.00  10.00    10.00    10.00    10.00    10.00                    10.00    G
Material B        70%             10.00  10.00    10.00    10.00    10.00                             10.00    G
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                 Average
Material C        80%              3.44   4.26     5.15     3.30     4.94     2.58                     3.94    G
Material D        20%              2.84   2.69     2.83                                                2.78    G
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6                 Average
Material C        50%              3.44   4.26     5.15     3.30     4.94     2.58                     3.94    G
Material E        50%              0.83   0.78     0.66     0.76     0.77     0.78                     0.76    M
Details of materials used in jacket
Material A       Velcro patch over leather shell, leather layer, foam layer and fabric inner layer
Material B       Leather patch over leather shell with mesh inner layer
Material C       Leather shell with mesh inner layer
Material D       Perforated leather shell with mesh inner layer
Material E       Stretch fabric shell with mesh inner layer




                                                 RST Sabre CE
Page 2 of 5                                      Leather Pants                                motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1535




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 2012           1824           2001          772           1214          1490          1552        G
Zones 3 & 4 1565           1431           1168          1020          1605          2006          1466        G




                                                 RST Sabre CE
Page 3 of 5                                      Leather Pants                                 motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     51.5




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15          15 - 24       25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                         Knee                                                Hip
Average force (kN)                             23.0       A                                       10.3      G
Maximum force (kN)                             25.4       M                                       12.3      G
Coverage of Zone 1 area                       110%                                               130%
Coverage of Zone after displacement           100%                                               100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee                                               Hip
Strike location             Centre            Mid             Edge             Centre            Mid            Edge
Impact Protector 1           21.4             21.8            25.4              9.2              10.0           11.4
Impact Protector 2           22.2             22.2            23.3              9.1              10.2           12.3
Impact Protector 3           23.9             23.0            24.0              9.1               9.7           11.4




                                                      RST Sabre CE
Page 4 of 5                                           Leather Pants                                      motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★★                                Breathability rating       N/A
Breathability score      0.345                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)          1               2          Average
Without removable liners                            35.1            35.3         35.2
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1               2         Average
Without removable liners                            0.205           0.199        0.202
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




   Assessment Details.
   Brand                            RST
   Model                            Sabre CE
   Type                             Pants - Leather
   Date purchased                   15 February 2022
   Tested by                        AMCAF, Deakin University
   Report approved by               MotoCAP Chief Scientist
   Garment test reference           P20L07
   Rating first published           July 2022
   Rating updated                   25 July 2022

                                                 RST Sabre CE
Page 5 of 5                                      Leather Pants                              motocap.com.au
