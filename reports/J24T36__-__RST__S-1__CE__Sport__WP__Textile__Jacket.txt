                                                           This MotoCAP safety rating applies to:
                                                           Brand                    RST
                                                           Model                    S-1 CE Sport WP
                                                           Type                     Textile Jacket
                                                           Date purchased           13 March 2024
                                                           Sizes tested             L and XL
                                                           Test garment gender      Male
                                                           Style                    Sports
                                                           RRP                      $279.99


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating          ★★             32.6
                                                           Abrasion                            1/10          0.71
                                                           Burst                              10/10         1604
                                                           Impact                              6/10          43.2
                                                           MotoCAP Breathability Rating        ⯨            0.059
                                                           Moisture Vapour Resistance           -           309.1
                                                           Thermal Resistance                   -           0.306
                                                           Water resistance                   1/10           47.5
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are zipped vents in the chest and back to allow controlled airflow
movement through the garment. The breathability rating is based on tests of the garment's materials when
all vents are closed. The breathability of this product may be better when the vents are opened.
Breathability was measured without the removable thermal liner installed.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                             RST S-1 CE Sport WP
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.71




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.86   0.80     0.68     0.57     0.65     0.71                 0.71   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.86     0.80     0.68     0.57     0.65     0.71                  0.71   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.86     0.80     0.68     0.57     0.65     0.71                  0.71   M


Details of materials used in jacket
Material A       Woven fabric shell, water-resistant layer and mesh inner liner




                                            RST S-1 CE Sport WP
Page 2 of 5                                    Textile Jacket                                motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1604




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1764           1648           1674          1587          1697          1537          1651        G
Zones 3 & 4 1491           1165           1210          1462          1481          1679          1415        G




                                             RST S-1 CE Sport WP
Page 3 of 5                                     Textile Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    6/10
                                                                               Impact score     43.2




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            19.9       A                                    18.0         A
Maximum force (kN)                            21.8       A                                    21.2         A
Coverage of Zone 1 area                      100%                                            100%
Coverage of Zone after displacement           90%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           19.3             20.2           19.8              16.2              17.5          18.3
Impact Protector 2           19.7             18.8           21.8              18.5              15.7          16.8
Impact Protector 3           20.8             19.3           19.1              18.7              19.1          21.2



                                                RST S-1 CE Sport WP
Page 4 of 5                                        Textile Jacket                                       motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ⯨                               Breathability rating       N/A
Breathability score      0.059                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)            1              2         Average
Without removable liners                             301.1          317.0          309.1
With water-resistant liner                            N/A            N/A            N/A

Thermal Resistance - Rct (K.m2/W)                      1              2         Average
Without removable liners                             0.307          0.306          0.306
With water-resistant liner                            N/A            N/A            N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment          Water absorbed by underwear
                Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Jacket 1            288             17%                129             46%
Jacket 2            275             15%                143             49%
Average             281             16%                136             47%

Location of wetting
There was major wetting to the cotton underwear present on the chest for both jackets tested.




    Assessment Details.
    Brand                           RST
    Model                           S-1 CE Sport WP
    Type                            Textile Jacket
    Date purchased                  13 March 2024
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J24T36
    Rating first published          July 2024
    Rating updated                  31 July 2024

                                                RST S-1 CE Sport WP
Page 5 of 5                                        Textile Jacket                           motocap.com.au
