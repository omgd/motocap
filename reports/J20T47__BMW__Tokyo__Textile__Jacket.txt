                                                           This MotoCAP safety rating applies to:
                                                           Brand                    BMW
                                                           Model                    Tokyo
                                                           Type                     Jacket - Textile
                                                           Date purchased           10 March 2022
                                                           Sizes tested             L and XL
                                                           Test garment gender      Male
                                                           Style                    Streetwear
                                                           RRP                      $425.00


                                                           Test Results Summary               Rating       Score
                                                           MotoCAP Protection Rating          ★★            35.6
                                                           Abrasion                            2/10         1.43
                                                           Burst                              10/10        1316
                                                           Impact                              7/10         50.8
                                                           MotoCAP Breathability Rating      ★★★           0.443
                                                           Moisture Vapour Resistance           -           40.4
                                                           Thermal Resistance                   -          0.298
                                                           Water resistance                    N/A          N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are zipped vents in the chest to allow controlled airflow movement
through the garment. The breathability rating is based on tests of the garment's materials when all vents are
closed. The breathability of this product may be better when the vents can be opened.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets      Armour
                                                                      Elbow                        
                                                                      Shoulder                     
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                 BMW Tokyo
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      2/10
                                                                     Abrasion score       1.43




Determining Criteria    Area             Good        Acceptable    Marginal       Poor
High abrasion risk      Zone 1 & 2       > 5.6        3.0 - 5.6    1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3           > 2.5        1.8 - 2.5    0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4           >1.5         1.0 - 1.5    0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        90%              2.09   1.48     2.13     1.77     1.66     1.55                 1.78     M
Material B        10%              0.83   0.66     0.89     0.82     0.78     0.85                 0.81     P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        80%              2.09   1.48     2.13     1.77     1.66     1.55                 1.78     M
Material B        20%              0.83   0.66     0.89     0.82     0.78     0.85                 0.81     M
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             0.83   0.66     0.89     0.82     0.78     0.85                 0.81     M


Details of materials used in jacket
Material A       Woven fabric shell, woven fabric layer with fabric inner liner
Material B       Woven fabric shell with fabric inner liner




                                                  BMW Tokyo
Page 2 of 5                                      Textile Jacket                              motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1316




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1378           1948           1948          990           924           1008          1366        G
Zones 3 & 4 1941           877            661           1945          468           788           1113        G




                                                  BMW Tokyo
Page 3 of 5                                      Textile Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     50.8




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            15.8       A                                    12.5         G
Maximum force (kN)                            21.2       A                                    16.0         A
Coverage of Zone 1 area                       90%                                            120%
Coverage of Zone after displacement           90%                                            100%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           13.4             16.0           18.1              11.7              11.6          12.6
Impact Protector 2           15.1             12.7           21.2              11.8              12.1          16.0
Impact Protector 3           14.8             14.3           16.2              11.6              11.6          13.5



                                                      BMW Tokyo
Page 4 of 5                                          Textile Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating   ★★★                                 Breathability rating       N/A
Breathability score      0.443                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            41.7            39.1          40.4
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.297           0.299        0.298
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           BMW
    Model                           Tokyo
    Type                            Jacket - Textile
    Date purchased                  10 March 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J20T47
    Rating first published          May 2022
    Rating updated                  23 May 2022


                                                  BMW Tokyo
Page 5 of 5                                      Textile Jacket                             motocap.com.au
