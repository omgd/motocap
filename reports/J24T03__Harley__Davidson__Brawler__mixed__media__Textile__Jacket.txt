                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Harley Davidson
                                                           Model                    Brawler mixed media
                                                           Type                     Jacket - Textile
                                                           Date purchased           19 August 2023
                                                           Sizes tested             L and XL
                                                           Test garment gender      Male
                                                           Style                    Cruiser
                                                           RRP                      $681.84


                                                           Test Results Summary               Rating       Score
                                                           MotoCAP Protection Rating          ★★            30.0
                                                           Abrasion                            3/10         2.52
                                                           Burst                              10/10        1736
                                                           Impact                              1/10          0.0
                                                           MotoCAP Breathability Rating       ★★           0.390
                                                           Moisture Vapour Resistance           -           39.9
                                                           Thermal Resistance                   -          0.260
                                                           Water resistance                   1/10          N/A
Pockets are provided at the shoulders, elbows and back for fitting aftermarket impact protectors. Adding
elbow and shoulder impact protectors would improve the protection levels of this garment. Mesh panels are
located in the arms, chest and back to allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets      Armour
                                                                      Elbow            
                                                                      Shoulder         
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                   Harley Davidson Brawler mixed media
Page 1 of 5                                    Textile Jacket                                    motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      3/10
                                                                     Abrasion score       2.52




Determining Criteria    Area             Good       Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2       > 5.6       3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3           > 2.5       1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4           >1.5        1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        65%             10.00  10.00    10.00     0.00     0.00     0.00                 10.00    G
Material B        35%              1.31   2.64     1.22     1.17     1.91     1.04                  1.55    M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        40%              1.31   2.64     1.22     1.17     1.91     1.04                  1.55    M
Material C        60%              1.06   0.70     0.71     1.17     0.70     1.05                  0.90    M
Zone 4            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        50%              1.31   2.64     1.22     1.17     1.91     1.04                  1.55    G
Material C        50%              1.06   0.70     0.71     1.17     0.70     1.05                  0.90    M
Details of materials used in jacket
Material A       Woven fabric shell, foam and rubber layer, laminated fabric layer with mesh inner liner
Material B       Woven fabric shell with laminated backing
Material C       Mesh fabric shell with mesh inner liner




                                     Harley Davidson Brawler mixed media
Page 2 of 5                                      Textile Jacket                              motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1736




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1874           1854           1733          1865          1603          1885          1802        G
Zones 3 & 4 1476           1573           1580          1573          1102          1517          1470        G




                                   Harley Davidson Brawler mixed media
Page 3 of 5                                    Textile Jacket                                  motocap.com.au
Impact Protection
This jacket was not tested for impact protection as impact protectors were not provided with the garment.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    1/10
                                                                               Impact score      0.0




Determining Criteria         Unit           Good        Acceptable      Marginal         Poor*
Impact force                 (kN)           < 15          15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            0.0        P                                    0.0         P
Maximum force (kN)                            0.0        P                                    0.0         P
Coverage of Zone 1 area                       0%                                              0%
Coverage of Zone after displacement           0%                                              0%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow       No impact protector present          Shoulder     No impact protector present
Strike location             Centre           Mid           Edge               Centre           Mid           Edge
Impact Protector 1
Impact Protector 2
Impact Protector 3



                                      Harley Davidson Brawler mixed media
Page 4 of 5                                       Textile Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating     ★★                                Breathability rating       N/A
Breathability score      0.390                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            41.8            38.0          39.9
With water-resistant liner                          N/A             N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1              2          Average
Without removable liners                            0.261           0.259        0.260
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Harley Davidson
    Model                           Brawler mixed media
    Type                            Jacket - Textile
    Date purchased                  19 August 2023
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J24T03
    Rating first published          December 2023
    Rating updated                  4 December 2023


                                    Harley Davidson Brawler mixed media
Page 5 of 5                                     Textile Jacket                              motocap.com.au
