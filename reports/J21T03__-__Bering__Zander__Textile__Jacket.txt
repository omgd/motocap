                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Bering
                                                           Model                    Zander
                                                           Type                     Jacket - Textile
                                                           Date purchased           17 June 2022
                                                           Sizes tested             M and XL
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $299.95


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating          ★★             29.4
                                                           Abrasion                           1/10           1.10
                                                           Burst                              9/10           953
                                                           Impact                             7/10           47.9
                                                           MotoCAP Breathability Rating        ⯨            0.158
                                                           Moisture Vapour Resistance           -           107.1
                                                           Thermal Resistance                   -           0.282
                                                           Water resistance                   8/10            2.6
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. There are zipped vents in the lower arms to allow controlled airflow movement
through the garment. The breathability rating is based on tests of the garment's materials when all vents are
closed. The breathability of this product may be better when the vents can be opened. Breathability was
measured without the removable thermal liner installed.



Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner                    
                                                                      Water resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                         
                                                                      Shoulder                      
                                                                      Back             




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                Bering Zander
Page 1 of 5                                     Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                       Abrasion Resistance Performance
                                                                       Abrasion rating      1/10
                                                                       Abrasion score       1.10




Determining Criteria    Area            Good        Acceptable     Marginal        Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9       < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7       < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9       < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.96   1.11     0.98     1.23     1.19     1.13                 1.10   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.96     1.11     0.98     1.23     1.19     1.13                  1.10   M

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material A       100%           0.96     1.11     0.98     1.23     1.19     1.13                  1.10   A


Details of materials used in jacket
Material A       Woven fabric shell with water-resistant inner liner




                                                Bering Zander
Page 2 of 5                                     Textile Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     9/10
                                                                          Burst score       953




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1018           988            993           654           1083          1066          967         A
Zones 3 & 4 880            853            958           979           783           939           899         A




                                                 Bering Zander
Page 3 of 5                                      Textile Jacket                                motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    7/10
                                                                               Impact score     47.9




Determining Criteria         Unit            Good        Acceptable     Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            16.1       A                                    18.0         A
Maximum force (kN)                            17.1       A                                    19.9         A
Coverage of Zone 1 area                      120%                                             90%
Coverage of Zone after displacement           90%                                             90%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           14.3             16.4           16.5              15.9              18.2          18.9
Impact Protector 2           16.1             15.6           17.1              17.7              18.6          19.9
Impact Protector 3           16.9             16.2           16.1              17.7              17.5          17.4



                                                     Bering Zander
Page 4 of 5                                          Textile Jacket                                     motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                  With water-resistant liner
Breathability rating       ⯨                              Breathability rating       N/A
Breathability score      0.158                            Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1             2          Average
Without removable liners                            107.8          106.5         107.1
With water-resistant liner                           N/A            N/A           N/A

Thermal Resistance - Rct (K.m2/W)                     1             2          Average
Without removable liners                            0.283          0.282         0.282
With water-resistant liner                           N/A            N/A           N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment        Water absorbed by underwear
                Volume (ml)    Percentage (%)    Volume (ml)    Percentage (%)
Jacket 1            254             22%               6.7            2.3%
Jacket 2            276             23%               8.7            3.0%
Average             265             22%               7.7            2.6%

Location of wetting
There was minor wetting to the cotton underwear present at the neck for both jackets tested.




    Assessment Details.
    Brand                           Bering
    Model                           Zander
    Type                            Jacket - Textile
    Date purchased                  17 June 2022
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J21T03
    Rating first published          September 2022
    Rating updated                  26 September 2022

                                                 Bering Zander
Page 5 of 5                                      Textile Jacket                            motocap.com.au
