                                                           This MotoCAP safety rating applies to:
                                                           Brand                    RST
                                                           Model                    Skinny
                                                           Type                     Pants - Denim
                                                           Date purchased           25 May 2021
                                                           Sizes tested             12 and 16
                                                           Test garment gender      Female
                                                           Style                    Sports
                                                           RRP                      $199.95


                                                           Test Results Summary              Rating         Score
                                                           MotoCAP Protection Rating           ★             14.9
                                                           Abrasion                            1/10          0.80
                                                           Burst                              10/10         1084
                                                           Impact                              1/10           0.0
                                                           MotoCAP Breathability Rating      ★★★            0.460
                                                           Moisture Vapour Resistance          -             29.0
                                                           Thermal Resistance                  -            0.223
                                                           Water resistance                   N/A            N/A
This garment is not fitted with impact protectors. There are no pockets provided at the knees or hips for
fitting aftermarket impact protectors. Adding knee and hip impact protectors would improve the protection
levels of this garment. There are no vents to allow airflow movement through the garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Knee
                                                                      Hip




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                                  RST Skinny
Page 1 of 5                                      Denim Pants                                    motocap.com.au
Abrasion Resistance
These pants were tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.80




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zones 1 & 2     > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        80%              1.73   1.16     2.40     2.69     2.10     1.84                 1.99   M
Material B        20%              0.45   0.47     0.39     0.29     0.41     0.27                 0.38   P
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             0.45   0.47     0.39     0.29     0.41     0.27                 0.38   P

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           0.45     0.47     0.39     0.29     0.41     0.27                  0.38   P


Details of materials used in jacket
Material A       Denim fabric shell with para-aramid inner liner
Material B       Denim fabric layer with mesh inner liner




                                                 RST Skinny
Page 2 of 5                                     Denim Pants                                  motocap.com.au
Burst Strength
These pants were tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1084




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1948           1398           1589          834           966           168           1150        G
Zones 3 & 4 604            769            1109          892           974           560           818         A




                                                  RST Skinny
Page 3 of 5                                      Denim Pants                                   motocap.com.au
Impact Protection
These pants were tested for impact protection and coverage in accordance with MotoCAP test protocols.
The diagram below is a visual indication of the likely performance of each impact protector calculated from
the data in the table below. The colour coding is based on the worst performing score for average or
maximum force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    1/10
                                                                               Impact score      0.0




Determining Criteria         Unit           Good        Acceptable      Marginal         Poor*
Impact force                 (kN)           < 15          15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Knee                                                Hip
Average force (kN)                                       P                                                P
Maximum force (kN)                                       P                                                P
Coverage of Zone 1 area
Coverage of Zone after displacement

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type        Knee       No impact protector present             Hip       No impact protector present
Strike location             Centre           Mid           Edge                Centre          Mid           Edge
Impact Protector 1
Impact Protector 2
Impact Protector 3



                                                     RST Skinny
Page 4 of 5                                         Denim Pants                                        motocap.com.au
Breathability
These pants were tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating   ★★★                                 Breathability rating       N/A
Breathability score      0.460                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            29.2            28.9          29.0
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.232           0.213        0.223
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This pants have not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           RST
    Model                           Skinny
    Type                            Pants - Denim
    Date purchased                  25 May 2021
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          P20D17
    Rating first published          November 2021
    Rating updated                  22 November 2021


                                                  RST Skinny
Page 5 of 5                                      Denim Pants                                motocap.com.au
