                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Macna
                                                           Model                    EPI Nighteye
                                                           Type                     Jacket - Textile
                                                           Date purchased           25 May 2021
                                                           Sizes tested             XL and 2XL
                                                           Test garment gender      Male
                                                           Style                    Tourer
                                                           RRP                      $359.00


                                                           Test Results Summary                Rating        Score
                                                           MotoCAP Protection Rating            ★             23.6
                                                           Abrasion                             1/10          0.76
                                                           Burst                               10/10         1210
                                                           Impact                               4/10          25.7
                                                           MotoCAP Breathability Rating         ⯨            0.109
                                                           Moisture Vapour Resistance            -           143.0
                                                           Thermal Resistance                    -           0.260
                                                           Water resistance                    1/10           26.1
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Replacing the elbow and shoulder armour with higher performing impact
protectors would improve the protection levels of this garment. There are zipped vents in the chest, and
mesh panels are located in the back to allow controlled airflow movement through the garment. The
breathability rating is based on tests of the garment's materials when all vents are closed. The breathability
of this product may be better when the vents can be opened. Breathability was measured without the
removable thermal liner installed.
Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                       Removable liners
                                                                       Thermal liner                    
                                                                       Water resistant liner

                                                                       Removable impact protection
                                                                                     Pockets        Armour
                                                                       Elbow                         
                                                                       Shoulder                      
                                                                       Back            




          Zone 1                      Zone 2                        Zone 3                       Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                               Macna EPI Nighteye
Page 1 of 5                                      Textile Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                     Abrasion Resistance Performance
                                                                     Abrasion rating      1/10
                                                                     Abrasion score       0.76




Determining Criteria    Area            Good        Acceptable     Marginal       Poor
High abrasion risk      Zone 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9      < 1.3
Medium abrasion risk    Zone 3          > 2.5        1.8 - 2.5     0.8 - 1.7      < 0.8
Low abrasion risk       Zone 4          >1.5         1.0 - 1.5     0.4 - 0.9      < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zone 1 & 2        Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        100%             0.84   1.04     0.89     1.39     0.66     0.44                 0.88   P

Zone 3           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       30%            0.59     0.62     0.32     0.50     0.52     0.35                  0.48     P
Material C       70%            0.43     0.43     1.00     0.63     0.47     0.75                  0.62     P
Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       15%            0.59     0.62     0.32     0.50     0.52     0.35                  0.48     M
Material C       85%            0.43     0.43     1.00     0.63     0.47     0.75                  0.62     M
Details of materials used in jacket
Material A       Heavy woven fabric shell with water-resistant inner liner
Material B       Reflective coated woven fabric shell with water-resistant inner liner
Material C       Woven fabric shell with water-resistant inner liner




                                            Macna EPI Nighteye
Page 2 of 5                                   Textile Jacket                                 motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating    10/10
                                                                          Burst score      1210




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1398           1123           1413          1345          1005          1343          1271        G
Zones 3 & 4 1114           659            1135          868           832           1173          963         A




                                             Macna EPI Nighteye
Page 3 of 5                                    Textile Jacket                                  motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximum
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    4/10
                                                                               Impact score     25.7




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Individual Impact Protector Results: - The table below shows the test results for each strike on each impact
protector in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone. Individual strike
results are capped at a maximum of 50kN.
Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            23.8       A                                    23.8         A
Maximum force (kN)                            27.1       M                                    27.1         M
Coverage of Zone 1 area                       70%                                             95%
Coverage of Zone after displacement           70%                                             95%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           24.9             22.3           27.1              24.9              22.3          27.1
Impact Protector 2           22.0             25.4           24.1              22.0              25.4          24.1
Impact Protector 3           20.9             22.6           24.8              20.9              22.6          24.8



                                                Macna EPI Nighteye
Page 4 of 5                                       Textile Jacket                                        motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ⯨                               Breathability rating       N/A
Breathability score      0.109                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)            1             2          Average
Without removable liners                             145.9          140.1          143.0
With water-resistant liner                            N/A            N/A            N/A
                              2
Thermal Resistance - Rct (K.m /W)                      1             2          Average
Without removable liners                             0.256          0.263          0.260
With water-resistant liner                            N/A            N/A            N/A

Water spray and rain resistance
This jacket is advertised as water-resistant, and so has been tested for water spray and rain resistance
according to the MotoCAP test protocols. The table below shows the water absorbed (ml) and the wetting
proportion (%) of the garment and undergarments due to water absorption.
                Water absorbed by garment          Water absorbed by underwear
                Volume (ml)    Percentage (%)      Volume (ml)    Percentage (%)
Jacket 1            246             19%                 85             29%
Jacket 2            233             18%                 64             23%
Average             239             19%                 75             26%

Location of wetting
Major visible wetting to the cotton underwear was present at the chest and neck of both jackets tested.




    Assessment Details.
    Brand                           Macna
    Model                           EPI Nighteye
    Type                            Jacket - Textile
    Date purchased                  25 May 2021
    Tested by                       AMCAF, Deakin University
    Report approved by              MotoCAP Chief Scientist
    Garment test reference          J20T26
    Rating first published          October 2021
    Rating updated                  12 November 2021

                                                Macna EPI Nighteye
Page 5 of 5                                       Textile Jacket                            motocap.com.au
