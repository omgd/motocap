                                                           This MotoCAP safety rating applies to:
                                                           Brand                    Ducati
                                                           Model                    Scrambler Quattrotasche
                                                           Type                     Jacket - Leather
                                                           Date purchased           4 December 2020
                                                           Sizes tested             54 & 56
                                                           Test garment gender      Male
                                                           Style                    All Purpose
                                                           RRP                      $839.00


                                                           Test Results Summary               Rating        Score
                                                           MotoCAP Protection Rating          ★★             29.9
                                                           Abrasion                           3/10           2.78
                                                           Burst                              9/10           962
                                                           Impact                             3/10           21.3
                                                           MotoCAP Breathability Rating        ★            0.285
                                                           Moisture Vapour Resistance           -            63.0
                                                           Thermal Resistance                   -           0.299
                                                           Water resistance                    N/A           N/A
This garment is fitted with impact protectors for the elbows and shoulders. A pocket is provided for an
aftermarket back protector. Elbow and shoulder impact protectors are permanently fixed and can not be
exchanged with aftermarket impact protectors. There are no vents to allow airflow movement through the
garment.




Jacket and Pants - Crash Impact Risk Zones
This diagram is a pictorial representation of the crash impact risk Zones.



                                                                      Removable liners
                                                                      Thermal liner
                                                                      Water-resistant liner

                                                                      Removable impact protection
                                                                                     Pockets       Armour
                                                                      Elbow                          
                                                                      Shoulder                       
                                                                      Back                




          Zone 1                      Zone 2                      Zone 3                        Zone 4

   High risk of abrasion       High risk of abrasion     Medium risk of abrasion          Low risk of abrasion
    High risk of impact
                                       Ducati Scrambler Quattrotasche
Page 1 of 5                                     Leather Jacket                                   motocap.com.au
Abrasion Resistance
The jacket was tested for abrasion resistance in accordance with MotoCAP test protocols. The diagram
below is a visual indication of the likely abrasion performance of the materials in each zone calculated from
the data in the table below. The colour coding is based on the worst performing material in each zone.




                                                                      Abrasion Resistance Performance
                                                                      Abrasion rating      3/10
                                                                      Abrasion score       2.78




Determining Criteria    Area             Good        Acceptable     Marginal      Poor
High abrasion risk      Zones 1 & 2      > 5.6        3.0 - 5.6     1.3 - 2.9     < 1.3
Medium abrasion risk    Zone 3           > 2.5        1.8 - 2.5     0.8 - 1.7     < 0.8
Low abrasion risk       Zone 4           >1.5         1.0 - 1.5     0.4 - 0.9     < 0.4

Individual Abrasion Resistance Results: - The table below shows the test results for time to abrade through
all layers of the materials. Calculated for each sample by Zone, type and area coverage of each material as a
proportion of that Zone. Abrasion times are capped at a maximum of 10.00s.
Abrasion time for each test (seconds)
Zones 1 & 2       Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material A        80%              3.04   2.65     3.62     3.07                                   3.10   A
Material B        20%              2.62   2.33     3.02     2.50     2.66     2.32                 2.58   M
Zone 3            Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6              Average
Material B        100%             2.62   2.33     3.02     2.50     2.66     2.32                 2.58   G

Zone 4           Coverage (%) Sample 1 Sample 2 Sample 3 Sample 4 Sample 5 Sample 6               Average
Material B       100%           2.62     2.33     3.02     2.50     2.66     2.32                  2.58   G


Details of materials used in jacket
Material A       Leather shell, foam layer and quilted fabric inner liner
Material B       Leather shell with quilted fabric inner liner




                                       Ducati Scrambler Quattrotasche
Page 2 of 5                                     Leather Jacket                               motocap.com.au
Burst Strength
The jacket was tested for burst strength in accordance with MotoCAP test protocols. The diagram below
illustrates the burst strength results in terms of the likely performance of the garment in an impact and is a
pictorial representation of the data from the table below.




                                                                          Burst Strength Performance
                                                                          Burst rating     9/10
                                                                          Burst score       962




Determining Criteria        Unit         Good        Acceptable     Marginal       Poor
Burst strength             (kPa)        > 1000       800 - 1000    500 - 799       < 500
Individual Burst Strength Results: - The table below shows the burst pressure in kilopascals (kPA) for each
sample tested by Zone and the average result for each zone.
Burst pressure for each seam (kPA)
Area         Sample 1      Sample 2       Sample 3      Sample 4      Sample 5      Sample 6      Average
Zones 1 & 2 1042           627            796           902           954           978           883         A
Zones 3 & 4 1956           704            1901          1872          674           554           1277        G




                                       Ducati Scrambler Quattrotasche
Page 3 of 5                                     Leather Jacket                                 motocap.com.au
Impact Protection
The jacket was tested for impact protection and coverage in accordance with MotoCAP test protocols. The
diagram below is a visual indication of the likely performance of each impact protector calculated from the
data in the table below. The colour coding is based on the worst performing score for average or maximium
force for each impact zone. Areas shaded black are not considered for impact protection ratings.




                                                                               Impact Protection Performance
                                                                               Impact rating    3/10
                                                                               Impact score     21.3




Determining Criteria         Unit            Good       Acceptable      Marginal         Poor*
Impact force                 (kN)            < 15         15 - 24        25 - 30         > 30
* Poor may also indicate that no impact protector, or impact protector pocket is present in the garment


Impact Protector Results: - The table below shows the average and maximum force transmitted through
each impact protector type in kilonewtons (kN) and their area of coverage as a proportion (%) of the Zone.


Impact protector type                        Elbow                                          Shoulder
Average force (kN)                            23.6       A                                    24.1         A
Maximum force (kN)                            24.3       A                                    25.7         M
Coverage of Zone 1 area                       70%                                             80%
Coverage of Zone after displacement           50%                                             70%

Individual Impact Protector Results: - The table below shows the test results for each strike on individual
impact protectors in kilonewtons (kN) and the position of the strike. Individual strike results are capped at a
maximum of 50kN.
Force transfer for each impact strike (kN)
Impact protector type       Elbow                                            Shoulder
Strike location             Centre            Mid            Edge             Centre             Mid           Edge
Impact Protector 1           23.2             23.6           23.5              22.8              24.0          25.0
Impact Protector 2           23.3             24.3           23.8              23.7              25.7          24.5
Impact Protector 3           23.3             23.8           23.5              23.9              23.4          24.0



                                         Ducati Scrambler Quattrotasche
Page 4 of 5                                       Leather Jacket                                        motocap.com.au
Breathability
The jacket was tested for breathability following the MotoCAP test protocols. The table below shows the
moisture vapour resistance and the thermal resistance values obtained.
Without removable liners                                   With water-resistant liner
Breathability rating       ★                               Breathability rating       N/A
Breathability score      0.285                             Breathability score        N/A

Moisture Vapour Resistance - Ret (kPa.m2/W)           1              2          Average
Without removable liners                            63.8            62.2          63.0
With water-resistant liner                          N/A             N/A           N/A
                             2
Thermal Resistance - Rct (K.m /W)                     1              2          Average
Without removable liners                            0.299           0.300        0.299
With water-resistant liner                           N/A             N/A          N/A

Water spray and rain resistance
This jacket has not been advertised as water-resistant so has not been tested for water spray and rain
resistance.




    Assessment Details.
    Brand                           Ducati
    Model                           Scrambler Quattrotasche
    Type                            Jacket - Leather
    Date purchased                  4 December 2020
    Tested by                       AMCAF, Deakin University
    Garment test reference          J20L04
    Rating first published          March 2021
    Rating updated                  5 March 2021


                                       Ducati Scrambler Quattrotasche
Page 5 of 5                                     Leather Jacket                              motocap.com.au
