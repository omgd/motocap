# SPDX-License-Identifier: AGPL-3.0-or-later

def enhance_gear: # add some fields
    .code = (.head."Test code" // .file[8:14])
    | . + (
        if .head.Type then ( .head.Type/" - " | { type:.[0], material:.[1] } ) else {} end
    ) + {
        brand:.head.Brand,
        model:.head.Model
    }
;

def add_note: # semi-automatic summary
    .note = (
        [
            ["L", (.desc+""|test("leather|suede";"i"))],
            ["S", (.desc+""|test("stretch";"i"))],
            ["K", (.desc+""|test("knit";"i"))],
            ["A", (.desc+""|test("aramid|kevlar";"i"))
             or [.brand,.model,.code]==["Triumph","Malvern Jeans","A"]],
            ["V", (.desc+""|test("vectran?|covec";"i"))],
            ["U", (.desc+""|test("UHMWPE|dyneema|armalith";"i"))
             or [.brand,.model]==["Saint","Model 2"]],
            ["?", [.brand,.model,.code]==["Macna","Fulcrum","A"]] # test error?
        ] | map(if .[1] then .[0] else "." end) | add
    )
;

def totable($fields): $fields, (.[]|[ getpath($fields[]|split(".")) ]) ;

def materials_tsv:
    [
        .[]
        | enhance_gear
        | {
            brand, model,
            gear_code:.code,
            gear_type:.type,
            gear_material:.material
        } + (
            .materials[] |
            {
                code,
                desc,
                avg
            }
        )
        | add_note
    ]
    | sort_by(.avg) | reverse
    | totable("avg note desc code gear_code brand model gear_type gear_material" | split(" "))
    | @tsv
;

def gears_tsv:
    [
        .[] | enhance_gear
    ]
    | sort_by(.abrasion.rating) | reverse
    | totable(["brand", "model", "type", "material", "code", "abrasion.rating", "abrasion.result",
               "burst.rating", "burst.result", "impact.rating", "impact.result",
               "moisture_vapour_resistance", "thermal_resistance", "water_resistance" ])
    | @tsv
;
