## Notes and errata on specific products and materials

---

*Syntax*: to link a comment to a specific product, include `{.mc-[test_code_here]}` on the header line. This will allow to attach it automatically to the report data, in case useful informations accumulate here.

---

### DriRider Vortex Adventure 2 Ladies {.mc-P20T01}
> Outer Shell Polyester 900D with 1200D on Shoulders & Elbows, PU coated
-- [source](https://www.dririder.com.au/all-products/vortex-adventure-2-jkt)

### Bull-it	Ladies Envy 17 Leggings	Pants {.mc-P19T17}
> This garment is fitted with impact protectors for the knees.
-- MotoCAP

> Reinforced knee panels. Knee armour pockets for optional armour.
-- [source](https://www.infinitymotorcycles.com/product/bull-it-envy-17-sp120-lite-ladies-leggings)

### Macna Fulcrum Pants {.mc-P18T08}
The MotoCAP report gives weird results for abrasion.

### Triumph Malvern Jeans {.mc-P18T04}
Has patches of nylon + aramid ("Armacor").
-- [source](https://shop.triumphmotorcycles.com/jeans/id-MTJC18408/Malvern_Jeans)

### Bull-it SP120 Lite Heritage Easy Pants {.mc-P18D03}
Liner is TLCP, not Kevlar ([Covec’s SP120 liner](https://oxford-products.com/close-out-products/jeans/bull-it-sp120-lite-heritage-easy-fit-jeans-close-out/)).
