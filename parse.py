# SPDX-License-Identifier: AGPL-3.0-or-later

import sys, re

infile = sys.argv[1] if len(sys.argv) >1 else None
raw = (open(infile) if infile else sys.stdin).read()

head = {}
materials = {}
obj = {'head': head, 'materials': materials}

for m in re.finditer(r'^ +(?P<key>.+?)(?:: |  +) *(?P<val>.*)$', raw[:2400], re.M):
    head[m['key']] = m['val']

def maybe_float(s):
    try: return float(s.replace('/10',''))
    except: return s

def parse_rating(s, one):
    if one:
        return maybe_float(s)
    else:
        s = s.split()
        return { 'rating': maybe_float(s[0]), 'result': maybe_float(s[1]) }

for k in ["Abrasion", "Burst", "Impact",
          "Moisture Vapour Resistance", "Thermal Resistance", "Water Resistance"]:
    if k in head:
        try:
            obj[k.lower().replace(' ', '_')] = parse_rating(head[k], k.endswith('Resistance'))
        except:
            pass

for m in re.finditer(r'(?:Material|Fabric) +(?P<code>[A-E](?: *& *[A-E])?)( +(?P<pct>\d+)% +(?P<vals>( *(?:\d{,3}\.\d+|-|Avoid|N/A)){,6}) +(?P<avg>[0-9.]+) *\w| *:? +(?P<desc>.*))$', raw, re.M):
    d = m.groupdict()
    a = materials.setdefault(d['code'], {'code': d['code']})
    if d.get('desc'):
        a['desc'] = d['desc']
    if d.get('avg'):
        a['avg'] = float(d['avg'])
    if d.get('vals'):
        a['vals'] = d['vals'].split() # [float(x) for x in ]
    # if d.get('pct'):
    #     a['pct'] = float(d['pct'])

for m in materials.values():
    if 'desc' not in m: sys.stderr.write('MISSING DESC')
    if 'avg' not in m: sys.stderr.write('MISSING AVG')


if infile: obj['file'] = infile


import json
json.dump(obj, sys.stdout, indent=4)
