MotoCAP Data
============

The ["Motorcycle Clothing Assessment Program"](https://motocap.com.au/) is an "independent, free resource supported by governments and private road safety organisations across Australia and New Zealand". It is the main publicly available source of motorcycle garments post-market testing.

They test jackets, pants and gloves, for safety (impact protection, burst and abrasion resistance) and comfort (breathability, water protection), using the same procedures as in the current European Standards EN 13595-1:2002, EN 13594:2002 and EN 1621-1:2012.

----

This repository not affiliated with MotoCAP. It addresses some caveats with the original publishing format.

First, MotoCAP arrange their results around an (under-documented) star rating system. It is misleading because products for which impact protectors are sold separately are simply evaluated without them, and breathability is tested on the fabric itself, ignoring any effect of vents (openings).

Second, the publishing format (the web site or hundreds of PDF files) is not easily searchable. This repository scrapes it into two tabular (.tsv) files, listing materials and gears. (The meaning of the additional one-letter fields in materials.tsv is given under `add_note` in the file `filters.jq`.)

Third, the descriptions of materials given in reports are somewhat vague and unreliable. Some cases are collected in [[errata-notes.md]]. If you have more, please (contact MotoCAP or) create an issue here.


License
-------

All programming code in this repo is free software, released under the GNU AGPL v3 or later.

Terms of use for the data themselves are [given by their original publishers](https://motocap.com.au/legal):
> This material is copyright but may be reproduced without formal
> permission or charge for personal, in-house or non-commercial use.
